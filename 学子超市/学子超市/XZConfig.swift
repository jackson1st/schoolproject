//
//  Resource.swift
//  学子超市
//
//  Created by jason on 3/13/16.
//  Copyright © 2016 jason. All rights reserved.
//

import UIKit

//MARK:- 全局常用属性
public let ScreenWidth: CGFloat = UIScreen.mainScreen().bounds.size.width
public let ScreenHeight: CGFloat = UIScreen.mainScreen().bounds.size.height
public let ScreenBounds: CGRect = UIScreen.mainScreen().bounds
public let ShopCarRedDotAnimationDuration: NSTimeInterval = 0.2
public let MarginV = 10.0
public let MarginH = 5.0
public let ShopCartCellHeight:CGFloat = 40
public let GDKey = "3f550df0f7a49463ca05a705b671ef15"
public let NotiPushAlertSizeChoose = "PushAlertSizeChoose"
public let NotiGoodNumChange = "GoodNumChange"
public let NotiShopCartClear = "ShopCartClear"
//MARK:- 主题颜色
public let MainBKColor = UIColor.colorWithRGB(239, g: 239, b: 239)
public let MainColor = UIColor.colorWithRGB(255, g: 150, b: 0)
public let RedBagOkColor = UIColor.colorWithRGB(240, g: 89, b: 103)
public let RedBagUnOkColor = UIColor.colorWithRGB(199, g: 199, b: 205)
public let ButtonOKColor = UIColor.colorWithRGB(255, g: 150, b: 0)
public let ButtonCancelColor = UIColor.colorWithRGB(155, g: 155, b: 155)
public let ButtonBottomColor = UIColor.colorWithRGB(249, g: 249, b: 249)
public let PriceRealColor = UIColor.colorWithRGB(254, g: 56, b: 36)
public let PricePastedColor = UIColor.colorWithRGB(155, g: 155, b: 155)
public let ClassColor = UIColor.colorWithRGB(239, g: 239, b: 239)
public let NoticeBKColor = UIColor.colorWithRGB(251, g: 249, b: 126)

//MARK:- 主题大小
public let PriceFont = UIFont.systemFontOfSize(18)
public let PriceChar = UIFont.systemFontOfSize(12)
public let PricePastedFont = UIFont.systemFontOfSize(10)


