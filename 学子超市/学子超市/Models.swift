//
//  Models.swift
//  学子超市
//
//  Created by jason on 16/3/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import RealmSwift

class GoodSize:Object{
    dynamic var name = ""//对应的规格具体值
    dynamic var detail = ""
    dynamic var imgURL = ""
    dynamic var oriPrice = 0.0
    dynamic var realPrice = 0.0
    dynamic var store = 0
    dynamic var active = 0    //活动的编码
    convenience init(name:String,detail:String,imgURL:String,oriPrice:Double,realPrice:Double,store:Int,active:Int){
        self.init()
        self.name = name
        self.detail = detail
        self.imgURL = imgURL
        self.oriPrice = oriPrice
        self.realPrice = realPrice
        self.store = store
        self.active = active

    }
    var realPriceStr:NSMutableAttributedString{
        get{
            let str = "¥" + String(format: "%.1f", realPrice)
            let attStr = NSMutableAttributedString(string: str)
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(0, 1))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(16),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(1, str.characters.count - 1))
            return attStr
        }
    }
    var oriPriceStr:NSMutableAttributedString{
        get{
            let str = "¥" + String(format: "%.1f", oriPrice)
            let attStr = NSMutableAttributedString(string: str)
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(10),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(0, 1))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(1, str.characters.count - 1))
            attStr.addAttributes([NSStrikethroughStyleAttributeName:1], range: NSMakeRange(0, str.characters.count))
            attStr.addAttribute( NSStrikethroughColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, str.characters.count))
            attStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, str.characters.count))
            return attStr
        }
    }
}

struct Comment{
    var scoreGood:CGFloat!
    var scoreSpeed:CGFloat!
    var userName:String!
//    var userId:String!
    var comContent:String!
    var date:String!//时间先粗略处理
    var photoURL:String!
    var storeReply:(content:String,time:String)?//先用String处理
    init(userName:String,comContent:String,date:String,photoURL:String,scoreGood:CGFloat,scoreSpeed:CGFloat,storeReply:(content:String,time:String)?=nil){
        self.scoreGood = scoreGood
        self.scoreSpeed = scoreSpeed
        self.userName = userName
        self.comContent = comContent
        self.date = date
        self.photoURL = photoURL
        self.storeReply = storeReply
    }
}

//活动：包含满减、满送、红包、兑换劵
enum Activity{
    case redBag(RedBag)
    case ticketGood(TicketGood)
    case afterReduction(AfterReduction)
    case afterSend(AfterSend)
}
class RedBag:NSObject{
    var id:String!
    var title:String!
    var startTime:String!
    var endDate:String!
    var isShare:Int = -1
    var logoImageUrl:String!
    var maxUseNum:Int = -1
    var merchantId:String!//可上的
    var value:Double!//抵多少钱
    var state:Bool! = true
    var getValue:NSMutableAttributedString{
        get{
            let attStr = NSMutableAttributedString(string: String(format: "%.1f",value))
            attStr.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(36)], range: NSMakeRange(0, attStr.length - 2))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(18)], range: NSMakeRange(attStr.length - 2, 2))
            return attStr
        }
    }
    var validTime:String{
        get{
            let str = startTime + " 至 " + "123"
            return str
        }
    }
//    init(title:String,startTime:String,endTime:String,value:Double){
//        self.title = title
//        self.startTime = startTime
////        self.endTime = endTime
//        self.value = value
//    }
//    init(title:String,startTime:String,endTime:String,value:Double,state:Bool){
//        self.title = title
//        self.startTime = startTime
////        self.endTime = endTime
//        self.value = value
//        self.state = state
//    }
    
}
//少了id 红包和兑换劵
class TicketGood:NSObject{
    
    var title:String
    var startTime:String
    var endTime:String
//    var good:Good//能够兑换的物品
    var num:Int = 0//能够兑换的数量
    var ticketID:String//兑换码
    var state:Bool = true
    init(title:String,startTime:String,endTime:String,state:Bool,num:Int,ticketID:String){
        self.title = title
        self.startTime = startTime
        self.endTime = endTime
        self.state = state
        self.num = num
        self.ticketID = ticketID
    }

    var getTicketID:String {
        get{
            return "兑换码:" + ticketID
        }
    }
//    var getGoodInfo:String{
//        get{
//            return "\(num)件\n\(good.name)"
//        }
//    }
    var getValidTime:String{
        get{
            let str = startTime + " 至 " + endTime
            return str
        }
    }
}

class AfterReduction:NSObject{
    var title:String
    var startTime:String
    var endTime:String
    init(title:String,startTime:String,endTime:String){
        self.title = title
        self.startTime = startTime
        self.endTime = endTime
    }
}

class AfterSend:NSObject{
    var title:String
    var startTime:String
    var endTime:String
    init(title:String,startTime:String,endTime:String){
        self.title = title
        self.startTime = startTime
        self.endTime = endTime
    }
}

class FeeEnableSend:NSObject,NSCoding{
    var center:CLLocation
    var radio:Double
    var fee:Double
    private let centerKey = "center"
    private let radioKey = "radio"
    private let feeKey = "fee"
    init(center:CLLocation,radio:Double,fee:Double){
        self.center = center
        self.radio = radio
        self.fee = fee
    }
    required init?(coder aDecoder: NSCoder) {
        self.center = aDecoder.decodeObjectForKey(centerKey) as! CLLocation
        self.radio = aDecoder.decodeDoubleForKey(radioKey)
        self.fee = aDecoder.decodeObjectForKey(feeKey) as! Double
    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(center, forKey: centerKey)
        aCoder.encodeObject(fee, forKey: feeKey)
        aCoder.encodeDouble(radio, forKey: radioKey)
    }
}


let StoreFilePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last! + "defaultStore.archiver"

class Store:NSObject,NSCoding{
    
    static var defaultStore:Store? = {
        let object = NSKeyedUnarchiver.unarchiveObjectWithFile(StoreFilePath) as? Store
        //连接一下网络，更新info
        return object
    }()
    var ID:String
    var name:String
    var photoURL:String
    var score:Double
    var location:CLLocation
    var activities = [Activity]()
    var feeSend:Double//配送费
    var feeEnableSends:[FeeEnableSend]//起送价
    var feeEnableSend:Double = -1
    var businessHours:String//营业时间，%%%类型等待讨论%%%
    var phone:String//电话
    var detailLocation:String//地址
    //商店类别
    //获取距离
    
    private let nameKey  = "name"
    private let photoURLKey = "photoURL"
    private let scoreKey = "score"
    private let locationKey = "location"
    private let activitiesKey = "activities"
    private let feeSendKey = "feeSend"
    private let feeEnableSendsKey = "feeEnableSends"
    private let feeEnableSendKey = "feeEnableSend"
    private let businessHoursKey = "businessHours"
    private let phoneKey = "phone"
    private let detailLocationKey = "detailLocation"
    private let IDKey = "ID"
    required init?(coder aDecoder: NSCoder) {
        ID = aDecoder.decodeObjectForKey(IDKey) as! String
        name = aDecoder.decodeObjectForKey(nameKey) as! String
        photoURL = aDecoder.decodeObjectForKey(photoURLKey) as! String
        score = aDecoder.decodeDoubleForKey(scoreKey)
        location = aDecoder.decodeObjectForKey(locationKey) as! CLLocation
        feeSend = aDecoder.decodeObjectForKey(feeSendKey) as! Double
        feeEnableSends = aDecoder.decodeObjectForKey(feeEnableSendsKey) as! [FeeEnableSend]
        feeEnableSend = aDecoder.decodeObjectForKey(feeEnableSendKey) as! Double
        businessHours = aDecoder.decodeObjectForKey(businessHoursKey) as! String
        phone = aDecoder.decodeObjectForKey(phoneKey) as! String
        detailLocation = aDecoder.decodeObjectForKey(detailLocationKey) as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(ID, forKey: IDKey)
        aCoder.encodeObject(name, forKey: nameKey)
        aCoder.encodeObject(photoURL, forKey: photoURLKey)
        aCoder.encodeDouble(score, forKey: scoreKey)
        aCoder.encodeObject(location, forKey: locationKey)
        aCoder.encodeObject(feeSend, forKey: feeSendKey)
        aCoder.encodeObject(feeEnableSends, forKey: feeEnableSendsKey)
        aCoder.encodeObject(feeEnableSend, forKey: feeEnableSendKey)
        aCoder.encodeObject(businessHours, forKey: businessHoursKey)
        aCoder.encodeObject(phone, forKey: phoneKey)
        aCoder.encodeObject(detailLocation, forKey: detailLocationKey)
    }
    
    func restoreData(){
        NSKeyedArchiver.archiveRootObject(self, toFile: StoreFilePath)
    }
    
    init(ID:String,name: String, photoURL: String, score: Double, location: CLLocation, activities: [Activity]?, feeSend: Double, feeEnableSends: [FeeEnableSend], businessHours: String, phone: String, detailLocation: String){
        self.ID = ID;
        self.name = name
        self.photoURL = photoURL
        self.score = score
        self.location = location
        if(activities != nil){
            self.activities.appendContentsOf(activities!)
        }
        self.feeSend = feeSend
        self.feeEnableSends = feeEnableSends
        self.businessHours = businessHours
        self.phone = phone
        self.detailLocation = detailLocation
    }
    
    func getFeeEnableSendStr() -> NSAttributedString{
        let str = feeEnableSend == -1 ? "超过范围":"¥\(feeEnableSend)起送"
        let attrStr = NSMutableAttributedString(string: str)
        attrStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.FeeSendStrColoc()], range: NSMakeRange(str.characters.count - 2, 2))
        if feeEnableSend != -1{
            attrStr.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:UIColor.MainColor()], range: NSMakeRange(1, str.characters.count - 3))
            attrStr.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(13),NSForegroundColorAttributeName:UIColor.MainColor()], range: NSMakeRange(0, 1))
        }
        return attrStr
    }
    
    func getFeeSendStr() -> NSAttributedString{
        let str = "¥\(feeSend)配送费"
        let attrStr = NSMutableAttributedString(string: str)
        attrStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(11),NSForegroundColorAttributeName:UIColor.FeeSendStrColoc()], range: NSMakeRange(str.characters.count - 3, 3))
        if feeEnableSend != -1{
            attrStr.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(15),NSForegroundColorAttributeName:UIColor.MainColor()], range: NSMakeRange(1, str.characters.count - 4))
            attrStr.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(11),NSForegroundColorAttributeName:UIColor.MainColor()], range: NSMakeRange(0, 1))
        }else{
            attrStr.replaceCharactersInRange(NSMakeRange(0, str.characters.count), withString: "免配送费")
        }
        return attrStr
    }
    
    func getFeeEnableSend(userLocation:CLLocation){
        if(feeEnableSend == -1){
            for x in feeEnableSends{
                if userLocation.distanceFromLocation(x.center) < x.radio {
                    feeEnableSend = (feeEnableSend == -1 || feeEnableSend > x.fee) ? x.fee:feeEnableSend
                }
            }
        }
    }
    
    func getDistance(fromLocation:CLLocation) -> CLLocationDistance{
        getFeeEnableSend(fromLocation)
        return location.distanceFromLocation(fromLocation)
    }
}