//
//  OrderSendTimeChooseViewController.swift
//  学子超市
//
//  Created by jason on 16/6/13.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import STPopup
class OrderSendTimeChooseViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
        self.contentSizeInPopup = CGSize(width: ScreenWidth, height: 190)
    }
    var pickView:UIPickerView!{
        didSet{
            self.view.addSubview(pickView)
            pickView.snp_makeConstraints { (make) in
                make.left.right.bottom.equalTo(self.view)
                make.top.equalTo(self.view).offset(-130)
            }
        }
    }
    var block:(()->Void)!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择送达时间"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "OK_White"), style: .Plain, target: self, action: #selector(self.close))
    }
    
    func close(){
        self.dismissViewControllerAnimated(true, completion: nil)
        block()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

}
