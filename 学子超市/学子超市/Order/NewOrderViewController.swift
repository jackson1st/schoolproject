//
//  NewOrderViewController.swift
//  学子超市
//
//  Created by Jason on 16/5/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import KeyboardMan
import STPopup
class NewOrderViewController: BaseViewController {

    @IBOutlet weak var ConstraintTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var ConstraintTableViewBottomToView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelSex: UILabel!
    @IBOutlet weak var LabelPhone: UILabel!
    @IBOutlet weak var LabelAddress: UILabel!
    @IBOutlet weak var LabelMoneySum: UILabel!
    @IBOutlet weak var ViewTip: UIView!
    @IBOutlet weak var LabelTip: UILabel!
    @IBOutlet weak var ImgTip: UIImageView!
    @IBOutlet weak var ConstraintTipHeight: NSLayoutConstraint!
    var payWays = ["支付宝","货到付款"]
    var payWay = 0
    var specialActicites = ["红包","兑换券"]
    var sendTimeStr:String! = "立即送达"
    var recAddress:RecAddress!{
        didSet{
            self.LabelName.text = recAddress.receiverName
            self.LabelSex.text = recAddress.sex
            self.LabelPhone.text = recAddress.receiverPhone
            self.LabelAddress.text = recAddress.detail
        }
    }
    weak var leftMsgTextFiled:UITextField!
    var keyboardMan = KeyboardMan()

    lazy var timePickerView:UIPickerView = {
       let view = UIPickerView(frame: CGRectMake(0, 0, self.view.frame.width, 300))
        view.delegate = self
        return view
    }()
    var pickerViewTime = [String]()
    lazy var SendTimeChooseViewSheet:STPopupController = {
        let vc = OrderSendTimeChooseViewController()
        vc.block = {
            self.sendTimeStr = self.pickerViewTime[self.timePickerView.selectedRowInComponent(0)]
            self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 2)], withRowAnimation: .None)
        }
        vc.pickView = self.timePickerView
        let vc2 = STPopupController(rootViewController: vc)
        vc2.navigationBar.tintColor = UIColor.whiteColor()
        vc2.style = .BottomSheet
        return vc2
    }()
    lazy var redBagUseViewController:RedBagUseViewController = {
        let vc = RedBagUseViewController()
        vc.actionSelected = {
           [weak self] _ in
            
        }
        return vc
    }()
    lazy var ticketGoodUseViewController:TicketGoodUseViewController = {
        let vc = TicketGoodUseViewController()
        
        return vc
    }()
    lazy var recAddressViewController:RecAddressesViewController = {
        let vc = RecAddressesViewController()
        vc.selectAddressAction = {
            [weak self] rec in
            self?.recAddress = rec
        }
        return vc
    }()
    
    var itemList = ShopCartGood.getAll(){
        didSet{
            tableView.reloadSections(NSIndexSet(index: 3), withRowAnimation: .None)
            LabelMoneySum.text = String(format: "%.1f", shopCartViewController.instance.price)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewConfigure()
        keyboardManConfigure()
        self.title = "确认订单"
    }
    
    private func keyboardManConfigure(){
        keyboardMan.animateWhenKeyboardAppear = {
            _,height,_ in
            self.ConstraintTableViewBottomToView.constant = height-49
            self.view.layoutIfNeeded()
        }
        
        keyboardMan.animateWhenKeyboardDisappear = {
            [weak self] _ in
            if let sSelf = self {
                sSelf.ConstraintTableViewBottomToView.constant = 0
                sSelf.view.layoutIfNeeded()
            }
        }
    }
    
    private func tableViewConfigure(){
        tableView.clearOtherLine()
        tableView.registerReusableCell(PayWayChooseTableViewCell)
        tableView.separatorStyle = .None
        tableView.keyboardDismissMode = .OnDrag
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        itemList = ShopCartGood.getAll()
    }
    

    
    @IBAction func ButtonChosseAddressClicked(sender: AnyObject) {
        self.navigationController?.pushViewController(self.recAddressViewController, animated: true)
    }

    @IBAction func ButtonChooseMakeAOrder(sender: AnyObject) {
        print("立即下单")
    }
    
}
extension NewOrderViewController:UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        var time = NSDate().timeIntervalSince1970
        time = time - time % 1800
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        self.pickerViewTime.removeAll()
        var x = 1800.0
        self.pickerViewTime.append("立即送出")
        for _ in 1..<7{
            self.pickerViewTime.append(dateFormatter.stringFromDate(NSDate.init(timeIntervalSince1970: time + x)))
            x += 1800.0
        }
        //最终还得考虑营业时间，后期修复
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 7
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewTime[row]
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
}

extension NewOrderViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return payWays.count
        case 1:
            return specialActicites.count
        case 2:
            return 2
        case 3:
            return itemList.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as PayWayChooseTableViewCell
            cell.name = payWays[indexPath.row]
            if indexPath.row == 0 {
                tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("specialActicites")!
            let textLabel = cell.viewWithTag(110) as! UILabel
            let detailTextLabel = cell.viewWithTag(111) as! UILabel
            switch indexPath.row {
            case 0:
                textLabel.text = "红包"
                detailTextLabel.text = "10元减8元"
                return cell
            case 1:
                textLabel.text = "兑换券"
                detailTextLabel.text = "有可用兑换券"
                return cell
            default:
                return UITableViewCell()
            }
        case 2:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("cellSendTime")!
                let textLabel = cell.viewWithTag(110) as! UILabel
                let detailTextLabel = cell.viewWithTag(111) as! UILabel
                textLabel.text = "送达时间"
                detailTextLabel.text = sendTimeStr
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("CellMsg")!
                if let titleLabel = cell.viewWithTag(110) as? UILabel{
                    titleLabel.text = "备注"
                }
                cell.selectionStyle = .None
                leftMsgTextFiled = cell.viewWithTag(111) as! UITextField
                leftMsgTextFiled.delegate = self
                return cell
            default:
                return UITableViewCell()
            }
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("CellItem") as! OrderItemTableViewCell
            let labelName = cell.viewWithTag(110) as! UILabel
            labelName.text = itemList[indexPath.row].displayName
            let labelNum = cell.viewWithTag(111) as! UILabel
            labelNum.text = itemList[indexPath.row].displayNum
            let labelPrice = cell.viewWithTag(112) as! UILabel
            labelPrice.text = itemList[indexPath.row].displayPrice
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            payWay = indexPath.row
            return
        }
        leftMsgTextFiled.resignFirstResponder()
        if indexPath.section == 2 && indexPath.row == 1{
            leftMsgTextFiled.becomeFirstResponder()
        }else
        if indexPath.section == 2 && indexPath.row == 0{
            self.SendTimeChooseViewSheet.presentInViewController(self)
        }else
            if indexPath.section == 1 && indexPath.row == 0{
                self.navigationController?.pushViewController(self.redBagUseViewController, animated: true)
        }else
            if indexPath.section == 1 && indexPath.row == 1{
                self.navigationController?.pushViewController(self.ticketGoodUseViewController, animated: true)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        tableView.selectRowAtIndexPath(NSIndexPath(forRow: payWay, inSection: 0), animated: false, scrollPosition: .None)
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 3{
            return 30
        }
        
        return 10
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 30
        }
        
        return 44
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let label = UILabel()
            label.text = "   选择支付方式"
            label.textColor = UIColor(red: 143/255.0, green: 142/255.0, blue: 148/255.0, alpha: 1)
            label.font = UIFont.systemFontOfSize(14)
            return label
        }
        if section == 3 {
            let label = UILabel()
            label.text = "   商品清单"
            label.textColor = UIColor(red: 143/255.0, green: 142/255.0, blue: 148/255.0, alpha: 1)
            label.font = UIFont.systemFontOfSize(14)
            return label
        }
        return nil
    }
    
}

