//
//  Order.swift
//  学子超市
//
//  Created by jason on 16/6/9.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import YYModel
class Order: NSObject,YYModel{
    var id:String!
    var shopId:String!
    var customerTelephone:String!
    var staffId:String!
    var addressDetail:String!
    var orderTime = 0.0
    var preTime = 0.0//预计送达
    var payType = 0
    var payedMonney = 0.0
    var receivableMonney = 0.0
    var remark:String!
    var shippFee = 0.0//配送费
    var state:Int = 0
    var totalMonney = 0.0
    var salesItems:NSArray!
}

class OrderActivity:NSObject,YYModel{
    var discountAmount = 0.0
    var actiInfo:String!
}

class OrderItem:NSObject,YYModel{
    var id:String!
    var orderId:String!
    var shopsGoodsSpecDetailId:String!
    var shopId:String!
    var method:String!
    var name:String!
    var number:String!
    var price = 0.0
    var refundState = 0
    var remark:String!
}
