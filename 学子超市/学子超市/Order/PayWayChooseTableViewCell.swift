//
//  PayWayChooseTableViewCell.swift
//  学子超市
//
//  Created by Jason on 16/5/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class PayWayChooseTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var ButtonChoose: UIButton!
    var name:String!{
        didSet{
            LabelName.text = name
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        ButtonChoose.selected = selected
    }
}
