//
//  OrderStatusViewController.swift
//  学子超市
//
//  Created by Jason on 16/6/15.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class OrderStatusViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var orderHeadView = OrderStatusHeadView()
    var itemList = ShopCartGood.getAll()//先用本地购物车测试
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewConfigure()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
 
    private func tableViewConfigure(){
        tableView.tableHeaderView = orderHeadView
        orderHeadView.setContent(("待付款","这是一个待付款的订单"))
        tableView.separatorStyle = .None
    }
}

extension OrderStatusViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return itemList.count + 1
        case 1:
            return 3
        case 2:
            return 1
        case 3:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == itemList.count {
                let cell = tableView.dequeueReusableCellWithIdentifier("CellPrice")
                let label = cell?.viewWithTag(110) as! UILabel
                label.text = "订单金额: ¥\(123)"
                return cell!
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("CellItem")
            let labelFirst = cell?.viewWithTag(110) as! UILabel
            let labelSecond = cell?.viewWithTag(111) as! UILabel
            let labelThird = cell?.viewWithTag(112) as! UILabel
            labelFirst.text = itemList[indexPath.row].displayName
            labelSecond.text = itemList[indexPath.row].displayNum
            labelThird.text = itemList[indexPath.row].displayPrice
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("CellActivity")
            let labelFirst = cell?.viewWithTag(110) as! UILabel
            let labelSecond = cell?.viewWithTag(111) as! UILabel
            labelFirst.text = "活动名称"
            labelSecond.text = "活动内容"
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("CellRealPrice")
            let label = cell?.viewWithTag(110) as! UILabel
            label.text = "实际金额: ¥123"
            return cell!
        case 3:
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCellWithIdentifier("CellAddress")
                let labelFirst = cell?.viewWithTag(110) as! UILabel
                let labelSecond = cell?.viewWithTag(111) as! UILabel
                let labelThird = cell?.viewWithTag(112) as! UILabel
                labelFirst.text = "收货信息"
                labelSecond.text = "姓名 手机号码"
                labelThird.text = "详细地址"
                return cell!
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("CellInfo")
            let labelFirst = cell?.viewWithTag(110) as! UILabel
            let labelSecond = cell?.viewWithTag(111) as! UILabel
            switch indexPath.row {
            case 0:
                labelFirst.text = "订单编号"
                labelSecond.text = "123412343432"
            case 2:
                labelFirst.text = "下单时间"
                labelSecond.text = "2016-03-01 15:32"
            case 3:
                labelFirst.text = "配送人员"
                labelSecond.text = "学子配送"
            default:
                break;
            }
            return cell!
        default:
            return UITableViewCell()
        }
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let label = UILabel()
            label.text = "   清单"
            label.textColor = UIColor(red: 143/255.0, green: 142/255.0, blue: 148/255.0, alpha: 1)
            label.font = UIFont.systemFontOfSize(14)
            return label
        case 3:
            let label = UILabel()
            label.text = "   订单信息"
            label.textColor = UIColor(red: 143/255.0, green: 142/255.0, blue: 148/255.0, alpha: 1)
            label.font = UIFont.systemFontOfSize(14)
            return label
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == itemList.count{
                return 35
            }
            return 30
        case 1:
            return 30
        case 2:
            return 35
        case 3:
            if indexPath.row == 1{
                return 50
            }
            return 25
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 30
        case 1,2:
            return 1
        case 3:
            return 30
        default:
            return 0
        }
    }
}
