//
//  OrderStatusHeadView.swift
//  学子超市
//
//  Created by Jason on 16/6/15.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class OrderStatusHeadView: UIView {

    private var labelStatus = UILabel()
    private var labelDetail = UILabel()
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.frame.size.height = 100
        self.backgroundColor = UIColor.whiteColor()
        self.addSubview(labelStatus)
        self.addSubview(labelDetail)
        labelStatus.font = UIFont.boldSystemFontOfSize(20)
        labelStatus.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
        labelStatus.textAlignment = .Center
        labelStatus.snp_makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(self).offset(20)
        }
        labelDetail.font = UIFont.boldSystemFontOfSize(16)
        labelDetail.textColor = UIColor.colorWithRGB(153, g: 153, b: 153)
        labelDetail.textAlignment = .Center
        labelDetail.snp_makeConstraints { (make) in
            make.top.equalTo(labelStatus.snp_bottom).offset(7)
            make.centerX.equalTo(self)
        }
    }
    func setContent(content: (String,String)){
        setStatus(content.0)
        setDetail(content.1)
    }
    
    func setStatus(text:String){
        self.labelStatus.text = text
    }
    
    func setDetail(text:String){
        self.labelDetail.text = text
    }
}
