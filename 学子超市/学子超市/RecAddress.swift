//
//  RecAddress.swift
//  学子超市
//
//  Created by jason on 16/6/9.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import YYModel
class RecAddress:NSObject,YYModel{
    var receiverName:String!
    var receiverPhone:String!
    var receiverSex:Bool!
    var coordinate:CLLocation!
    var locAddress:String!
    var detailAddress:String!
    var remark:String?
    var detail:String{
        get {
            return locAddress + detailAddress
        }
    }
    
    var sex:String{
        get{
            return self.receiverSex == true ? "男":"女"
        }
    }
    
    static func modelPropertyBlacklist() -> [String]?{
        return ["coordinate","locAddress"]
    }
    
    func modelCustomTransformFromDictionary(dic: [NSObject : AnyObject]) -> Bool{
        let coordinate = (dic["position"] as! String).componentsSeparatedByString("-")
        let position = (_:(coordinate[0] as NSString).doubleValue,_:(coordinate[1] as NSString).doubleValue)
        self.coordinate = CLLocation(latitude: position.0, longitude: position.1)
        self.locAddress = self.detailAddress.componentsSeparatedByString("-")[0]
        if self.detailAddress.componentsSeparatedByString("-").count == 1{
            self.detailAddress = "暂无"
        }
        if dic["receiverSex"] as! Int == 1{
            self.receiverSex = true
        }else{
            self.receiverSex = false
        }
        return true
    }
}