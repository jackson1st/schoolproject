//
//  ActivityView.swift
//  学子超市
//
//  Created by jason on 16/4/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
//用来显示活动
class ActivityView: UIView {
    var imgView:UIImageView!
    var labelName:UILabel!
    init() {
        super.init(frame: CGRectZero)
        imgView = UIImageView()
        labelName = UILabel()
        labelName.font = UIFont.boldSystemFontOfSize(14)
        labelName.textColor = UIColor.ActivityNameColor()
        self.addSubview(imgView)
        self.addSubview(labelName)
    }
    
    convenience init(activity:Activity){
        self.init()
        setContent(activity)
    }
    
    func setContent(activity:Activity){
        switch activity {
        case .redBag(let object):
            imgView.image = UIImage(named: "Active_returnS")
            labelName.text = object.title
        case .ticketGood(let object):
            imgView.image = UIImage(named: "Active_Change")
            labelName.text = object.title
        case .afterReduction(let object):
            imgView.image = UIImage(named: "Active_ReductionS")
            labelName.text = object.title
        case .afterSend(let object):
            imgView.image = UIImage(named: "Active_SendS")
            labelName.text = object.title
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imgView.sizeToFit()
        labelName.sizeToFit()
        imgView.frame.origin = CGPointMake(0, 0)
        labelName.frame.origin = CGPointMake(imgView.frame.maxX + 5, 0)
        self.frame.size = CGSizeMake(labelName.frame.origin.y + labelName.frame.width, imgView.frame.height)
        labelName.center.y = self.frame.height / 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
