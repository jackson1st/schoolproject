//
//  MuliPageHeaderView.swift
//  MulitPageViewControllerTest
//
//  Created by jason on 16/3/20.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

 protocol MuliPageHeaderViewDelegate:NSObjectProtocol{
    func pageSelectedOfIndex(index:Int)
    func pageSelected(to:Int,from:Int)
}
enum MuliPageHeaderViewStyle {
    case HotKey//有一个圆角边框，无bottomLine
    case Page //没有边框，有Bottomline
    case Active//有图片
    case Size//用于规格选择
}

class MuliPageHeaderView: UIScrollView {
    

    private var pageButtons = [UIButton]()
    private var pageButtonView = [UIView]()
    private var titles = [String]()//不知道如何区分是整个修改还是单个添加
    private var bottomLine:UIView!
    private let screenWidth:CGFloat = UIScreen.mainScreen().bounds.width
    private(set) var selectedIndex = -1
    private var scrollView:UIScrollView {
        let sco = UIScrollView()
        addSubview(sco)
        return sco
    }
    private var LineHeight:CGFloat = 2
    var viewStyle:MuliPageHeaderViewStyle = .Page
    var offSetCorrect:CGFloat = 0 //上下偏移修正，针对超市页面迷bug
    var TitleFont = UIFont.systemFontOfSize(16)
    var space:CGFloat = 10
    weak var pageDelegate:MuliPageHeaderViewDelegate!
    var hilightColor = UIColor.redColor(){
        didSet{
            bottomLine.backgroundColor = hilightColor
            for x in pageButtons{
                x.setTitleColor(hilightColor, forState: .Selected)
            }
            self.layoutIfNeeded()
        }
    }//默认高亮颜色
    var bottomLineEnable:Bool = true {
        didSet{
            bottomLine.hidden = !bottomLineEnable
        }
    }
    
    lazy var shadowBottomLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGrayColor()
        self.addSubview(view)
        view.frame = CGRect(x: 0, y: self.frame.height - 0.5 + self.offSetCorrect, width: self.frame.width, height: 0.5)
        view.hidden = true
        return view
    }()
    //初始化
    
    init(){
        super.init(frame: CGRectZero)
        self.backgroundColor = UIColor.whiteColor()
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.bounces = false
        bottomLine = UIView()
        bottomLine.backgroundColor = hilightColor
        bottomLine.frame = CGRect(x:  bottomLine.frame.origin.x, y: self.frame.height - LineHeight, width: 0, height: LineHeight)
        bottomLine.hidden = !bottomLineEnable
        self.addSubview(bottomLine)
    }
    
    convenience init(titles:[String]){
        self.init()
        setButtons(titles)
    }
    
    convenience init(titles:[String],style:MuliPageHeaderViewStyle){
        self.init()
        self.viewStyle = style
        setButtons(titles)
    }
    
    convenience init(style:MuliPageHeaderViewStyle){
        self.init()
        self.viewStyle = style
    }

    //留坑，自定义view如何支持xib
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func addButton(title:String){
        let btn = UIButton()
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        btn.setTitle(title, forState: .Normal)
        btn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn.setTitleColor(hilightColor, forState: .Selected)
        btn.setTitleColor(hilightColor, forState: .Highlighted)
        btn.titleLabel?.font = TitleFont
        btn.tag = pageButtons.count
        btn.addTarget(self, action: #selector(MuliPageHeaderView.ButtonClicked(_:)), forControlEvents: .TouchUpInside)
        pageButtons.append(btn)
        pageButtonView.append(view)
        view.addSubview(btn)
        self.addSubview(view)
    }
    
    func doSome(sender:UIButton){
        print("cao")
    }
    
    func removeAllPageButton(){
        while(pageButtons.isEmpty == false){
            pageButtons.first!.removeFromSuperview()
            pageButtons.removeFirst()
            pageButtonView.first!.removeFromSuperview()
            pageButtonView.removeFirst()
        }
    }
    
    func removeButtonAtIndex(index:Int){
        pageButtons[index].removeFromSuperview()
        pageButtons.removeAtIndex(index)
    }
    
    func removeButton(object:UIButton){
        removeButtonAtIndex(pageButtons.indexOf(object)!)
    }
    
    func setButtons(titles:[String]){
        removeAllPageButton()
        for x in titles{
            addButton(x)
        }
    }
    //同时会通知代理
    func scrollToPageAtIndex(index:Int){
        ButtonClicked(pageButtons[index])
    }
    //滚动不通知代理
    func scollToPageAtIndexWithoutDelegate(index:Int){
        guard pageButtons.count > 0 else {return}
        switchSelectedButton(pageButtons[index])
        moveBottomLineToButton(pageButtons[index])
    }
    
    //先一次完成，之后再添加确认
    func ButtonClicked(sender:UIButton){
        pageDelegate.pageSelected(sender.tag, from: selectedIndex)
        switchSelectedButton(sender)
        moveBottomLineToButton(sender)
        pageDelegate.pageSelectedOfIndex(sender.tag)
    }
    
    func moveBottomLineToButton(sender:UIButton){
        
        moveBottomLineToxWithAnimation(pageButtonView[selectedIndex].frame.origin.x,animation: true,length: pageButtonView[selectedIndex].frame.width)
    }
    
    func moveBottomLineToxWithAnimation(x:CGFloat,animation:Bool,length:CGFloat = -1){
        if(animation == true){
            dispatch_async(dispatch_get_main_queue()) { [weak self] in
                UIView.animateWithDuration(0.1) { () -> Void in
                    if(length != -1 ){
                        self?.bottomLine.frame.size.width = length
                    }
                    self?.bottomLine.frame.origin.x = x
                }
            }
        }else{
            if(length != -1 ){
                self.bottomLine.frame.size.width = length
            }
            bottomLine.frame.origin.x = x
        }
        
    }
    
    private func switchSelectedButton(sender:UIButton){
        if(viewStyle == .HotKey || viewStyle == .Size){
            pageButtons[selectedIndex].layer.borderColor = UIColor.lightGrayColor().CGColor
            sender.layer.borderColor = hilightColor.CGColor
        }
        pageButtons[selectedIndex].selected = false
        sender.selected = true
        selectedIndex = sender.tag
    }
    
    override func layoutSubviews() {
        let Width = self.frame.width/CGFloat(pageButtons.count)
        var i:CGFloat = 0
        if(self.viewStyle == .Size){
            //不需要均分，合适就好
            for x in 0..<pageButtons.count{
                let view = pageButtonView[x]
                let btn = pageButtons[x]
                let buttonSize = (btn.titleLabel?.text!.sizeWithAttributes([NSFontAttributeName: (btn.titleLabel?.font)!]))!
                let buttonWidth = buttonSize.width + space
                view.frame = CGRect(x: i, y: offSetCorrect, width: buttonWidth , height: frame.height)
                btn.frame.size = CGSize(width: buttonSize.width + 10, height: buttonSize.height + 14)
                btn.center = CGPoint(x: buttonWidth/2, y: frame.height/2)
                i += buttonWidth
            }
        }else{
            for x in 0..<pageButtons.count{
                let view = pageButtonView[x]
                let btn = pageButtons[x]
                let buttonSize = (btn.titleLabel?.text!.sizeWithAttributes([NSFontAttributeName: (btn.titleLabel?.font)!]))!
                var buttonWidth = buttonSize.width
                buttonWidth = (buttonWidth + space) < Width ? Width:(buttonWidth + space)
                view.frame = CGRect(x: i, y: offSetCorrect, width: buttonWidth, height: frame.height)
                btn.frame.size = CGSize(width: buttonSize.width + 10, height: buttonSize.height + 14)
                btn.center = CGPoint(x: buttonWidth/2, y: frame.height/2)
                i += buttonWidth
            }
        }
        contentSize.width = i + 10
        if(abs(contentSize.width - frame.width) < 15){
            contentSize.width = frame.width
        }
        if(selectedIndex == -1){
            selectedIndex = 0
        }
        shadowBottomLine.frame  = CGRect(x: 0, y: self.frame.height - 0.5 + self.offSetCorrect, width: self.frame.width, height: 0.5)
        bottomLine.frame = CGRect(x:  bottomLine.frame.origin.x, y: self.frame.height - LineHeight + offSetCorrect, width: bottomLine.frame.width, height: LineHeight)
        if(self.viewStyle == .HotKey || self.viewStyle == .Size){
            bottomLine.hidden = true
             for x in pageButtons{
                x.setTitleColor(UIColor.ButtonHotKeyColor(), forState: .Normal)
                x.layer.borderColor = UIColor.ButtonHotKeyColor().CGColor
                x.layer.cornerRadius = 6
                x.layer.borderWidth = 1
            }
        }
        scollToPageAtIndexWithoutDelegate(selectedIndex)
    }

}
