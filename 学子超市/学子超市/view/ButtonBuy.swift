//
//  ButtomBuy.swift
//  学子超市
//
//  Created by Jason on 16/3/16.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

protocol ButtonBuyDelegate: NSObjectProtocol{
    func dec(sender:UIButton,oldNum:Int,newNum:Int)
    func inc(sender:UIButton,oldNum:Int,newNum:Int)
}

class ButtonBuy: UIView {
    let buttonHeight = ScreenHeight * 0.053
    let buttonWidth = ScreenWidth * 0.25
    var num:Int = 0{
        didSet{
            if(num == 0){
                buttonDec.hidden = true
                self.constraint.constant = buttonWidth - buttonHeight
                self.layoutIfNeeded()
                labelNum.hidden = true
            }else{
                if(oldValue == 0){
                    
                    buttonDec.hidden = false
                    UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseIn, animations: { () -> Void in
                        self.constraint.constant = 0
                        self.layoutIfNeeded()
                        }, completion: { (_) -> Void in
                            self.labelNum.hidden = false
                    })
                }
            }
            labelNum.text = "\(num)"
        }
    }
    weak var delegate:ButtonBuyDelegate!
    lazy var labelNum:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.textAlignment = .Center
        label.font = UIFont.systemFontOfSize(14)
        label.textColor = UIColor.TextNormalBlackColor()
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.snp_makeConstraints(closure: { (make) -> Void in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self)
        })
        label.sizeToFit()
        return label
    }()
    
    var buttonInc:UIButton!
    var constraint:NSLayoutConstraint!
    var buttonDec:UIButton!
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.backgroundColor = UIColor.clearColor()
        buttonInc = UIButton()
        buttonInc.setImage(UIImage(named: "inc.png"), forState: .Normal)
        buttonInc.addTarget(self, action: #selector(ButtonBuy.buttonIncClicked(_:)), forControlEvents: .TouchUpInside)
        self.addSubview(buttonInc)
        buttonInc.translatesAutoresizingMaskIntoConstraints = false
        buttonInc.snp_makeConstraints(closure: { (make) -> Void in
            make.centerY.equalTo(self)
            make.right.equalTo(self)
            make.height.width.equalTo(buttonHeight)
        })
        buttonDec = UIButton()
        buttonDec.setImage(UIImage(named: "dec"), forState: .Normal)
        buttonDec.addTarget(self, action: #selector(ButtonBuy.buttondecClicked(_:)), forControlEvents: .TouchUpInside)
        self.addSubview(buttonDec)
        buttonDec.translatesAutoresizingMaskIntoConstraints = false
        buttonDec.snp_makeConstraints(closure: { (make) -> Void in
            make.centerY.equalTo(self)
            make.height.width.equalTo(buttonHeight)
        })
        self.constraint = NSLayoutConstraint(item: buttonDec, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: buttonWidth - buttonHeight)
        self.addConstraint(self.constraint)
        buttonDec.hidden = true

        
        self.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(buttonInc)
            make.width.equalTo(buttonWidth)
        }
        
    }

    
    
    
    func buttonIncClicked(sender:UIButton){
        guard num < 99 else{return}
        num += 1
        labelNum.text = "\(num)"
        delegate.inc(sender, oldNum:num - 1 ,newNum:num)
    }
    
    func buttondecClicked(sender:UIButton){
        num -= 1
        labelNum.text = "\(num)"
        delegate.dec(sender, oldNum:num + 1,newNum:num)
    }
}
