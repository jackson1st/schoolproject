//
//  StarView.swift
//  学子超市
//
//  Created by jason on 16/3/17.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class StarView: UIView {
    var imgHeight:CGFloat = ScreenHeight * 0.015
    var imgWidth:CGFloat = ScreenWidth * 0.139
    
    private var percent:CGFloat = 0 {
        didSet{
            BKView.frame.size.width = imgWidth * percent
        }
    }
    lazy var starView:UIImageView = {
        let view = UIImageView(image: UIImage(named: "star"))
        view.backgroundColor = UIColor.clearColor()
        view.frame.origin = CGPoint(x: 0,y: 0)
        view.frame.size = CGSize(width: self.imgWidth, height: self.imgHeight)
        return view
    }()
    
    lazy var BKView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MainColor()
        view.frame.origin = CGPointMake(0, 0)
        view.frame.size = CGSizeMake(self.imgWidth, self.imgHeight)
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.colorWithRGB(216, g: 216, b: 216)
        self.addSubview(BKView)
        self.addSubview(starView)
        percent = 0
        self.frame.size = CGSize(width: imgWidth, height: imgHeight)
        BKView.frame.size.width = imgWidth * percent
    }
    
    convenience init(){
        self.init(frame: CGRectZero)
        self.backgroundColor = UIColor.colorWithRGB(216, g: 216, b: 216)
        self.addSubview(BKView)
        self.addSubview(starView)
        percent = 0
        self.frame.size = CGSize(width: imgWidth, height: imgHeight)
        BKView.frame.size.width = imgWidth * percent
    }
    
    func setSize(width:CGFloat,height:CGFloat){
        imgHeight = height
        imgWidth = width
        self.layoutIfNeeded()
    }
    
    convenience init(percent:CGFloat){
        self.init()
        setPercent(percent)
    }
    
    convenience init(p:CGFloat,sum:CGFloat){
        self.init(percent:p/sum)
    }
    
    func setPercent(p:CGFloat){
        percent = p
    }
    
    func setPercent(p:CGFloat,sum:CGFloat){
        percent = p/sum
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame.size = CGSize(width: imgWidth, height: imgHeight)
        BKView.frame.size = self.frame.size
        starView.frame.size = self.frame.size
        BKView.frame.size.width = imgWidth * percent
       
    }
    
    
}
