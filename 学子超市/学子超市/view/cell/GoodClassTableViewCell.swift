//
//  GoodClassTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/30.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class GoodClassTableViewCell: UITableViewCell,Reusable {

    lazy var imgLine:UIImageView! = {
        let imgLine = UIImageView(image: UIImage(named: "Line"))
        imgLine.sizeToFit()
        imgLine.center.y = self.frame.size.height / 2
        imgLine.frame.origin.x = 3
        imgLine.hidden = false
        self.addSubview(imgLine)
        return imgLine
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.textLabel?.frame.origin.y = 15
        self.textLabel?.sizeToFit()
        self.textLabel?.textAlignment = .Center
        self.textLabel?.font = UIFont.systemFontOfSize(14)
        self.selectionStyle = .None
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        switch(selected){
        case true:
            imgLine.hidden = false
            self.backgroundColor = UIColor.whiteColor()
        default:
            imgLine.hidden = true
            self.backgroundColor = UIColor.ViewMBKColor()
        }
    }

}
