//
//  EVATableViewCell.swift
//  学子超市
//
//  Created by jason on 16/4/1.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class EVATableViewCell: UITableViewCell,Reusable {

    //视图
    class ScoreView:UIView{
        private let labelTitle = UILabel()
        private let startView = StarView()
        var title:String!{
            didSet{
                labelTitle.text = title
                labelTitle.sizeToFit()
            }
        }
        var percent:CGFloat!{
            didSet{
                startView.setPercent(percent)
            }
        }
        init(title:String){
            super.init(frame:CGRectZero)
            labelTitle.text = title
            labelTitle.sizeToFit()
            self.percent = 0
            labelTitle.textColor = UIColor.ButtonCancelColor()
            labelTitle.font = UIFont.systemFontOfSize(9.5)
            self.addSubview(labelTitle)
            self.addSubview(startView)
            self.frame.size = CGSizeMake(88.125, 11.3369140625)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            labelTitle.center.y = self.frame.height / 2
            labelTitle.frame.origin.x = 0
            startView.center.y = self.frame.height / 2
            startView.frame.origin.x = labelTitle.frame.maxX + 2
            frame.size.width = startView.frame.maxX
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    class Reply:UIView{
        private let labelTitle = UILabel()
        private let labelContent = UILabel()
        private let labelDate = UILabel()
        func setContent(content:String,date:String){
            labelContent.text = content
            labelContent.sizeToFit()
            labelDate.text = date
            labelDate.sizeToFit()
        }
        
        init() {
            super.init(frame: CGRectZero)
            self.backgroundColor = UIColor.colorWithRGB(239, g: 239, b: 239)
            self.addSubview(labelTitle)
            self.addSubview(labelContent)
            self.addSubview(labelDate)
            labelTitle.text = "商家回复:"
            labelTitle.font = UIFont.systemFontOfSize(14)
            labelTitle.textColor = UIColor.colorWithRGB(74, g: 74, b: 74)
            labelTitle.sizeToFit()
            labelContent.font = UIFont.systemFontOfSize(14)
            labelContent.textColor = UIColor.colorWithRGB(74, g: 74, b: 74)
            labelDate.font = UIFont.systemFontOfSize(14)
            labelDate.textColor = UIColor.colorWithRGB(74, g: 74, b: 74)
        }
        override func layoutSubviews() {
            super.layoutSubviews()
            if(labelContent.text != nil){
                labelTitle.frame.origin = CGPointMake(12, 3)
                labelDate.frame.origin = CGPointMake(self.frame.width - 10 - labelDate.frame.width, 3)
                labelContent.frame.origin = CGPoint(x: 12, y: labelTitle.frame.maxY + 3)
                let size = (labelContent.text! as NSString).sizeWithAttributes([NSFontAttributeName:labelContent.font])
                var contentSize = size
                let lineWidth = self.frame.width - 25
                let line:Int = size.width / lineWidth > 3.0 ? 3 : Int(ceil(size.width / lineWidth))
                contentSize.width = lineWidth
                contentSize.height = CGFloat(line) * size.height
                labelContent.frame.size = contentSize
                labelContent.numberOfLines = line
                self.frame.size.height = labelContent.frame.maxY + 3
//                print(size.height)
//                print(self.frame.size.height)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    private let ScoreGood = ScoreView(title: "商品")
    private let ScoreSpeed = ScoreView(title: "速度")
    private let userName = UILabel()
    private let headPhoto = UIImageView()
    private let comContent = UILabel()
    private let comDate = UILabel()
    private var reply = Reply()
    //数据
    var object:Comment?{
        didSet{
            if let object = object{
            ScoreGood.percent = object.scoreGood/5
            ScoreSpeed.percent = object.scoreSpeed/5
            userName.text = (object.userName as NSString).stringByReplacingCharactersInRange(NSMakeRange(1, object.userName.characters.count - 2), withString: "****")
            headPhoto.sd_setImageWithURL(NSURL(string: object.photoURL))
            comContent.text = object.comContent
            comDate.text = object.date
            comDate.sizeToFit()
                if(object.storeReply != nil){
                    reply.setContent(object.storeReply!.content, date: object.storeReply!.time)
                    reply.hidden = false
                }else{
                    reply.hidden = true
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(ScoreGood)
        self.addSubview(ScoreSpeed)
        self.addSubview(userName)
        userName.textColor = UIColor.blackColor()
        userName.font = UIFont.systemFontOfSize(12)
        self.addSubview(headPhoto)
        headPhoto.frame.size = CGSize(width: 16, height: 16)
        headPhoto.layer.cornerRadius = 8
        headPhoto.clipsToBounds = true
        self.addSubview(comContent)
        comContent.textColor = UIColor.blackColor()
        comContent.font = UIFont.systemFontOfSize(14)
        comContent.sizeToFit()
        self.addSubview(comDate)
        comDate.textColor = UIColor.ButtonCancelColor()
        comDate.font = UIFont.systemFontOfSize(12)
        comDate.sizeToFit()
        self.addSubview(reply)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(object != nil ){
            ScoreGood.frame.origin = CGPoint(x: 12, y:10)
            ScoreSpeed.frame.origin = CGPoint(x: ScoreGood.frame.maxX + 10, y: 10)
            userName.sizeToFit()
            userName.frame.origin = CGPoint(x: self.frame.width - 40 - userName.frame.width, y: 5)
            headPhoto.frame.origin = CGPoint(x: userName.frame.maxX + 5, y: 5)
            comContent.frame.origin = CGPoint(x: 12, y: 24)
            let size = (comContent.text! as NSString).sizeWithAttributes([NSFontAttributeName:comContent.font])
            var contentSize = size
            let lineWidth = self.frame.width - 25
            let line:Int = size.width / lineWidth > 3.0 ? 3 : Int(ceil(size.width / lineWidth))
            contentSize.width = lineWidth
            contentSize.height = CGFloat(line) * size.height
            comContent.frame.size = contentSize
            comContent.numberOfLines = line
            comDate.frame.origin = CGPoint(x: self.frame.width - 15 - comDate.frame.width, y: comContent.frame.maxY + 2)
            self.contentView.frame.size.height = comDate.frame.maxY + 5
            if let _ = object?.storeReply{
                reply.frame.origin.x = 15
                reply.frame.origin.y = comDate.frame.maxY + 5
                reply.frame.size.width = self.frame.width - 30
                reply.layoutIfNeeded()
                self.contentView.frame.size.height = reply.frame.maxY + 5
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
