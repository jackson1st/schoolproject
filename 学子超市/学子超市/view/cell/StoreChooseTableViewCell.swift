//
//  StoreChooseTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/4/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class StoreChooseTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var ImgPicture: UIImageView!
      var LabelName: UILabel = UILabel()
      var LabelDistance: UILabel = UILabel()
      var starView: StarView = StarView()
      var LabelScore: UILabel = UILabel()
      var LabelFeeEnableSend: UILabel = UILabel()
      var LabelFeeSend: UILabel = UILabel()
    var activities = [ActivityView]()
    var dataObject:Store?{
        didSet{
            let dataObject = self.dataObject!
            ImgPicture.sd_setImageWithURL(NSURL(string: dataObject.photoURL))
            LabelName.text = dataObject.name
            LabelDistance.text = String(format: "(距离%.f米)",dataObject.getDistance(CLLocation(latitude: 116.403875, longitude: 39.915168)))
            //从偏好设置当中读取当前的经纬度
            starView.setPercent(CGFloat(dataObject.score), sum: 5)
            LabelScore.text = String(format: "%.1f",dataObject.score)
            LabelFeeEnableSend.attributedText = dataObject.getFeeEnableSendStr()
            LabelFeeSend.attributedText = dataObject.getFeeSendStr()
            for _ in 0..<(dataObject.activities.count - activities.count < 0 ? 0 : dataObject.activities.count - activities.count) {
                let view = ActivityView()
                self.contentView.addSubview(view)
                activities.append(view)
            }
            for x in activities{
                x.hidden = true
            }
            for i in 0..<dataObject.activities.count{
                activities[i].setContent(dataObject.activities[i])
            }
            setNeedsLayout()
        }
    }
    var enterAction = {
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        ImgPicture.clipsToBounds = true
        
        ImgPicture.layer.borderColor = UIColor.lightGrayColor().CGColor
        ImgPicture.layer.borderWidth = 1
        contentView.addSubview(LabelName)
        contentView.addSubview(LabelDistance)
        contentView.addSubview(LabelFeeEnableSend)
        contentView.addSubview(LabelFeeSend)
        contentView.addSubview(starView)
        contentView.addSubview(LabelScore)
        
        LabelDistance.textColor = UIColor.lightGrayColor()
        LabelDistance.font = UIFont.systemFontOfSize(14)
        LabelName.font = UIFont.systemFontOfSize(16)
        
        LabelScore.textColor = UIColor.redColor()
        LabelScore.font = UIFont.boldSystemFontOfSize(18)
    }
    // 10 + 19 + 5 + (5) + (21.5 - LabelFeeEnableSend) + (18 - LabelFeeSend) + (30 - ButtonEnter) + (23 - starView)
    override func layoutSubviews() {
        super.layoutSubviews()
        ImgPicture.frame = CGRectMake(10, 10, 60, 60)
        LabelName.sizeToFit()
        LabelName.frame.origin = CGPointMake(ImgPicture.frame.maxX + 10, 10)
        LabelDistance.sizeToFit()
        LabelDistance.frame.origin = CGPointMake(LabelName.frame.maxX + 1, 10)
        LabelDistance.center.y = LabelName.center.y
        //作一下超出判定
        if let dis:CGFloat? = LabelDistance.frame.maxX - ScreenWidth + 5 where dis > 0.0 {
            LabelName.frame.size.width -= dis!
            LabelDistance.frame.origin.x -= dis!
        }
        starView.layoutIfNeeded()
        starView.frame.origin = CGPointMake(LabelName.frame.origin.x, LabelName.frame.maxY + 5)
        LabelScore.sizeToFit()
        LabelScore.frame.origin = CGPointMake(starView.frame.maxX + 5, starView.frame.origin.y)
        LabelScore.center.y = starView.center.y
        var Y = LabelScore.frame.maxY + 2
        var X:CGFloat = LabelName.frame.origin.x
        LabelFeeEnableSend.sizeToFit()
        X = ScreenWidth - 10 - LabelFeeEnableSend.frame.width
        LabelFeeEnableSend.frame.origin = CGPointMake(X,LabelName.frame.maxY + 2)
        LabelFeeSend.sizeToFit()
        LabelFeeSend.frame.origin = CGPointMake(X, LabelFeeEnableSend.frame.maxY + 5)
        LabelFeeSend.center.x = LabelFeeEnableSend.center.x
        X = LabelName.frame.origin.x
        for i in 0..<(dataObject?.activities.count ?? Int(0)){
            let x = activities[i]
            x.hidden = false
            x.layoutIfNeeded()
            x.frame.origin = CGPointMake(X, Y)
            if let dis:CGFloat? = x.labelName.frame.maxX - LabelFeeEnableSend.frame.origin.x - 10 where dis > 0.0 {
                x.labelName.frame.size.width -= dis!
            }
            Y = x.frame.maxY + 2
        }
        self.frame.size = CGSizeMake(ScreenWidth, max(LabelFeeSend.frame.maxY + 10,Y) + 5)
    }
    
    func ButtonEnterClicked() {
        enterAction()
    }
    
}
