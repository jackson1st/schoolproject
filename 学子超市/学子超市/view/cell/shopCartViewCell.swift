//
//  ShopCartViewCell.swift
//  学子超市
//
//  Created by Jason on 16/3/16.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class ShopCartViewCell: UITableViewCell,Reusable,ButtonBuyDelegate{
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var LabelPrice: UILabel!
    var title:String!{
        didSet{
            LabelTitle.text = title
        }
    }
    var price:Double!{
        didSet{
            LabelPrice.text = "¥" + String(format: "%.1f",price)
        }
    }
    var num:Int!{
        didSet{
            ButtonIO.num = num
        }
    }
    @IBOutlet weak var ButtonIO: ButtonBuy!
    var index:Int!
    var incAction:((sender:UIButton,oldNum:Int,newNum:Int,cell:ShopCartViewCell) -> Void)!
    var decAction:((sender:UIButton,oldNum:Int,newNum:Int,cell:ShopCartViewCell) -> Void)!
    
    static var nib: UINib? {
        return  UINib(nibName: "ShopCartViewCell", bundle: NSBundle.mainBundle())
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        ButtonIO.delegate = self
        LabelPrice.textColor = UIColor.MainColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func inc(sender: UIButton, oldNum: Int, newNum: Int) {
        incAction(sender: sender,oldNum: oldNum,newNum:newNum,cell:self)
    }
    
    func dec(sender: UIButton, oldNum: Int, newNum: Int) {
        decAction(sender: sender,oldNum: oldNum,newNum:newNum,cell:self)
    }
    
}
