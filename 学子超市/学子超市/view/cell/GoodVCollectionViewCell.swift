//
//  GoodVCollectionViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/27.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
class HomeCollectionHeaderView: UICollectionReusableView {
    let titleLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.text = "热卖促销"
        titleLabel.textAlignment = NSTextAlignment.Left
        titleLabel.font = UIFont.systemFontOfSize(14)
        titleLabel.frame = CGRectMake(10, 0, 200, 20)
        titleLabel.textColor = UIColor.colorWithRGB(150, g: 150, b: 150)
        addSubview(titleLabel)
    }
    
    func setTitle(title:String){
        self.titleLabel.text = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SecondClassCollectionHeaderView: UICollectionReusableView {
    
    var PageView:MuliPageHeaderView!
    var delegate:MuliPageHeaderViewDelegate!{
        didSet{
            PageView.pageDelegate = delegate
        }
    }
//    var title:[GoodClass.classInfo]!{
//        didSet{
//            var titles = [String]()
//            for x in title{
//                titles.append(x.name)
//            }
//            PageView.setButtons(titles)
//        }
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        PageView = MuliPageHeaderView()
        PageView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        PageView.bottomLineEnable = false
        self.addSubview(PageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class GoodVCollectionViewCell: UICollectionViewCell,ButtonBuyDelegate{

    lazy var ImgView: UIImageView! = {
        let view = UIImageView()
        return view
    }()
    var mode = 1
    var LabelTitle: UILabel!
    var LabelRealPrice: UILabel!
    var LabelOriPrice: UILabel!
    var LabelSell: UILabel!
    var imgViewTag:UIImageView = UIImageView()
    var buttonChooseSize = UIButton()
    var buttonBuy = ButtonBuy()
    
    var shopCartObject:ShopCartGood?
    var object:Commodity!{
        didSet{
            let specsGood = object.mGoodsSpecs.firstObject as! GoodsSpecs
            self.shopCartObject = ShopCartGood(good: object, specsID: specsGood.id, specDescription: specsGood.specDescription, price: specsGood.currentPrice)
            if specsGood.imageUrl != nil {
                ImgView.sd_setImageWithURL(NSURL(string: specsGood.imageUrl)!)
            }else{
                ImgView.sd_setImageWithURL(NSURL(string: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg")!)
            }
            LabelTitle.text = object.name
            if(specsGood.promotivePrice == 0){
                LabelOriPrice.hidden = true
                LabelRealPrice.attributedText = specsGood.currentPriceStr
            }else{
                LabelRealPrice.attributedText = specsGood.promotivePriceStr
                LabelOriPrice.attributedText = specsGood.currentPriceStr
            }
            LabelSell.text = "已售\(specsGood.buySum)件"
            if(object.isMultiSpec == true){
                buttonBuy.hidden = true
                buttonChooseSize.hidden = false
            }else{
                buttonBuy.hidden = false
                buttonChooseSize.hidden = true
                buttonBuy.num = object.buied
            }
            
//            imgViewTag.image = UIImage(named: "activeTag")
//            
//            if(object.defaultSize?.active == 0){
//                imgViewTag.hidden = true
//            }else{
//                imgViewTag.hidden = false
//            }
            
        }
    }
    
    func chooseSize(){
        NSNotificationCenter.defaultCenter().postNotificationName(NotiPushAlertSizeChoose, object: nil, userInfo: ["object":object])
    }
    
    func dec(sender:UIButton,oldNum:Int,newNum:Int){
        object.buied -= 1
        ShopCartGood.decOne(self.shopCartObject!)
        NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":object.id,"num":object.buied])
    }
    func inc(sender:UIButton,oldNum:Int,newNum:Int){
        print("多一个")
        object.buied += 1
        ShopCartGood.addOne(self.shopCartObject!)
        NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":object.id,"num":object.buied,"view":sender])
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.whiteColor()
        LabelTitle = UILabel()
        LabelTitle.numberOfLines = 2
        self.contentView.addSubview(LabelTitle)
        LabelRealPrice = UILabel()
        self.contentView.addSubview(LabelRealPrice)
        LabelOriPrice = UILabel()
        self.contentView.addSubview(LabelOriPrice)
        LabelSell = UILabel()
        self.contentView.addSubview(ImgView)
        self.contentView.addSubview(imgViewTag)
        self.contentView.addSubview(buttonBuy)
        buttonBuy.snp_makeConstraints(closure: { (make) in
            make.right.equalTo(self).offset(-5)
            make.bottom.equalTo(self)
        })
        self.contentView.addSubview(LabelSell)
        self.contentView.addSubview(buttonChooseSize)
        ImgView.contentMode = UIViewContentMode.ScaleAspectFit
        
        if #available(iOS 8.2, *) {
            LabelTitle.font = UIFont.systemFontOfSize(16, weight: 0.05)
        } else {
            LabelTitle.font = UIFont.systemFontOfSize(16)
        }
        LabelSell.textColor = UIColor.lightGrayColor()
        LabelSell.font = UIFont.systemFontOfSize(14)
        buttonChooseSize.setTitle("选择规格", forState: .Normal)
        buttonChooseSize.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        buttonChooseSize.titleLabel?.font = UIFont.systemFontOfSize(14)
        buttonChooseSize.layer.cornerRadius = 4
        buttonChooseSize.layer.borderWidth = 0.7
        buttonChooseSize.layer.borderColor = UIColor.lightGrayColor().CGColor
        buttonChooseSize.addTarget(self, action: #selector(GoodVCollectionViewCell.chooseSize), forControlEvents: .TouchUpInside)
        buttonBuy.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if( mode == 1){
        ImgView.frame = CGRect(x: 0, y: 5, width: self.frame.width, height: 130)
        LabelTitle.frame = CGRect(x: 3, y: ImgView.frame.maxY + 2, width: self.frame.width, height: 20)
            if((LabelTitle.text! as NSString).sizeWithAttributes([NSFontAttributeName: LabelTitle.font]).width / LabelTitle.frame.width > 1){
                LabelTitle.frame.size.height = 40
            }else{
                LabelTitle.frame.size.height = 20
            }
        imgViewTag.frame.origin = CGPoint(x: 3, y: ImgView.frame.maxY + 44)
        imgViewTag.sizeToFit()
        LabelRealPrice.frame.origin = CGPoint(x: 3, y: imgViewTag.frame.maxY + 4)
        LabelRealPrice.sizeToFit()
        LabelOriPrice.frame.origin = CGPoint(x: LabelRealPrice.frame.maxX + 5, y: LabelRealPrice.frame.origin.y + 2)
        LabelOriPrice.center.y = LabelOriPrice.center.y
        LabelOriPrice.sizeToFit()
        LabelSell.sizeToFit()
        LabelSell.frame.origin = CGPoint(x: 3, y: self.frame.height - 6 - LabelSell.frame.height)
        buttonChooseSize.frame = CGRect(x: self.frame.width - ScreenWidth * 0.20, y: LabelSell.frame.origin.y, width: ScreenWidth * 0.18, height: ScreenHeight * 0.033)
        buttonChooseSize.center.y = LabelSell.center.y
        }else{
            ImgView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            ImgView.center.y = self.frame.height / 2
            let x = ImgView.frame.maxX + 5
            LabelTitle.frame = CGRect(x: x, y: 0, width: self.frame.width, height: 20)
            imgViewTag.frame.origin = CGPoint(x: x, y: LabelTitle.frame.maxY + 2)
            imgViewTag.sizeToFit()
            LabelRealPrice.frame.origin = CGPoint(x: x, y: imgViewTag.frame.maxY + 2)
            LabelRealPrice.sizeToFit()
            LabelOriPrice.frame.origin = CGPoint(x: LabelRealPrice.frame.maxX + 5, y: LabelRealPrice.frame.origin.y + 2)
            LabelOriPrice.center.y = LabelOriPrice.center.y
            LabelOriPrice.sizeToFit()
            LabelSell.frame.origin = CGPoint(x: x, y: LabelRealPrice.frame.maxY + 5)
            LabelSell.sizeToFit()
            buttonChooseSize.frame = CGRect(x: self.frame.width - ScreenWidth * 0.20, y: LabelSell.frame.origin.y, width: ScreenWidth * 0.18, height: ScreenHeight * 0.033)
        }
    }

}
