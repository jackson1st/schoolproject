//
//  SizeTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/27.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class SizeTableViewCell: UITableViewCell,MuliPageHeaderViewDelegate,Reusable{

    @IBOutlet weak var LabelTitle: UILabel!
    var PageViewSizeChoose: MuliPageHeaderView!
    static var nib: UINib? { return UINib(nibName: "SizeTableViewCell", bundle: NSBundle.mainBundle())}
    var sizeType:String!{
        didSet{
            LabelTitle.text = sizeType
        }
    }
    var sizes:[String]!{
        didSet{
            PageViewSizeChoose.setButtons(sizes)
        }
    }
    var selectedAction:(to:String,from:String) -> Void = {
        to,from in
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LabelTitle.frame = CGRect(x: 15, y: 5, width: ScreenWidth, height: 20)
        PageViewSizeChoose = MuliPageHeaderView()
        PageViewSizeChoose.backgroundColor = self.contentView.backgroundColor
        self.addSubview(PageViewSizeChoose)
        PageViewSizeChoose.frame = CGRect(x: 15, y: 25, width: ScreenWidth, height: 60)
        PageViewSizeChoose.hilightColor = UIColor.MainColor()
        PageViewSizeChoose.viewStyle = .Size
        PageViewSizeChoose.pageDelegate = self
        PageViewSizeChoose.space = 20
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func pageSelectedOfIndex(index: Int) {
        
    }
    
    func pageSelected(to: Int, from: Int) {
        selectedAction(to: sizes[to],from: sizes[from])
    }
    
}
