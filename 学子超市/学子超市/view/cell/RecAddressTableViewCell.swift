//
//  RecAddressTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RecAddressTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelSex: UILabel!
    @IBOutlet weak var LabelPhone: UILabel!
    @IBOutlet weak var LabelDetail: UILabel!
    static var nib: UINib? { return UINib(nibName: "RecAddressTableViewCell", bundle: NSBundle.mainBundle())}
    let checkImgView = UIImageView(image: UIImage(named: "selected.png"))
    let unCheckImgView = UIImageView(image: UIImage(named: "imSelected.png"))
    let editingImgView = UIImageView(image: UIImage(named: "edit.png"))
    var checkMark = false{
        didSet{
            self.accessoryView = checkMark == true ? checkImgView : unCheckImgView
        }
    }
    var edit:Bool = false{
        didSet{
            self.accessoryView = editingImgView
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryView = UIImageView(image: UIImage(named: "imSelected.png"))
        self.editingAccessoryView = UIImageView(image: UIImage(named: "edit.png"))
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.checkMark = selected
    }
    
    
    
}
