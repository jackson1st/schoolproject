//
//  XZTTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class XZTableViewCell: UITableViewCell,Reusable {

    //Label的两个约束
    @IBOutlet weak var ConstraintLeftToLeft: NSLayoutConstraint!
    @IBOutlet weak var ConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var LabelTitle: UILabel!
    var title:String!{
        didSet{
            LabelTitle.text = title
        }
    }
    //会有内存泄露
    var ViewExtend: UIView?{
        didSet{
            LabelTitle.sizeToFit()
            self.addSubview(ViewExtend!)
            ViewExtend!.frame = CGRectMake(LabelTitle.frame.maxX + 15, 0, self.frame.width - LabelTitle.frame.width - 15, self.frame.height)
        }
    }
    var textField:UITextField!{
        get{
            if let field = ViewExtend {
                return field as! UITextField
            }else{
                ViewExtend = UITextField()
                ViewExtend?.frame.origin.x = 110
                return ViewExtend as! UITextField
            }
        }
    }
    var info:String!{
        didSet{
            if let textLabel = ViewExtend{
                (textLabel as! UILabel).text = info
            }else{
                self.accessoryType = .DisclosureIndicator
                let label = UILabel()
                label.font = UIFont.systemFontOfSize(16)
                label.textColor = UIColor.MainColor()
                label.text = info
                label.sizeToFit()
                let x = self.contentView.frame.width - 45 - label.frame.width
                ViewExtend = label
                ViewExtend?.frame.origin.x = x
            }
        }
    }
    
    static var nib:UINib? {return UINib(nibName: "XZTableViewCell", bundle: NSBundle.mainBundle())}
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
