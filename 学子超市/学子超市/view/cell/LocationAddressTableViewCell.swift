//
//  LocationAddressTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/3/22.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class LocationAddressTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var DetailLabel: UILabel!
    @IBOutlet weak var TextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib? { return UINib(nibName: "LocationAddressTableViewCell", bundle: NSBundle.mainBundle())}
    
}
