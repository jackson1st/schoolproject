//
//  StoreActivityTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/5/12.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class StoreActivityTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var ImgIcon: UIImageView!
    @IBOutlet weak var LabelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContent(activity:Activity){
        switch activity {
        case .redBag(let object):
            ImgIcon.image = UIImage(named: "Active_return")
            LabelText.text = object.title
        case .afterSend(let object):
            ImgIcon.image = UIImage(named: "Active_Send")
            LabelText.text = object.title
        case .afterReduction(let object):
            ImgIcon.image = UIImage(named: "Active_Reduction")
            LabelText.text = object.title
        case .ticketGood(let object):
            ImgIcon.image = UIImage(named: "Active_Change")
            LabelText.text = object.title
        }
    }
    
}
