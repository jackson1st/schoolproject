//
//  TicketTableViewCell.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell,Reusable{
    enum TicketTableViewCellStyle{
        case RedBagCheck
        case RedBagGet
        case RedBagUse
        case TicketGoodCheck
        case TicketGoodUse
    }
    @IBOutlet weak var ConstraintLeft: NSLayoutConstraint!
    @IBOutlet weak var LabelRedBagIcon: UILabel!
    @IBOutlet weak var RedBackView: UIView!
    @IBOutlet weak var ImgLine: UIImageView!
    @IBOutlet weak var LabelValue: UILabel!
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var LabelDetail: UILabel!//可以来存放兑换码
    @IBOutlet weak var LabelValid: UILabel!
    @IBOutlet weak var TicketState: UIImageView!
    @IBOutlet weak var ButtonGet: UIButton!
    @IBOutlet weak var ButtonCheck: UIButton!
    var style:TicketTableViewCellStyle = .RedBagCheck //cell的类型，默认是红包
    var redBag:RedBag!{
        didSet{
            LabelValue.attributedText = redBag.getValue
            LabelValid.text = redBag.validTime
            LabelTitle.text = redBag.title
            switch style{
            case .RedBagCheck:
                    if(redBag.state == true){
                        TicketState.hidden = true
                        RedBackView.backgroundColor = UIColor.RedBagActiveBKColor()
                    }else{
                        TicketState.hidden = false
                        RedBackView.backgroundColor = UIColor.RedBagNOActiveBKColor()
                }
            case .RedBagGet:
                if(redBag.state == true){
                    TicketState.hidden = true
                    ButtonGet.hidden = false
                }else{
                    TicketState.hidden = false
                    ButtonGet.hidden = true
                    TicketState.image = UIImage(named:"RedBagGeted")
                }
            case .RedBagUse:
                ButtonCheck.hidden = false
            default:break;
            }
        }
    }
    var ticketGood:TicketGood!{
        didSet{
            LabelRedBagIcon.hidden = true
            LabelTitle.text = ticketGood.title
            LabelValid.text = ticketGood.getValidTime
//            LabelValue.text = ticketGood.getGoodInfo
            LabelValue.numberOfLines = 0
            LabelValue.textAlignment = .Center
            LabelDetail.text = ticketGood.getTicketID
            LabelDetail.font = UIFont.systemFontOfSize(20)
            switch style{
            case .TicketGoodCheck:
                if(ticketGood.state == true){
                    TicketState.hidden = true
                    RedBackView.backgroundColor = UIColor.RedBagActiveBKColor()
                }else{
                    TicketState.hidden = false
                    RedBackView.backgroundColor = UIColor.RedBagNOActiveBKColor()
            }
            case .TicketGoodUse:
                ButtonCheck.hidden = false
            default:break
            }
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        RedBackView.layoutIfNeeded()
        ConstraintLeft.constant = (ScreenWidth - 24) * 0.291
    }

    @IBAction func ButtonGetClicked() {
        print("领取")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if(style == .RedBagUse || style == .TicketGoodUse){
            ButtonCheck.hidden = !ButtonCheck.hidden
        }
        // Configure the view for the selected state
    }
    
}
