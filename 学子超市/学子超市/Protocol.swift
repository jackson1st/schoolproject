//
//  SearchProtocol.swift
//  学子超市
//
//  Created by jason on 16/3/21.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation

protocol searchProtocol:NSObjectProtocol{
    func searchUpdating(key:String?)
}
