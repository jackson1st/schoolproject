//
//  SecondClassCollectionViewCell.swift
//  学子超市
//
//  Created by Jason on 16/3/18.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class SecondClassCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var LabelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setTitle(title:String){
        LabelTitle.text = title
        LabelTitle.sizeToFit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame.size = LabelTitle.frame.size
    }

}
