//
//  XZUserDefault.swift
//  学子超市
//
//  Created by jason on 16/4/15.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation

let userIDKey = "userID"
let nicknameKey = "nickname"
let defaultStoreKey = "DefaultStore"
let EnableSendFeeKey = "EnableSendFee"
struct Listener<T>:Hashable{
    let name:String
    typealias Action = T->Void
    let action:Action
    var hashValue: Int{
        return name.hashValue
    }
}
func ==<T>(left:Listener<T>,right:Listener<T>)->Bool{
    return left.name == right.name
}
class Listenable<T>{
    var value:T{
        didSet{
            
        }
    }
    typealias SetterAction = T->Void
    var setterAction:SetterAction
    var listenerSet = Set<Listener<T>>()
    
    func bindListener(name:String,action:Listener<T>.Action){
        let listener = Listener(name: name, action: action)
        listenerSet.insert(listener)
    }
    
    func bindAndFireListener(name:String,action:Listener<T>.Action){
        bindListener(name, action: action)
        action(value)
    }
    
    func removeListenerWithName(name:String){
        for Listener in listenerSet{
            if Listener.name == name{
                listenerSet.remove(Listener)
                break
            }
        }
    }
    
    func removeAllListeners(){
        listenerSet.removeAll(keepCapacity: false)
    }
    
    init(_ v:T,setterAction action:SetterAction){
        value = v
        setterAction = action
    }
}

class XZUserDefaults{
    static var defaults = NSUserDefaults(suiteName: "com.jason.XZ")!
    
    static var isLogined: Bool {
        
        if let _ = XZUserDefaults.userID.value {
            return true
        } else {
            return false
        }
    }
    
    static var userID:Listenable<String?> = {
        let userID = defaults.stringForKey(userIDKey)
        return Listenable<String?>(userID){ userID in
            defaults.setObject(userID, forKey: userIDKey)
        }
    }()
    
    static var nickname:Listenable<String?> = {
        let nickname = defaults.stringForKey(nicknameKey)
        return Listenable<String?>(nickname){nickname in
            defaults.setObject(nickname, forKey: nicknameKey)
        }
    }()
    static var EnableSendFee:Listenable<Double?> = {
        let fee = defaults.doubleForKey(EnableSendFeeKey)
        return Listenable<Double?>(fee) {
            fee in
            defaults.setValue(fee,forKey:EnableSendFeeKey)
        }
    }()
    
}