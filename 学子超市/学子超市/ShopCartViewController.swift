//
//  shopCartViewController.swift
//  学子超市
//
//  Created by Jason on 16/3/16.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import RealmSwift
class shopCartViewController: UIViewController,ShopCartViewDelegate{
    var shopCart = ShopCartGood.getAll()
    let headViewForDetailHeight:CGFloat = 27
    static let instance = shopCartViewController()
    var superViewController: ShopBaseViewController?
    let shopCartView = ShopCartView()
    
    lazy var containerView:UIView = {
        let container = UIView()
        container.backgroundColor = UIColor.clearColor()
        self.view.addSubview(container)
        container.snp_makeConstraints(closure: { make -> Void in
            make.bottom.equalTo(self.shopCartView.buttonDetail.snp_top)
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
        })
        let gesture = UITapGestureRecognizer(target: self, action: #selector(shopCartViewController.hideDetailView))
        gesture.delegate = self
        container.addGestureRecognizer(gesture)
        return container
    }()
    var showingDetail = false
    var price:Double =  0 {
        didSet{
            if(price == 0){
                hideAndDo({ () -> Void in
                    shopCartViewController.instance.showShopCart(false)
                })
            }
            self.shopCartView.price = price
        }
    }
    
    lazy var headViewForDetail: UIView = {
        let headView = UIView()
        headView.backgroundColor = UIColor.whiteColor()
        //不要对headView加约束，否则错乱
        let imgLine = UIImageView(image: UIImage(named: "Line.png"))
        headView.addSubview(imgLine)
        imgLine.snp_makeConstraints(closure: { (make) -> Void in
            make.left.equalTo(headView)
            make.height.equalTo(headView)
            make.width.equalTo(4)
            make.centerY.equalTo(headView)
        })
        
        let label = UILabel()
        label.text = "购物车"
        label.font = UIFont.systemFontOfSize(12)
        label.textColor = UIColor.TextNormalBlackColor()
        headView.addSubview(label)
        label.snp_makeConstraints(closure: { (make) -> Void in
            make.left.equalTo(headView).offset(15)
            make.centerY.equalTo(headView)
        })
        
        let button = UIButton()
        button.setImage(UIImage(named: "dustbin.png"), forState: .Normal)
        button.setTitle("清空购物车", forState: .Normal)
        button.setTitleColor(UIColor.TextNormalBlackColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(12)
        button.addTarget(self, action: #selector(shopCartViewController.clearShopCart), forControlEvents: .TouchUpInside)
        headView.addSubview(button)
        button.snp_makeConstraints(closure: { (make) -> Void in
            make.centerY.equalTo(headView)
            make.height.equalTo(20)
            make.right.equalTo(headView).offset(-8)
        })
        return headView
    }()
    
    func clearShopCart(){
        let realm = try! Realm()
        try! realm.write {
            for x in shopCart{
                realm.delete(x)
            }
            price = 0
            NSNotificationCenter.defaultCenter().postNotificationName(NotiShopCartClear, object: nil)
        }
       
    }
    
    lazy var newOrderViewController:UINavigationController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = story.instantiateViewControllerWithIdentifier("NewOrderViewController") as! NewOrderViewController
        return XZNavigationController(rootViewController: vc)
    }()
    
    var DetailView:UITableView!
    var constraintTop:NSLayoutConstraint!
    var constraintHeight:NSLayoutConstraint!
    var constraintViewH:NSLayoutConstraint!
    var token:NotificationToken!
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintViewH = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 46)
        view.backgroundColor = UIColor.clearColor()
        view.frame.origin = CGPoint(x: 1000, y: 1000)
        
        shopCartView.delegate = self
        view.addSubview(shopCartView)
        shopCartView.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(view)
        }
        shopCartView.makeUI()
        
        detailViewConfigure()
        
        token = shopCart.addNotificationBlock({ result in
            shopCartViewController.instance.calcPrice()
        })
    }
    
    func detailViewConfigure(){
        DetailView = UITableView()
        DetailView.bounces = false
        DetailView.delegate = self
        DetailView.dataSource = self
        DetailView.registerReusableCell(ShopCartViewCell)
        DetailView.layoutMargins = UIEdgeInsetsZero
        DetailView.separatorInset = UIEdgeInsetsZero
        containerView.addSubview(DetailView)
        DetailView.snp_makeConstraints { (make) -> Void in
            make.left.right.equalTo(containerView)
        }
        
        constraintTop = NSLayoutConstraint(item: DetailView, attribute: .Top, relatedBy: .Equal, toItem: shopCartView, attribute: .Top, multiplier: 1, constant: 0)
        constraintHeight = NSLayoutConstraint(item: DetailView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
        view.addConstraints([constraintTop,constraintHeight,constraintViewH])
        self.view.bringSubviewToFront(shopCartView)

    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        calcPrice()
    }
    
    func calcPrice(){
        var sum:Double = 0
        for x in shopCart{
            sum += x.currentPrice * Double(x.num)
        }
        price = sum
        shopCartView.updateButtonState(sum)
    }
    
    func showDetailView(){
        showingDetail = true
        constraintHeight.constant = min(ScreenHeight - 250, (CGFloat(shopCart.count) * (ShopCartCellHeight + 4)) + headViewForDetailHeight)
        constraintViewH.constant = ScreenHeight
        self.view.layoutIfNeeded()
        
        UIView.animateWithDuration(0.2) { [weak self] in
            self!.containerView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
        }
        UIView.animateWithDuration(0.3, delay: 0.1, options: .CurveEaseIn, animations: { [weak self] in
            self?.constraintTop.constant = -self!.constraintHeight.constant
            
            self!.view.layoutIfNeeded()
            }) { _ -> Void in
        }
        DetailView.reloadData()
        
    }
    
    func hideDetailView(){
        
        showingDetail = false
        UIView.animateWithDuration(0.2) {  [weak self]  in
            self!.containerView.backgroundColor = UIColor.clearColor()
        }
        UIView.animateWithDuration(0.3, delay: 0.1, options: .CurveEaseOut, animations: { [weak self] in
            self?.constraintTop.constant = 0
            self!.view.layoutIfNeeded()
            }) {  _ -> Void in
                
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { [weak self]  in
            NSThread.sleepForTimeInterval(0.3)
            dispatch_async(dispatch_get_main_queue(), {
                 self?.constraintViewH.constant = 46
            })
           
        }
    }
    
    func hideAndDo(action:()->Void){
        showingDetail = false

        UIView.animateWithDuration(0.2) {  [weak self]  in
            self!.containerView.backgroundColor = UIColor.clearColor()
        }
        UIView.animateWithDuration(0.3, delay: 0.1, options: .CurveEaseOut, animations: { [weak self] in
            self?.constraintTop.constant = 0
            self!.view.layoutIfNeeded()
            }) {  _ -> Void in
                
        }
        
        //由于container依赖于view，所以需要在动画完成之后再修改，可以尝试一下GCD组
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { [weak self] in
            NSThread.sleepForTimeInterval(0.3)
            dispatch_async(dispatch_get_main_queue(), {
                self?.constraintViewH.constant = 46
                action()
            })
            
        }
    }
    //MARK:- shopCartDelegate
    func showDetail() {
        if(showingDetail){
            hideDetailView()
        }else{
            showDetailView()
        }
    }
    
    func CreatingOrder() {
        self.presentViewController(newOrderViewController, animated: true, completion: nil)
    }
    
    
    func showInViewController(viewController:ShopBaseViewController){
        removeFromViewController()
        superViewController = viewController
        viewController.addChildViewController(self)
        view.frame = CGRect(x: 0, y: 1000, width: viewController.view.frame.width, height:shopCartView.ShopCartViewHeight)
        viewController.view.addSubview(view)
        view.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(viewController.contentView).offset(shopCartView.ShopCartViewHeight)
            make.centerX.equalTo(viewController.view)
            make.width.equalTo(viewController.view)
        }
        if(shopCart.count > 0){
            showShopCart(true)
        }
    }
    
    class func updateGood(){
        NSNotificationCenter.defaultCenter().postNotificationName(NotiShopCartClear, object: nil)
        for x in shopCartViewController.instance.shopCart{
            NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":x.goodID,"num":x.num])
        }
    }
    
    func removeFromViewController(){
        showShopCart(false)
        dis = 46
        removeFromParentViewController()
        view.removeFromSuperview()
    }
    var isShowing = false
    var dis:CGFloat = 46
    func showShopCart(tag:Bool){
        if(tag){
            superViewController?.contentView.frame.size.height  = (superViewController?.view.frame.size.height)! - dis
        }else{
            superViewController?.contentView.frame.size.height  = (superViewController?.view.frame.size.height)!
        }

        isShowing = tag
        superViewController?.contentView.layoutIfNeeded()

    }
}

//tableView代理
extension shopCartViewController:UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        if touch.view != containerView {
            return false
        }
        
        return true
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopCart.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ShopCartViewCell
        cell.title = shopCart[indexPath.row].name
        cell.price = shopCart[indexPath.row].currentPrice
        cell.num = shopCart[indexPath.row].num
        cell.incAction = {
            [weak self] sender,oldNum,newNum,cell -> Void in
            //假设有数据
            
            let realm = try! Realm()
            try! realm.write({
                self!.shopCart[indexPath.row].num += 1
                NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":self!.shopCart[indexPath.row].goodID,"num":self!.shopCart[indexPath.row].num])
            })
            
            //最后更新数组的数据
        }
        cell.decAction = {
            [weak self] sender,oldNum,newNum,cell -> Void in
            //假设有数据
            let realm = try! Realm()
            try! realm.write({
                 let index = tableView.indexPathForCell(cell)!.row
                self!.shopCart[index].num -= 1
            NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":self!.shopCart[index].goodID,"num":self!.shopCart[index].num])
            //最后更新数组的数据
            //判断是否为0
            if (newNum == 0){
                realm.delete(self!.shopCart[index])
                if(self!.price == 0){
                    return
                }
                if(self?.constraintTop.constant == -min(ScreenHeight - 250, (CGFloat(self!.shopCart.count) * (ShopCartCellHeight + 4)) + self!.headViewForDetailHeight)){
                    tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Fade)
                }else{
                    UIView.animateWithDuration(0.3, delay: 0.1, options: .CurveEaseIn, animations: { [weak self] in
                        self?.constraintTop.constant = -min(ScreenHeight - 250, (CGFloat(self!.shopCart.count) * (ShopCartCellHeight + 4)) + self!.headViewForDetailHeight)
                        
                        self!.view.layoutIfNeeded()
                        }) { _ -> Void in
                            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Fade)
                    }
                }
                
            }
        })
            
        }
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headViewForDetail
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headViewForDetailHeight
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsetsZero
    }
    
    
}