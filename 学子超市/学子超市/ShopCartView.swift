//
//  ShopCartView.swift
//  学子超市
//
//  Created by jason on 16/3/16.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import pop
protocol ShopCartViewDelegate:NSObjectProtocol{
    func CreatingOrder()
    func showDetail()
}

class ShopCartView: UIView {
    
    let ShopCartViewHeight:CGFloat = 46
    let buttonOKWidth:CGFloat = 115
    
    weak var delegate:ShopCartViewDelegate!
    
    var price:Double = 0 {
        didSet{
            labelPrice.text = "￥" + String(format: "%.1f", price)
        }
    }
    lazy var edgeView:UILabel = {
        let edge = UILabel()
        edge.backgroundColor = UIColor.redColor()
        edge.textColor = UIColor.whiteColor()
        edge.textAlignment = .Center
        edge.layer.cornerRadius = 8
        edge.hidden = true
        edge.clipsToBounds = true
        edge.font = UIFont.systemFontOfSize(14)
        return edge
    }()
    
    var edge:Int = 0{
        didSet{
            if(edge == 0){
                edgeView.hidden = true
                return
            }
            guard edge != oldValue else {return}
            let animation = POPBasicAnimation(propertyNamed: kPOPLayerSize)
            animation.duration = 0.5
            animation.beginTime = CACurrentMediaTime()
            animation.toValue = NSValue(CGSize: CGSize(width: 16, height:   16))
            if(edgeView.hidden == true){
                animation.fromValue = NSValue(CGSize: CGSize(width: 0, height: 0))
            }else{
                animation.fromValue = NSValue(CGSize: CGSize(width: 10, height: 10))
            }
            edgeView.hidden = false
            edgeView.text = "\(edge)"
            edgeView.layer.pop_addAnimation(animation, forKey: "NumAdd")
        }
    }
    func numAnimation(){
        let animation = POPBasicAnimation(propertyNamed: kPOPLayerSize)
        animation.duration = 0.5
        animation.beginTime = CACurrentMediaTime()
        animation.toValue = NSValue(CGSize: CGSize(width: 16, height:   16))
        if(edgeView.hidden == true){
            animation.fromValue = NSValue(CGSize: CGSize(width: 0, height: 0))
        }else{
            animation.fromValue = NSValue(CGSize: CGSize(width: 10, height: 10))
        }
        edgeView.hidden = false
        edgeView.text = "\(edge)"
        edgeView.layer.pop_addAnimation(animation, forKey: "NumAdd")
    }
    
    lazy var labelPrice: UILabel = {
       let label = UILabel()
        label.textColor = UIColor.MainColor()
        label.font = UIFont.shopCartPriceFont()
        label.text = "￥0"
        return label
    }()
    
    lazy var  imgView:UIImageView = {
        let view = UIImageView(image: UIImage(named: "shopCart"))
        return view
    }()
    
    
    lazy var buttonOK:UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(UIImage.createImageWithColor(UIColor.colorWithRGB(199, g: 199, b: 205)), forState: .Disabled )
        btn.setBackgroundImage(UIImage.createImageWithColor(UIColor.MainColor()), forState: .Normal)
        //考虑用userDefault存取起送价
        btn.setTitle("还差1元起送", forState: .Disabled)
        btn.setTitle("去下单", forState: .Normal)
        btn.titleLabel?.font = UIFont.systemFontOfSize(14)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(ShopCartView.buttonOKClicked), forControlEvents: .TouchUpInside)
        btn.enabled = false
        return btn
    }()
    
    func buttonOKClicked(){
        delegate.CreatingOrder()
    }
    
    func updateButtonState(nowPrice:Double){
        if nowPrice >= XZUserDefaults.EnableSendFee.value{
            buttonOK.enabled = true
        }else{
            buttonOK.enabled = false
            buttonOK.setTitle("还差\(XZUserDefaults.EnableSendFee.value!-nowPrice)元起送", forState: .Disabled)
        }
    }
    
    
    lazy var buttonDetail: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.clearColor()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(ShopCartView.showDetail), forControlEvents: .TouchUpInside)
        return btn
    }()//显示购物车清单按钮
    
    func showDetail(){
        delegate.showDetail()
    }
    
    func makeUI(){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.whiteColor()
        self.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(ShopCartViewHeight)
            make.width.equalTo(ScreenWidth)
        }
        let viewLine = UIView()
        viewLine.backgroundColor = UIColor.lightGrayColor()
        self.addSubview(viewLine)
        viewLine.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(0.3)
            make.centerX.equalTo(self)
            make.width.equalTo(self)
            make.top.equalTo(self).offset(0.5)
        }
        
        self.addSubview(imgView)
        imgView.snp_makeConstraints { (make) -> Void in
            make.width.height.equalTo(33)
            make.left.equalTo(self).offset(18)
            make.centerY.equalTo(self)
        }
        
        self.addSubview(edgeView)
        edgeView.snp_makeConstraints { (make) in
            make.centerX.equalTo(imgView.snp_right)
            make.centerY.equalTo(imgView.snp_top)
            make.width.height.equalTo(16)
        }
        
        self.addSubview(labelPrice)
        labelPrice.snp_makeConstraints { (make) -> Void in
            make.centerY.equalTo(self)
            make.left.equalTo(imgView.snp_right).offset(2)
        }
        labelPrice.sizeToFit()
        
        self.addSubview(buttonDetail)
        buttonDetail.snp_makeConstraints(closure: {make -> Void in
            make.height.equalTo(self)
            make.centerX.equalTo(self)
            make.width.equalTo(ScreenWidth)
            make.bottom.equalTo(self)
        })

        self.addSubview(buttonOK)
        buttonOK.snp_makeConstraints(closure: { make -> Void in
            make.height.equalTo(self)
            make.width.equalTo(buttonOKWidth)
            make.right.equalTo(buttonDetail)
            make.top.equalTo(buttonDetail)
        })
    }


}
