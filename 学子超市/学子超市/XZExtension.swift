//
//  JSExtension.swift
//  学子超市
//
//  Created by jason on 3/13/16.
//  Copyright © 2016 jason. All rights reserved.
//

import UIKit
import MBProgressHUD

typealias Task = (cancel:Bool) -> Void
func delay(time:NSTimeInterval,task:()->()) -> Task?{
    func dispatch_later(block:()->()){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,Int64(time*Double(NSEC_PER_SEC))), dispatch_get_main_queue(), block)
    }
    var closure:dispatch_block_t? = task
    var result:Task?
    let delayedClosure:Task = {
        cancel in
        if let internalClosure = closure{
            if(cancel == false){
                dispatch_async(dispatch_get_main_queue(), internalClosure)
            }
        }
        closure = nil
        result = nil
    }
    result = delayedClosure
    dispatch_later { 
        if let delayedClosure = result{
            delayedClosure(cancel: false)
        }
    }
    return result
}

func cancel(task:Task?){
    task?(cancel:true)
}

protocol Reusable: class {
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
}

extension CLLocationCoordinate2D{
    func isEqual(object:CLLocationCoordinate2D?) -> Bool{
        return self.latitude == object?.latitude && self.longitude == object?.longitude
    }
}

extension Reusable {
    static var reuseIdentifier: String { return String(Self) }
    static var nib: UINib? { return UINib(nibName: String(Self), bundle: NSBundle.mainBundle())}
}

extension UIViewController{
    
    func tabBarOK() -> Bool{
        return false
    }
    
    func closeCurrentViewControllerAniamted(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension UIView{
    func startLoading(){
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        var hud:MBProgressHUD! = self.subviews.filter({$0.tag == 999}).last as? MBProgressHUD
        if(hud == nil){
            hud = MBProgressHUD.showHUDAddedTo(self, animated: true)
            hud.removeFromSuperViewOnHide = false
            hud.activityIndicatorColor = UIColor.MainColor()
            hud.opacity = 0
            hud.tag = 999
            
        }
        hud.hidden = false
    }
    
    func endLoading(){
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        let hud:MBProgressHUD! = self.subviews.filter({$0.tag == 999}).last as? MBProgressHUD
        if let hud = hud{
            hud.hidden = true
        }
    }
}



extension UITableView {
    
    func clearOtherLine(){
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        self.tableFooterView = view
    }
    
    func registerReusableCell<T: UITableViewCell where T: Reusable>(_: T.Type) {
        if let nib = T.nib {
            self.registerNib(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.registerClass(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    func registerReusableCellInClass<T: UITableViewCell where T: Reusable>(_: T.Type) {
        
        self.registerClass(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    
    func dequeueReusableCell<T: UITableViewCell where T: Reusable>(indexPath indexPath: NSIndexPath) -> T {
        return self.dequeueReusableCellWithIdentifier(T.reuseIdentifier, forIndexPath: indexPath) as! T
    }
    
    func registerReusableHeaderFooterView<T: UITableViewHeaderFooterView where T: Reusable>(_: T.Type) {
        if let nib = T.nib {
            self.registerNib(nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        } else {
            self.registerClass(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView where T: Reusable>() -> T? {
        return self.dequeueReusableHeaderFooterViewWithIdentifier(T.reuseIdentifier) as! T?
    }
}

extension UINavigationBar{
}

extension UIColor{
    class func colorWithRGB(r: CGFloat, g:CGFloat, b:CGFloat) -> UIColor {
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
    }

    class func MainColor() -> UIColor{
        return UIColor.colorWithRGB(255, g: 162, b: 30)
    }
    
    class func  ViewMBKColor() -> UIColor{
        return UIColor.colorWithRGB(239, g: 239, b: 239)
    }
    
    class func RedBagOkColor() -> UIColor{
        return UIColor.colorWithRGB(240, g: 89, b: 103)
    }
    
    class func RedBagUnOkColor() -> UIColor{
        return UIColor.colorWithRGB(199, g: 199, b: 205)
    }
    
    class func ButtonOkColor() -> UIColor{
        return UIColor.colorWithRGB(255, g: 150, b: 0)
    }
    
    class func ButtonCancelColor() -> UIColor {
        return UIColor.colorWithRGB(155, g: 155, b: 155)
    }
    
    class func ButtonBottomColor() -> UIColor {
        return UIColor.colorWithRGB(249, g: 249, b: 249)
    }
    
    class func ButtonHotKeyColor() -> UIColor{
        return UIColor.colorWithRGB(142, g: 142, b: 147)
    }
    
    class func PriceRealColor() -> UIColor {
        return UIColor.colorWithRGB(254, g: 56, b: 36)
    }
    
    class func PricePastedColor() -> UIColor{
        return UIColor.colorWithRGB(155, g: 155, b: 155)
    }
    
    class func ClassBKColor() -> UIColor{
        return  UIColor.colorWithRGB(239, g: 239, b: 239)
    }
    
    class func NoticeBKColor() -> UIColor{
        return UIColor.colorWithRGB(251, g: 249, b: 126)
    }
    
    class func TextNormalBlackColor() -> UIColor{
        return UIColor.colorWithRGB(74, g: 74, b: 74)
    }
    
    class func ShopCartCellBKColor() -> UIColor{
        return UIColor.colorWithRGB(246, g: 246, b: 246)
    }
    
    class func RedBagActiveBKColor() -> UIColor{
        return UIColor.colorWithRGB(240, g: 89, b: 103)
    }
    class func RedBagNOActiveBKColor() -> UIColor{
        return UIColor.colorWithRGB(199, g: 199, b: 205)
    }
    
    class func FeeSendStrColoc() -> UIColor{
        return UIColor.colorWithRGB(92, g: 92, b: 92)
    }
    
    class func ActivityNameColor() -> UIColor{
        return UIColor.colorWithRGB(74, g: 74, b: 74)
    }
    
}

extension UIButton{
    func verticalImgAndTitle(space:CGFloat){
        sizeToFit()
        let imgSize = self.imageView!.frame.size
        var titleSize = self.titleLabel!.frame.size
        let textSize = self.titleLabel?.text!.sizeWithAttributes([NSFontAttributeName:titleLabel!.font])
        let frameSize = CGSize(width: ceil(textSize!.width), height: ceil(textSize!.height))
        if(titleSize.width + 0.5 < frameSize.width){
            titleSize.width = frameSize.width
        }
        let totalHeight = imgSize.height + titleSize.height + space
        self.imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight-imgSize.height), 0, 0,-titleSize.width)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -imgSize.width, -(totalHeight - titleSize.height), 0)
    }
}



extension MBProgressHUD{
    
    class func show(text:String,icon:String,  view:UIView?){
        var toView = view
        if toView == nil{
            toView = UIApplication.sharedApplication().windows.last!
        }
        let hud = MBProgressHUD.showHUDAddedTo(toView, animated: true)
        hud.labelText = text
        hud.customView = UIImageView(image: UIImage(named: "MBProgressHUD.bundle/\(icon)"))
        hud.mode = .CustomView
        hud.removeFromSuperViewOnHide = true
        hud.hide(true, afterDelay: 0.7)
    }
    
    class func showSuccess(success:String, toView:UIView?){
        self.show(success, icon: "success.png", view: toView)
    }
    
    class func showError(error:String, toView:UIView?){
        self.show(error, icon: "error.png", view: toView)
    }
    
    class func showMessage(msg:String?,view:UIView?) -> MBProgressHUD{
        var toView = view
        if toView == nil{
            toView = UIApplication.sharedApplication().windows.last!
        }
        let hud = MBProgressHUD.showHUDAddedTo(toView, animated: true)
        hud.removeFromSuperViewOnHide = true
        hud.dimBackground = false
        return hud
    }
}



extension UIFont{
    class func navigationBarFont() -> UIFont{
        return UIFont.systemFontOfSize(17)
    }
    
    class func shopCartPriceFont() -> UIFont{
        return UIFont.systemFontOfSize(18)
    }
}

extension UIImage{
    class func createImageWithColor(color:UIColor) -> UIImage{
        let frame = CGRectMake(0, 0, 1, 1)
         UIGraphicsBeginImageContext(frame.size)
        let content = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(content, color.CGColor)
        CGContextFillRect(content, frame)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
}

extension UIEdgeInsets{
    static func ButtonSearchEdge() -> UIEdgeInsets{
        return UIEdgeInsetsMake(4, 10, 4, 10)
    }
}

