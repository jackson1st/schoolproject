//
//  StoreInfoViewController.swift
//  学子超市
//
//  Created by jason on 16/5/11.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class StoreInfoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var LabelStoreName: UILabel!
    @IBOutlet weak var starView: StarView!
    @IBOutlet weak var LabelScore: UILabel!
    @IBOutlet weak var LabelInfo: UILabel!
    @IBOutlet weak var ImgPhoto: UIImageView!
    @IBOutlet weak var LabelManageTime: UILabel!
    @IBOutlet weak var LabelPhone: UILabel!
    @IBOutlet weak var LabelAddress: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var ConstraintTableViewHeight: NSLayoutConstraint!
    lazy var store : Store = {
        //网络请求商店信息
        return  Store(ID:"123",name: "学子超市", photoURL: "http://p0.meituan.net/120.0/xianfu/8ad01f73021eb40bc54159f22280842e18432.jpg", score: 4.3, location: CLLocation(latitude: 116.4,longitude: 39.9), activities: [Activity.afterReduction(AfterReduction(title: "满减活动", startTime: "123", endTime: "123")),Activity.afterSend(AfterSend(title: "满送活动", startTime: "123", endTime: "123"))], feeSend: 20, feeEnableSends: [FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 1000, fee: 10),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 10000, fee: 20),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 100000, fee: 30)], businessHours: "12:00 - 17:00", phone: "15260025228", detailLocation: "这是一个虚拟的店铺")
    }()
    override func loadView() {
        super.loadView()
        starView.setSize(64, height: 14)
        self.view.backgroundColor = UIColor.ViewMBKColor()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerReusableCell(StoreActivityTableViewCell)
        ConstraintTableViewHeight.constant = 40.0 * CGFloat(store.activities.count)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return store.activities.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as StoreActivityTableViewCell
        cell.setContent(store.activities[indexPath.row])
        return cell
    }
    
    
    @IBAction func ButtonActionCall() {
       UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(store.phone)")!)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setContent()
    }
    
    //考虑将数据更新完后存本地，从本地读取
    //数据还需处理
    func setContent(){
        LabelStoreName.text = store.name
        LabelPhone.text = store.phone
        LabelScore.text = String(store.score)
        starView.setPercent(CGFloat(store.score), sum: 5)
        ImgPhoto.sd_setImageWithURL(NSURL(string: store.photoURL))
        LabelManageTime.text = store.businessHours
        LabelAddress.text = store.detailLocation
        LabelInfo.text = "\(store.feeEnableSend)元起送 | \(store.feeSend)元配送费"
    }

}
