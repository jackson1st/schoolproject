//
//  BaseViewController.swift
//  学子超市
//
//  Created by jason on 16/3/16.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController,UIGestureRecognizerDelegate,searchProtocol,UISearchResultsUpdating {

    var searchController:UISearchController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if self.respondsToSelector(Selector("automaticallyAdjustsScrollViewInsets")){
//            self.automaticallyAdjustsScrollViewInsets = false
//        }
        self.navigationController?.navigationBar.translucent = false
        view.backgroundColor = UIColor.ViewMBKColor()
//        navigationController?.navigationBar.translucent = true
//        navigationController?.navigationBar.shadowImage = nil
//        navigationController?.navigationBar.barStyle = UIBarStyle.Default
//        navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
//        self.navigationItem.leftBarButtonItem = //        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
    }
    
    func initSearchController(placeholder:String){
        searchController = UISearchController(searchResultsController: nil)
        searchController!.searchResultsUpdater = self
        searchController!.hidesNavigationBarDuringPresentation = false
        searchController!.dimsBackgroundDuringPresentation = false
        searchController!.searchBar.placeholder = placeholder
        searchController!.searchBar.barStyle = .Default
        searchController!.searchBar.searchBarStyle = .Default
        //mark，通过修改背景图片可以解决诡异现象
        searchController!.searchBar.backgroundImage = UIImage.createImageWithColor(UIColor.clearColor())
        self.navigationItem.titleView = searchController!.searchBar
        definesPresentationContext = true
    }
    
    func popToSuperVC(){
        if(navigationController?.childViewControllers.count > 1){
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        guard let _ = navigationController else{
            return
        }
        navigationController?.navigationBar.barTintColor = UIColor.MainColor()
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let textAttributes = [
            NSForegroundColorAttributeName:UIColor.whiteColor(),
            NSFontAttributeName:UIFont.navigationBarFont()
        ]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchUpdating(key: String?) {
        //提供一个空实现
    }
    
    //搜索代理需要些searchUpdating方法
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        searchUpdating(searchController.searchBar.text)
    }
    

}
