//
//  ShopBaseViewController.swift
//  学子超市
//
//  Created by Jason on 16/3/17.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import MMTweenAnimation
class ShopBaseViewController: BaseViewController {
    
    var goods = [Commodity]()
    var animationY:MMTweenAnimation! {
        let animation = MMTweenAnimation(block: { (_, _) -> Bool in
            return true
        })
        
        animation.animationBlock = {
            a,b,value,target,c in
            let view = target as! UIView
            view.frame.origin.y = value[0] as! CGFloat
        }
        animation.functionType = .Sine
        animation.easingType = .In
        animation.beginTime = CACurrentMediaTime()
        animation.duration = 1
        return animation
    }
    
    var ay:POPAnimatableProperty = {
        let ay = POPAnimatableProperty.propertyWithName("Y") { (pop) in
            pop.writeBlock = {
                one,two in
                let view = one as! UIView
                view.frame.origin.y = two[0]
            }
            pop.readBlock = {
                one,two in
                
            }
            pop.threshold = 0.01
        } as! POPAnimatableProperty
        return ay
    }()
    
     var animationX:POPBasicAnimation!{
        let animation = POPBasicAnimation(propertyNamed: kPOPLayerPositionX)
        animation.beginTime = CACurrentMediaTime()
        animation.duration = 1
        
        return animation
    }
    var RedView:UIView!{
        let view = UIView()
        view.backgroundColor = UIColor.redColor()
        view.frame.size = CGSize(width: 10, height: 10)
        view.layer.cornerRadius = 5
        self.view.addSubview(view)
        return view
    }
    var RedViews = [UIView]()
    
    var tag:Bool {
        get{
            return shopCartViewController.instance.isShowing
        }
    }//是否显示
    override func loadView() {
        super.loadView()
        initContentView()
    }
    
    var contentView:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let leftItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = leftItem
    }
    
    func initContentView(){
        contentView = UIView()
        contentView.backgroundColor = UIColor.clearColor()
        contentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(contentView)

    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        shopCartViewController.instance.showInViewController(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShopBaseViewController.GoodNumChange(_:)), name: NotiGoodNumChange, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShopBaseViewController.ShopCartClear), name: NotiShopCartClear, object: nil)
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShopBaseViewController.pushSizeChoose(_:)), name: NotiPushAlertSizeChoose, object: nil)
        shopCartViewController.updateGood()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NotiPushAlertSizeChoose, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NotiGoodNumChange, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NotiShopCartClear, object: nil)
    }
    
    func GoodNumChange(not: NSNotification) {
        let info = not.userInfo as! [String:AnyObject]
        let ID = info["goodID"] as! String
        let num = info["num"] as! Int
        if let btn = info["view"] as? UIButton{
            self.addAitem(btn.convertRect(btn.bounds, toView: self.view).origin)
        }
        for i in 0..<goods.count{
            if(goods[i].id == ID && goods[i].isMultiSpec == false){
                goods[i].buied = num
                //用hash存下下标，快速更新，后期工作
                break
            }else{
                goods[i].buied = 0
            }
        }
    }

    
    func ShopCartClear(){
        
    }

    func addAitem(from:CGPoint){
       let to = shopCartViewController.instance.shopCartView.imgView.convertRect(shopCartViewController.instance.shopCartView.imgView.bounds, toView: self.view).origin
        //animation也可以复用，来空再撸吧
        let aY = POPBasicAnimation.easeInAnimation()
        aY.duration = 1
        aY.property = ay
        let aX = animationX
        aY.fromValue = from.y
        aY.toValue = to.y
        aX.fromValue = from.x
        aX.toValue = to.x
        aX.beginTime = CACurrentMediaTime()
        aY.beginTime = CACurrentMediaTime()
       
        if(tag == false){
            showShopCart(true)
            aY.toValue = to.y - 40
            aX.beginTime += 0.1
            aY.beginTime += 0.1
        }
        
        var redView:UIView!
        if(RedViews.count == 0){
            redView = RedView
        }else{
            redView = RedViews.popLast()
        }
        redView.frame.origin = from
        self.view.bringSubviewToFront(redView)
        redView.hidden = false
        redView.pop_addAnimation(aX, forKey: "X")
        redView.pop_addAnimation(aY, forKey: "Y")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            [weak self] in
            NSThread.sleepForTimeInterval(1)
            dispatch_async(dispatch_get_main_queue(), {
                redView.hidden = true
                self?.RedViews.append(redView)
            })
        }
    }
    
    func showShopCart(tag:Bool){
        shopCartViewController.instance.showShopCart(tag)
    }
    //选择规格弹窗
    let sizeChoose = GoodSizeChooseViewController()
    lazy var chooseAndGo:(object:ShopCartGood)->Void = {
        [weak self] object in
       // 加入购物车
        ShopCartGood.addOne(object)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            [weak self] in
            NSThread.sleepForTimeInterval(0.5)
            dispatch_async(dispatch_get_main_queue(), { 
                self?.addAitem(self!.view.center)
            })
        })
        
    }
    func pushSizeChoose(noti:NSNotification){
        if let object = noti.userInfo!["object"] as? Commodity{
            sizeChoose.object = object
            sizeChoose.chooseAndGo = chooseAndGo
            sizeChoose.pushInViewController(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
}
