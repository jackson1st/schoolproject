//
//  LoginViewController.swift
//  学子超市
//
//  Created by jason on 16/4/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var TextFieldPhone: UITextField!
    @IBOutlet weak var TextFieldPassWord: UITextField!
    init(){
        super.init(nibName: "LoginViewController", bundle: nil)
        super.loadView()
        self.view.backgroundColor = UIColor.ViewMBKColor()
        self.title = "登录"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "注册", style: .Plain, target: self, action: #selector(self.goToRegist))
    }
    
    override func closeCurrentViewControllerAniamted() {
        TextFieldPhone.resignFirstResponder()
        TextFieldPassWord.resignFirstResponder()
        super.closeCurrentViewControllerAniamted()
    }
    
    func goToRegist(){
        let vc = RegisterViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func ButtonLoginClicked() {
        
    }
    
    @IBAction func ButtonForgetClicked() {
        let vc = ForgotPassWordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    
    //textfield代理
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField.tag == 101){
            textField.resignFirstResponder()
            TextFieldPassWord.becomeFirstResponder()
        }else{
            TextFieldPassWord.resignFirstResponder()
            ButtonLoginClicked()
        }
        return true
    }


}
