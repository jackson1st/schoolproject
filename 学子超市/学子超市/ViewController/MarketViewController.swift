//
//  MarketViewController.swift
//  学子超市
//
//  Created by jason on 16/3/30.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import Alamofire
class MarketViewController: BaseViewController,MuliPageHeaderViewDelegate {

    var multiPageViewController:MuliPageViewController!
    lazy var superMarketViewController:SuperMarketViewController! = {
        let controller = SuperMarketViewController()
//        self.addChildViewController(controller)
//        controller.view.frame.size = CGSize(width: ScreenWidth, height: self.view.frame.height - 104)
        return controller
    }()
    lazy var evaViewController:EVAViewController! = {
        return EVAViewController()
    }()
    lazy var barItemSearch:UIBarButtonItem = {
        return  UIBarButtonItem(image: UIImage(named:"Search"), style: .Plain, target: self, action: #selector(self.pushSearchVC))
    }()
    lazy var searchVC: SearchViewController = {
        let vc = SearchViewController()
        return vc
    }()
    lazy var storeInfoViewController:StoreInfoViewController = {
        let vc = StoreInfoViewController(nibName: "StoreInfoViewController", bundle: NSBundle.mainBundle())
        return vc
    }()
    
    func pushSearchVC(){
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    //如果不放在view里面，那么图标将会消失
    lazy var buttonStoreChoose:UIView = {
        let view = UIView()
        let btn = UIButton()
        btn.setImage(UIImage(named: "IconLocation"), forState: .Normal)
        btn.setTitle("点击定位", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.setTitleColor(UIColor.colorWithRGB(168, g: 140, b: 73), forState: .Highlighted)
        btn.addTarget(self, action: #selector(self.chooseStore), forControlEvents: .TouchUpInside)
        btn.sizeToFit()
        view.addSubview(btn)
        btn.frame.origin = CGPoint(x: 0, y: 0)
        view.frame.size = btn.frame.size
        return view
    }()
    lazy var storeChooseViewController:UINavigationController = {
        (ViewController.storeChooseViewController.topViewController as! StoreChooseViewController).StoreChangeAction.append({
            print("更新商店页面")
        })
        return  ViewController.storeChooseViewController
    }()
    
    func chooseStore(){
        self.presentViewController(storeChooseViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initmuliPage()
        evaViewController.setScore(4.92, convey: 4.81)
        self.navigationItem.rightBarButtonItem = barItemSearch
        self.navigationItem.titleView = buttonStoreChoose
        
    }
    
    
    func initmuliPage(){
        //评论页面和信息页面
       
        multiPageViewController = MuliPageViewController(titles: ["超市","评论","信息"], views: [superMarketViewController.view,evaViewController.view,storeInfoViewController.view])
        multiPageViewController.pageTitleFont = UIFont.systemFontOfSize(15)
        multiPageViewController.headViewHeight = 40
//        multiPageViewController.headView.offSetCorrect = -64
        multiPageViewController.headView.bottomLineEnable = true
        self.addChildViewController(multiPageViewController)
        self.view.addSubview(multiPageViewController.view)
        multiPageViewController.addChildViewController(evaViewController)
        multiPageViewController.addChildViewController(superMarketViewController)
        multiPageViewController.view.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: self.view.frame.height )
    }

    
    func pageSelectedOfIndex(index:Int){
        
    }
    func pageSelected(to:Int,from:Int){
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
