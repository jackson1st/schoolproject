//
//  RegisterViewController.swift
//  学子超市
//
//  Created by jason on 16/4/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var ButtonGetPhoneDig: UIButton!//获取验证码按钮
    @IBOutlet weak var TextFiledPhone: UITextField!
    @IBOutlet weak var TextFiledPhoneDIg: UITextField!
    @IBOutlet weak var TextFiledPassWord: UITextField!
    @IBOutlet weak var LabelWait: UILabel!
    @IBOutlet weak var ButtonRegister: UIButton!
    init(){
        super.init(nibName: "RegisterViewController", bundle: nil)
        super.loadView()
        self.title = "注册"
        ButtonGetPhoneDig.layer.cornerRadius = 6
        ButtonGetPhoneDig.layer.borderColor = UIColor.MainColor().CGColor
        ButtonGetPhoneDig.layer.borderWidth = 0.6
        LabelWait.layer.cornerRadius = 6
        LabelWait.layer.borderWidth = 0.6
        LabelWait.layer.borderColor = UIColor.lightGrayColor().CGColor
        
    }
    @IBAction func ButtonGetPhoneDigClicked() {
        print("获取验证码")
        ButtonGetPhoneDig.hidden = true
        LabelWait.text = "1:00"
        LabelWait.hidden = false
        setTime(60)
    }
    @IBAction func Register() {
        print("注册")
    }
    
    func setTime(length:Int){
        
        let str = String(format:"%d:%d%d",length/60,length%60/10,length%60%10)
        self.LabelWait.text = str
        if(length == 0){
            LabelWait.hidden = true
            ButtonGetPhoneDig.hidden = false
            return
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1*NSEC_PER_SEC)), dispatch_get_main_queue()) {
            [weak self] in
            self?.setTime(length - 1)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: "RegisterViewController", bundle: nil)
    }
}
