//
//  RedBagUseViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RedBagUseViewController: RedBagGetViewController {

    var actionSelected:(Int) -> Void = {
        _ in
    }
    var selectedIndex:Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择红包"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "确定", style: .Plain, target: self, action: #selector(self.redBagSelected))
        initButtonNoUse()
    }
    

    
    func initButtonNoUse(){
        let btn = UIButton()
        btn.setTitle("不使用红包", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.backgroundColor = UIColor.MainColor()
        self.view.addSubview(btn)
        btn.snp_makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view)
            make.height.equalTo(40)
        }
        btn.addTarget(self, action: #selector(self.noUseRedBag), forControlEvents: .TouchUpInside)
    }
    
    func noUseRedBag(){
        if(selectedIndex != -1){
            tableView.deselectRowAtIndexPath(NSIndexPath(forRow: selectedIndex,inSection: 0 ), animated: true)
        }
        selectedIndex = -1
        redBagSelected()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(selectedIndex != -1){
            tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedIndex,inSection: 0 ), animated: true, scrollPosition: .None)
        }
    }
    func redBagSelected(){
        actionSelected(selectedIndex)
        if self.navigationController?.childViewControllers.count == 1{
            self.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewBinding.hidden = true
        tableView.frame = CGRectMake(0, 64, self.view.frame.width, self.view.frame.height - 104)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as TicketTableViewCell
        cell.style = .RedBagUse
        cell.redBag = redBags[indexPath.row]
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        selectedIndex = indexPath.row
    }
    
    

}
