//
//  GoodSizeChooseViewController.swift
//  学子超市
//
//  Created by jason on 16/3/27.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import STPopup

class GoodSizeChooseViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let viewWidth:CGFloat = ScreenWidth * 0.84
    lazy var tableView:UITableView! = {
        let tableview = UITableView()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.bounces = false
        tableview.backgroundColor = UIColor.colorWithRGB(246, g: 246, b: 244)
        tableview.registerReusableCell(SizeTableViewCell)
        return tableview
    }()
    lazy var picture:UIImageView = {
        let view = UIImageView()
        self.view.addSubview(view)
        view.frame = CGRect(x: 40, y: 20, width: 100, height: 100)
        view.clipsToBounds = true
        return view
    }()
    
    lazy var labelChooseSize:UILabel = {
        let label = UILabel()
        let title = UILabel()
        title.text = "已选"
        title.textColor = UIColor.colorWithRGB(74, g: 74, b: 74)
        title.font = UIFont.systemFontOfSize(14)
        title.sizeToFit()
        title.frame = CGRect(x: 160, y: 30, width: 40, height: 20)
        self.view.addSubview(title)
        label.frame = CGRect(x: title.frame.maxX + 5, y: 25, width: ScreenWidth - title.frame.maxX - 5, height: 30)
        label.textColor = UIColor.MainColor()
        label.font = UIFont.systemFontOfSize(15)
        self.view.addSubview(label)
        return label
    }()
    var chooseSize:String!{
        didSet{
            labelChooseSize.text = chooseSize
            selectedSizeIndex = dict[chooseSize]!
        }
    }
    
    lazy var labelPrice:UILabel! = {
        let label = UILabel()
        let title = UILabel()
        title.text = "价格"
        title.textColor = UIColor.colorWithRGB(74, g: 74, b: 74)
        title.font = UIFont.systemFontOfSize(14)
        title.sizeToFit()
        title.frame = CGRect(x: 160, y: 90, width: 40, height: 20)
        self.view.addSubview(title)
        label.frame = CGRect(x: title.frame.maxX + 5, y: 85, width: ScreenWidth - title.frame.maxX - 5, height: 30)
        self.view.addSubview(label)
        return label
    }()
    var price:Double!{
        didSet{
            let str = "¥" + String(format: "%.1f", price)
            let attStr = NSMutableAttributedString(string: str)
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(0, 1))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(16),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(1, str.characters.count - 1))
            labelPrice.attributedText = attStr
        }
    }
    var object:Commodity!{
        didSet{
            //向服务器请求一下规格
            sizes = object.sizeNames
            for i in 0..<object.mGoodsSpecs.count {
                let x = object.mGoodsSpecs[i] as! GoodsSpecs
                self.dict[x.specDescription] = i
            }
            
        }
    }
    var selectGoodSpecs:GoodsSpecs!
    
    var buttonGo:UIButton!
    var stores = [Int]()
    var prices = [Double]()
    var dict = [String:Int]()//用来做一个具体串跟index的映射
    var sizes = [(title:String,detail:[String])]()
    var selectedSizeIndex = 0 {
        didSet{
            selectGoodSpecs = (object.mGoodsSpecs[selectedSizeIndex] as! GoodsSpecs)
            price = selectGoodSpecs.currentPrice
            picture.sd_setImageWithURL(NSURL(string:selectGoodSpecs.imageUrl))
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择规格"
        self.view.clipsToBounds = true
        self.view.addSubview(tableView)
        initButtonGo()
        tableView.frame = CGRect(x: 0, y: 140, width: viewWidth, height: CGFloat(sizes.count * 100))
        buttonGo.center.x = tableView.center.x
        buttonGo.frame.origin.y = tableView.frame.maxY + 15
        buttonGo.frame.size = CGSize(width: 250, height: 40)
        self.contentSizeInPopup = CGSize(width: viewWidth, height: buttonGo.frame.maxY + 15)
        chooseSize = (object.mGoodsSpecs[0] as! GoodsSpecs).specDescription
    }
        
    func testData(){
        //解决库存点选问题
            sizes = [(title:"颜色",detail:["红色","白色","黑色"]),(title:"尺寸",detail:["大","小"])]
            dict = ["红色大":0,"红色小":1,"白色大":2,"白色小":3,"黑色大":4,"黑色小":5]
            chooseSize = "红色大"
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 140, width: viewWidth, height: CGFloat(sizes.count * 80))
        buttonGo.center.x = tableView.center.x
        buttonGo.frame.origin.y = tableView.frame.maxY + 15
        buttonGo.frame.size = CGSize(width: 250, height: 40)
        self.contentSizeInPopup = CGSize(width: viewWidth, height: buttonGo.frame.maxY + 15)
    }
    
    func initButtonGo(){
        buttonGo = UIButton()
        buttonGo.setTitle("加入购物车", forState: .Normal)
        buttonGo.titleLabel?.font = UIFont.systemFontOfSize(18)
        buttonGo.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        buttonGo.setBackgroundImage(UIImage.createImageWithColor(UIColor.MainColor()), forState: .Normal)
        buttonGo.setBackgroundImage(UIImage.createImageWithColor(UIColor.lightGrayColor()), forState: .Disabled)
        buttonGo.layer.cornerRadius = 6
        buttonGo.clipsToBounds = true
        buttonGo.addTarget(self, action: #selector(GoodSizeChooseViewController.Go), forControlEvents: .TouchUpInside)
        self.view.addSubview(buttonGo)
        
    }
    
    var chooseAndGo:(object:ShopCartGood)->Void = {
        _ in
    }
    func Go(){
        print("购买")
        //这里需要根据具体选择规格，生成一个记录，加入到购物车，所选的规格就放在默认规格当中
        //这里假装已经选好
        chooseAndGo(object: ShopCartGood(good: self.object, specsID: selectGoodSpecs.id, specDescription: selectGoodSpecs.specDescription, price: selectGoodSpecs.currentPrice))
        self.popupController.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sizes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as SizeTableViewCell
        cell.sizes = sizes[indexPath.row].detail
        cell.selectedAction = {
            [weak self] to,from in
            self!.chooseSize = self!.chooseSize.stringByReplacingOccurrencesOfString(from, withString: to)
        }
        cell.LabelTitle.text = sizes[indexPath.row].title
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    lazy var pop:STPopupController = {
       let pop = STPopupController(rootViewController: self)
        pop.containerView.layer.cornerRadius = 4
        return pop
    }()
    func pushInViewController(viewController:UIViewController){
        pop.presentInViewController(viewController)
    }
    
}
