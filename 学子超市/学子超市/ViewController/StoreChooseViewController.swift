//
//  StoreChooseViewController.swift
//  学子超市
//
//  Created by jason on 16/4/14.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import pop
class StoreChooseViewController: BaseViewController,UISearchBarDelegate,MAMapViewDelegate,AMapSearchDelegate {

    private lazy var searchBarButtonItem:UIBarButtonItem = {
        return UIBarButtonItem(image: UIImage(named:"Search"), style: .Plain, target: self, action: #selector(self.showSearchBar))
    }()
    private lazy var SearchAPI:AMapSearchAPI! = {
        let search = AMapSearchAPI()
        search.delegate = self
        return search
    }()
    private lazy var regeoCodeRequest:AMapReGeocodeSearchRequest = {
        return AMapReGeocodeSearchRequest()
    }()
    var UserAddress:String = "正在定位..."{
        didSet{
            self.tableView.reloadSections(NSIndexSet(indexesInRange: NSMakeRange(0, 1)), withRowAnimation: .Automatic)
        }
    }
    var tableView:UITableView!
    var cellHeight = [CGFloat]()
    var mapView:MAMapView!
    var stores = [Store]()
    var reloadLocationImgView:UIView!
    var StoreChangeAction:[()->()] = [{
        
    }]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择店铺"
        self.navigationController?.navigationBar.translucent = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Close"), style: .Plain, target: self, action: #selector(self.popToSuperVC))
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
        initTableView()
        initMAMapView()
        createData()
        calcCellHeight()
    }
    
    //虚拟数据
    func createData(){
        
        for _ in 0...100{
            stores.append(Store(ID:"123",name: "学子超市", photoURL: "http://p0.meituan.net/120.0/xianfu/8ad01f73021eb40bc54159f22280842e18432.jpg", score: 4.3, location: CLLocation(latitude: 116.4,longitude: 39.9), activities: nil, feeSend: 20, feeEnableSends: [FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 1000, fee: 10),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 10000, fee: 20),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 100000, fee: 30)], businessHours: "12:00 - 17:00", phone: "15260025228", detailLocation: "这是一个虚拟的店铺"))
            stores.append(Store(ID:"123",name: "学子超市", photoURL: "http://p0.meituan.net/120.0/xianfu/8ad01f73021eb40bc54159f22280842e18432.jpg", score: 4.8, location: CLLocation(latitude: 116.4,longitude: 39.9), activities: [Activity.afterReduction(AfterReduction(title: "满减活动", startTime: "123", endTime: "123")),Activity.afterSend(AfterSend(title: "满送活动", startTime: "123", endTime: "123"))], feeSend: 20, feeEnableSends: [FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 1000, fee: 10),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 10000, fee: 20),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 100000, fee: 30)], businessHours: "12:00 - 17:00", phone: "15260025228", detailLocation: "这是一个虚拟的店铺"))
            stores.append(Store(ID:"123",name: "学子超市", photoURL: "http://p0.meituan.net/120.0/xianfu/8ad01f73021eb40bc54159f22280842e18432.jpg", score: 4.8, location: CLLocation(latitude: 116.4,longitude: 39.9), activities: [Activity.afterReduction(AfterReduction(title: "满减活动", startTime: "123", endTime: "123"))], feeSend: 20, feeEnableSends: [FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 1000, fee: 10),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 10000, fee: 20),FeeEnableSend(center: CLLocation(latitude: 116.4,longitude: 39.9), radio: 100000, fee: 30)], businessHours: "12:00 - 17:00", phone: "15260025228", detailLocation: "这是一个虚拟的店铺"))
        }
    }
    
    func calcCellHeight(){
        let cell = NSBundle.mainBundle().loadNibNamed("StoreChooseTableViewCell", owner: nil, options: nil)[0] as! StoreChooseTableViewCell
        for x in stores{
            cell.dataObject = x
            cell.layoutIfNeeded()
            cellHeight.append(cell.frame.height)
        }
    }
    
    func initMAMapView(){
        mapView = MAMapView()
        mapView.delegate = self
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.Denied && CLLocationManager.locationServicesEnabled()){
            mapView.showsUserLocation = true
        }
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        tableView.clearOtherLine()
        tableView.delegate = self
        tableView.dataSource  = self
        tableView.bounces = false
        tableView.registerReusableCell(XZTableViewCell)
        tableView.registerReusableCell(StoreChooseTableViewCell)
        tableView.backgroundColor = UIColor.ViewMBKColor()
        tableView.layoutMargins = UIEdgeInsetsZero
        tableView.separatorInset = UIEdgeInsetsZero
        self.view.addSubview(tableView)
    }
    
    func showSearchBar(){
        self.navigationItem.rightBarButtonItem = nil
        if(searchController == nil){
            initSearchController("输入商店")
            searchController?.searchBar.delegate = self
        }else{
            self.navigationItem.titleView = searchController?.searchBar
        }
        searchController?.searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.navigationItem.titleView = nil
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
    }
    
    lazy var popBasicAnimation:POPBasicAnimation = {
        let animation = POPBasicAnimation(propertyNamed: kPOPLayerRotationX)
        animation.duration = 0.5
        animation.fromValue = 0
        animation.toValue = CGFloat(2 * M_PI)
        animation.autoreverses = true
        animation.repeatForever = true
        return animation
    }()
    func locationStartLoading(){
        let transform = CGAffineTransformRotate(reloadLocationImgView.transform, CGFloat( M_PI))
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.reloadLocationImgView.transform = transform
        })
    }
    
    //定位
    func mapView(mapView: MAMapView!, didUpdateUserLocation userLocation: MAUserLocation!) {
        mapView.showsUserLocation = false
        regeoCodeRequest.location = AMapGeoPoint.locationWithLatitude(CGFloat(userLocation.location.coordinate.latitude), longitude: CGFloat(userLocation.location.coordinate.longitude))
        SearchAPI.AMapReGoecodeSearch(regeoCodeRequest)
    }
    
    func onReGeocodeSearchDone(request: AMapReGeocodeSearchRequest!, response: AMapReGeocodeSearchResponse!) {
        UserAddress =  response.regeocode.formattedAddress

    }
    
}

extension StoreChooseViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return stores.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsetsZero
        if(indexPath.section == 1 && cellHeight.count <= indexPath.row){
            cell.layoutIfNeeded()
            cellHeight.append(cell.frame.height)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as XZTableViewCell
            cell.title = UserAddress
            cell.accessoryView = UIImageView(image: UIImage(named: "IconLoading"))
            self.reloadLocationImgView = cell.accessoryView
            cell.selectionStyle = .None
            cell.ConstraintWidth.constant = ScreenWidth - 40
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as StoreChooseTableViewCell
            cell.selectionStyle = .None
            cell.dataObject = stores[indexPath.row]
            cell.enterAction = {
                [weak self] in
                self?.tableView(tableView, didSelectRowAtIndexPath: indexPath)
            }
            return cell
        }
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 40
        }
        return cellHeight[indexPath.row]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0){
            mapView.showsUserLocation = true
            locationStartLoading()
        }else{
            for x in StoreChangeAction{
                x()
            }
        }
    }
}