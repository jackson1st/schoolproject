//
//  AddressSearchViewController.swift
//  学子超市
//
//  Created by jason on 16/3/21.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
class AddressSearchViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate{

    struct address{
        var name:String
        var loc:String
        var point:AMapGeoPoint
        
        init(name:String,loc:String,point:AMapGeoPoint){
            self.name = name
            self.loc = loc
            self.point = point
        }
        
        init(poi:AMapPOI){
            self.name = poi.name
            self.loc = poi.address
            self.point = poi.location
        }
        
    }
    

    var GDSearch:AMapSearchAPI!
    var searchRequest:AMapPOIAroundSearchRequest!
    var tableView:UITableView!
    var addresses = [address]()
    
    var SelectedAction:((name:String,point:AMapGeoPoint) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initGDSearch()
        initSearchController("写字楼/小区/学校等")
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        tableView.keyboardDismissMode = .OnDrag
        tableView.bounces = false
        tableView.rowHeight = 60
        tableView.estimatedRowHeight = 60
        tableView.clearOtherLine()
        tableView.registerReusableCell(LocationAddressTableViewCell)
        self.view.addSubview(tableView)
    }
    
    func initGDSearch(){
        GDSearch = AMapSearchAPI()
        GDSearch.delegate = self
        searchRequest = AMapPOIAroundSearchRequest()
        let userDefault = NSUserDefaults.standardUserDefaults()
        let longitude:CGFloat = userDefault.valueForKey("longitude") as! CGFloat
        let latitude:CGFloat = userDefault.valueForKey("latitude") as! CGFloat
        searchRequest.location = AMapGeoPoint.locationWithLatitude(latitude, longitude: longitude)
        searchRequest.radius = 10000
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func searchUpdating(key: String?) {
        if(key == nil || key == ""){
            addresses.removeAll()
            self.tableView.reloadData()
        }else{
            self.tableView.startLoading()
            searchRequest.keywords = key
            GDSearch.AMapPOIAroundSearch(searchRequest)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as LocationAddressTableViewCell
        cell.TextLabel?.text = addresses[indexPath.row].name
        cell.DetailLabel?.text = addresses[indexPath.row].loc
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let viewControllers = navigationController?.viewControllers
        navigationController?.popToViewController(viewControllers![viewControllers!.count - 3], animated: true)
        SelectedAction(name:addresses[indexPath.row].name,point:addresses[indexPath.row].point)
        
    }

    
    func onPOISearchDone(request: AMapPOISearchBaseRequest!, response: AMapPOISearchResponse!) {
        
        addresses.removeAll()
        for x in response.pois{
            addresses.append(address(poi: x as! AMapPOI))
        }
        self.tableView.endLoading()
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    

}
