//
//  TicketGoodUseViewController.swift
//  学子超市
//
//  Created by jason on 16/4/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class TicketGoodUseViewController: RedBagUseViewController {

    var titckets = [TicketGood]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择兑换劵"
    }

//    override func testData(){
//        for _ in 0...5{
//            titckets.append(TicketGood(title: "一部iPhone 6s Plus", startTime: "2016-04-02", endTime: "2016-04-07", state: true, good:Good(name: "iPhone 6s Plus", firClass: "电子产品", secClass: "手机", buied: 0, sell: 100, sizes: [GoodSize(name: "内存:16G-版本:港货-颜色:玫瑰金", detail: "小内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 4676, realPrice: 4675, store: 100, active: 1)]) , num: 1, ticketID: "15260025228"))
//            titckets.append(TicketGood(title: "一部iPhone 6s Plus", startTime: "2016-04-02", endTime: "2016-04-07", state: true, good:Good(name: "iPhone 6s Plus", firClass: "电子产品", secClass: "手机", buied: 0, sell: 100, sizes: [GoodSize(name: "内存:16G-版本:港货-颜色:玫瑰金", detail: "小内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 4676, realPrice: 4675, store: 100, active: 1)]) , num: 1, ticketID: "15260025228"))
//        }
//    }
    
    override func initButtonNoUse(){
        let btn = UIButton()
        btn.setTitle("不使用兑换劵", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.backgroundColor = UIColor.MainColor()
        self.view.addSubview(btn)
        btn.snp_makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view)
            make.height.equalTo(40)
            
        }
        btn.addTarget(self, action: #selector(self.noUseRedBag), forControlEvents: .TouchUpInside)
    }
    
    override  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titckets.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as TicketTableViewCell
        cell.style = .TicketGoodUse
        cell.ticketGood = titckets[indexPath.row]
        cell.selectionStyle = .None
        return cell
    }

}
