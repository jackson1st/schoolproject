////
////  GoodCollectionViewController.swift
////  学子超市
////
////  Created by jason on 16/3/27.
////  Copyright © 2016年 jason. All rights reserved.
////
//
//import UIKit
//
//private let reuseIdentifier = "Cell"
//
//class GoodCollectionViewController: UICollectionViewController {
//
//    var goods = [Commodity]()
//    var layoutH:UICollectionViewFlowLayout!
//    var decAction:(sender:UIButton,oldNum:Int,newNum:Int,object:Good)->Void = {
//        _,_,_,_ in
//        
//    }
//    var incAction:(sender:UIButton,oldNum:Int,newNum:Int,object:Good)->Void = {
//        _,_,_,_ in
//        
//    }
//    var chooseSize:(object:Good) -> Void = {
//        _ in
//        
//    }
//    convenience init(){
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .Vertical
//        layout.minimumInteritemSpacing = 5
//        layout.minimumLineSpacing = 8
//        layout.headerReferenceSize = CGSizeMake(0, 25)
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        layout.itemSize = CGSize(width: 170,height: 170)
//        self.init(collectionViewLayout: layout)
//        layoutH = layout
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.collectionView?.registerNib(UINib(nibName: "GoodVCollectionViewCell",bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cellH")
//        collectionView!.registerClass(HomeCollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//    
//    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
//        let headView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) as! HomeCollectionHeaderView
//        return headView
//    }
//    
//
//
//    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of items
//        return goods.count
//    }
//
//    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellH", forIndexPath: indexPath) as! GoodVCollectionViewCell
//        cell.object = goods[indexPath.row]
//        cell.decAction = decAction
//        cell.incAction = incAction
//        cell.ChooseSize = chooseSize
//        return cell
//    }
//
//}
