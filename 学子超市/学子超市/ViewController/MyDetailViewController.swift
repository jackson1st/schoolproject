//
//  MyDetailViewController.swift
//  学子超市
//
//  Created by jason on 16/4/12.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import MBProgressHUD
class MyDetailViewController: BaseViewController {

    var tableView:UITableView!
    lazy var headPhoto:UIImageView! = {
        let imgView = UIImageView(image: UIImage(named: "headPhoto"))
        imgView.frame.size = CGSize(width: 70, height: 70)
        imgView.layer.cornerRadius = 35
        imgView.clipsToBounds = true
        return imgView
    }()
    var saveAction:(photo:UIImage,name:String) -> Void = {
        _,_ in
        
    }
    lazy var ImgPickViewController:UIImagePickerController = {
        let viewController = UIImagePickerController()
        
        viewController.delegate = self
        return viewController
    }()
    lazy var forgotPassWordViewController:ForgotPassWordViewController = {
        return ForgotPassWordViewController()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "编辑资料"
        initTableView()
        initRightBarButtonItem()
        initButtonExit()
    }
    
    func initButtonExit(){
        let btn = UIButton()
        btn.setTitle("退出当前账号", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        btn.setBackgroundImage(UIImage.createImageWithColor(UIColor.colorWithRGB(249, g: 249, b: 249)), forState: .Normal)
        btn.setTitleColor(UIColor.colorWithRGB(254, g: 56, b: 36), forState: .Normal)
        btn.addTarget(self, action: #selector(self.exit), forControlEvents: .TouchUpInside)
        self.view.addSubview(btn)
        btn.frame = CGRect(x: 0, y: self.view.frame.height - 40, width: self.view.frame.width, height: 40)
    }
    
    func exit(){
        print("退出")
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.clearOtherLine()
        tableView.bounces = false
        self.view.addSubview(tableView)
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 104)
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.registerReusableCell(XZTableViewCell)
        tableView.backgroundColor = UIColor.ViewMBKColor()
        tableView.layoutMargins = UIEdgeInsetsZero
        tableView.separatorInset = UIEdgeInsetsZero
        tableView.keyboardDismissMode = .Interactive
    }
    
    func initRightBarButtonItem(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完成", style: .Plain, target: self, action: #selector(self.finish))
    }
    
    func finish(){
        print("完成")
        let cells = tableView.visibleCells
        
        let name:String = (cells[1] as! XZTableViewCell).textField.text!
        saveAction(photo: headPhoto.image!, name: name)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

}

extension MyDetailViewController:UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //图片选择器代理
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        headPhoto.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        default:
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
               let cell = tableView.dequeueReusableCellWithIdentifier("cell")
                cell?.textLabel?.text = "头像"
                cell?.accessoryView = headPhoto
                return cell!
            }else{
                switch indexPath.row {
                case 1:
                    let cell = tableView.dequeueReusableCell(indexPath: indexPath) as XZTableViewCell
                    cell.LabelTitle.text = "昵称"
                    cell.textField.text = "Jason"
                    cell.selectionStyle = .None
                    return cell
                default :
                    let cell = tableView.dequeueReusableCell(indexPath: indexPath) as XZTableViewCell
                    cell.LabelTitle.text = "性别"
                    cell.textField.text = "男"
                    cell.textField.enabled = false
                    cell.accessoryType = .DisclosureIndicator
                    return cell
                
                }
                
            }
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            cell?.textLabel?.text = "修改登录密码"
            return cell!
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.Camera){
            ImgPickViewController.sourceType = .Camera
            self.presentViewController(ImgPickViewController, animated: true, completion: nil)
        }else{
        }
    }
    
    func openPhotoLibrary(){
        ImgPickViewController.sourceType = .PhotoLibrary
        ImgPickViewController.allowsEditing = true
        self.presentViewController(ImgPickViewController, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if(indexPath.section == 1){
            self.navigationController?.pushViewController(forgotPassWordViewController, animated: true)
        }else{
            switch indexPath.row {
            case 0:
                print("换头像")
                let alter = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
                alter.addAction(UIAlertAction(title: "拍照", style: .Default, handler: { [weak self] action in
                    self?.openCamera()
                }))
                alter.addAction(UIAlertAction(title: "从相册选择", style: .Default, handler: { [weak self] action in
                   self?.openPhotoLibrary()
                }))
                alter.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
                self.presentViewController(alter, animated: true, completion: nil)
                
            case 2:
                print("换性别")
                let alter = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
                alter.addAction(UIAlertAction(title: "男", style: .Default, handler: { (action) in
                    print("选择了男")
                }))
                alter.addAction(UIAlertAction(title: "女", style: .Default, handler: { (action) in
                    print("选择了女")
                }))
                alter.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
                self.presentViewController(alter, animated: true, completion: nil)
            default:
                print("无操作")
            }
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            
        cell.layoutMargins = UIEdgeInsetsZero
            
        }else{
            cell.layoutMargins = UIEdgeInsetsMake(0,0, 0, 10)
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 1){
            return 10
        }else{
            return 0
        }
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section{
        case 0:
            if indexPath.row == 0{
                return 100
            }
            return 50
        default:
            return 50
        }
        
    }
}