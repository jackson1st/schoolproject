//
//  MuliPageViewController.swift
//  MulitPageViewControllerTest
//
//  Created by jason on 16/3/20.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MuliPageViewController: UIViewController,MuliPageHeaderViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource{

    private var cellViews = [UIView]()
    var width = UIScreen.mainScreen().bounds.width
    lazy var headView:MuliPageHeaderView = {
        let view = MuliPageHeaderView()
        view.pageDelegate = self
        view.hilightColor = UIColor.MainColor()
        view.TitleFont = self.pageTitleFont
        view.shadowBottomLine.hidden = false
        return view
    }()
    private var onDraging = false
    
    var headViewHeight:CGFloat = 30
    var pageTitleFont = UIFont.systemFontOfSize(16)
    
    lazy var flowLayout:UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        layout.minimumLineSpacing = 0
        return layout
    }()
    
    var collectionView:UICollectionView?
    
    var pageAction:((Int)->Void)! = {
        _ in
        
    }
    
    convenience init(var titles:[String],var views:[UIView]){
        self.init()
        let counts = titles.count < views.count ? titles.count:views.count
        for  _ in titles.count ..< counts{
            titles.removeLast()
        }
        for _ in views.count ..< counts{
            views.removeLast()
        }
        headView.setButtons(titles)
        setViews(views)
    }
    
    func setViews(views:[UIView]){
        cellViews = views
        if let collectionView = collectionView{
            collectionView.reloadData()
        }
    }
    
    func setPageTitles(titles:[String]){
        headView.setButtons(titles)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initHeaderView()
        initCollectionView()
    }
    
    private func initHeaderView(){
        self.view.addSubview(headView)
    }
    
    private func initCollectionView(){
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: self.flowLayout)
        self.view.addSubview(collectionView!)
        collectionView!.backgroundColor = UIColor.whiteColor()
        collectionView!.showsHorizontalScrollIndicator = false
        collectionView!.delegate = self
        collectionView!.dataSource = self
        //采用nib注册的话，会有意想不到的惊喜，草
        collectionView!.registerClass(MuliPageCollectionViewCell.self, forCellWithReuseIdentifier: "MuliPageCell")
        collectionView!.bounces = false
        collectionView!.pagingEnabled = true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func pageSelectedOfIndex(index:Int){
        scrollToPage(index)
    }
    
    func pageSelected(to: Int, from: Int) {
        //空实现
    }
    
    private func scrollToPage(index:Int){
        collectionView!.scrollToItemAtIndexPath(NSIndexPath(forRow: index, inSection: 0), atScrollPosition: .None, animated: true)
        pageAction(index)
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    

     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellViews.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MuliPageCell", forIndexPath: indexPath) as! MuliPageCollectionViewCell
        cell.view?.removeFromSuperview()
        cell.view = cellViews[indexPath.row]
        return cell
    }
    
    //没办法处理按钮不定长的情况
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView.dragging){
            let offSet = scrollView.contentOffset.x/scrollView.contentSize.width * self.view.frame.width
            headView.moveBottomLineToxWithAnimation(offSet, animation: false)
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        print("准备动了")
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        headView.scrollToPageAtIndex(Int(scrollView.contentOffset.x/scrollView.frame.width))
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: headViewHeight)
        collectionView!.frame = CGRect(x: 0, y: headViewHeight, width: self.view.frame.width, height: self.view.frame.height - headViewHeight)
        (collectionView!.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = (self.collectionView!.frame.size)
    }
    
    
}
