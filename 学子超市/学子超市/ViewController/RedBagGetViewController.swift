//
//  TicketTableViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RedBagGetViewController: BaseViewController {

    var tableView: UITableView!
    var mode = 0
    var viewBinding:UIView!
    var textFiledID:UITextField!
    var redBags = [RedBag]()
    override func viewDidLoad() {
        super.viewDidLoad()
        initTextFieldID()
        initTableView()
//        testData()
        self.title = "红包领取"
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if(self.navigationController?.navigationBarHidden == true){
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
//    func testData(){
//        for _ in 0...5{
//            redBags.append(RedBag(title: "满20减5", startTime: "2016-04-02", endTime: "2016-04-07", value: 5))
//            redBags.append(RedBag(title: "满30减3", startTime: "2016-04-02", endTime: "2016-05-07", value: 3))
//            redBags.append(RedBag(title: "满30减3", startTime: "2016-04-02", endTime: "2016-05-07", value: 3,state:false))
//        }
//    }
    
    func initTextFieldID(){
        viewBinding = UIView()
        viewBinding.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewBinding)
        viewBinding.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: 50)
        textFiledID = UITextField()
        textFiledID.borderStyle = .RoundedRect
        textFiledID.placeholder = "输入兑换码"
        viewBinding.addSubview(textFiledID)
        textFiledID.frame = CGRect(x: 20, y: 5, width: self.view.frame.width - 100, height: 40)
        let button = UIButton()
        button.setTitle("兑换", forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.backgroundColor = UIColor.MainColor()
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(self.buttonBinding), forControlEvents: .TouchUpInside)
        viewBinding.addSubview(button)
        button.frame = CGRect(x: textFiledID.frame.maxX + 8, y: textFiledID.frame.origin.y, width: 60, height: 40)
    }
    
    func buttonBinding(){
        //暂时处理
        if let id = textFiledID.text where id != "" {
            print("兑换成功")
        }
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.backgroundColor = UIColor.ViewMBKColor()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.registerReusableCell(TicketTableViewCell)
        tableView.clearOtherLine()
        tableView.rowHeight = 143
        tableView.estimatedRowHeight = 143
        tableView.keyboardDismissMode = .OnDrag
        tableView.frame = CGRect(x: 0, y: 50, width: ScreenWidth, height: self.view.frame.height - 50)
        self.view.addSubview(tableView)
    }

}

extension RedBagGetViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as TicketTableViewCell
        cell.style = .RedBagGet
        cell.redBag = redBags[indexPath.row]
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        tableView.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 143
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return redBags.count
    }
}