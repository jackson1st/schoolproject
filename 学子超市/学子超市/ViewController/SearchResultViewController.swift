//
//  SearchResultViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import RealmSwift
class SearchResultViewController: ShopBaseViewController {

    var key:String!{
        didSet{
            //被设置的时候就进行网络请求数据
            if let collectionView = collectionView{
                collectionView.startLoading()
            }
            //网络请求
            if let collectionView = collectionView{
                collectionView.endLoading()
            }
        }
    }
    var collectionView:UICollectionView?
    lazy var goodDetailViewController:GoodDetailViewController = {
        return GoodDetailViewController()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initSearchController("输入商品名称")
        initCollectionView()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .Plain, target: self, action:#selector(XZNavigationController.popToSuperVC))
    }
    override func searchUpdating(key: String?) {
        self.key = key
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView!.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
    }
    
    func initCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical
        layout.minimumLineSpacing = 1
        layout.itemSize = CGSize(width: ScreenWidth ,height: 90)
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: self.contentView.frame.height), collectionViewLayout: layout)
        collectionView!.backgroundColor = UIColor.ViewMBKColor()
        collectionView!.delegate = self
        collectionView!.dataSource = self
        collectionView!.bounces = false
        collectionView!.registerNib(UINib(nibName: "GoodVCollectionViewCell",bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cellH")
        collectionView?.keyboardDismissMode = .OnDrag
        //到时候处理一下分页
        self.contentView.addSubview(collectionView!)
//        for _ in 0...10{
//            goods.append(Good(name: "iPhone 6s PlusiPhone 6s PlusiPhone 6s Plus", firClass: "电子产品", secClass: "手机", buied: 0, sell: 100, sizes: [GoodSize(name: "内存:16G-版本:港货-颜色:玫瑰金", detail: "小内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 4676, realPrice: 4675, store: 100, active: 1),GoodSize(name: "内存:16G-版本:行货-颜色:玫瑰金", detail: "大 内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 6076, realPrice: 6075, store: 100, active: 0),GoodSize(name: "内存:16G-版本:行货-颜色:白色", detail: "大 内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 6076, realPrice: 6075, store: 100, active: 0),GoodSize(name: "内存:16G-版本:港货-颜色:白色", detail: "中内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 5276, realPrice: 5275, store: 100, active: 0),GoodSize(name: "内存:16G-版本:行货-颜色:土豪金", detail: "大 内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 6076, realPrice: 6075, store: 100, active: 0),GoodSize(name: "内存:16G-版本:港货-颜色:土豪金", detail: "大 内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 6076, realPrice: 6075, store: 100, active: 0)]))
//            
//            goods.append(Good(name: "iPhone 6s Plus", firClass: "电子产品", secClass: "手机", buied: 0, sell: 100, sizes: [GoodSize(name: "内存:16G-版本:港货-颜色:玫瑰金", detail: "小内存", imgURL: "http://img11.360buyimg.com/n1/s450x450_jfs/t2113/203/128429424/95656/be32c7d2/55f0e7b5Nf1ed773f.jpg", oriPrice: 4676, realPrice: 4675, store: 100, active: 1)]))
//        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension SearchResultViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return goods.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellH", forIndexPath: indexPath) as! GoodVCollectionViewCell
        cell.mode = 2
        cell.object = goods[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        goodDetailViewController.object = goods[indexPath.row]
        self.navigationController?.pushViewController(goodDetailViewController, animated: true)
    }

}