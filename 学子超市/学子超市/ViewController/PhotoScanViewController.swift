//
//  PhotoScanViewController.swift
//  学子超市
//
//  Created by jason on 16/3/26.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import SDWebImage
private let reuseIdentifier = "Cell"

class PhotoScanViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var PhotoScanHeight:CGFloat = 200
    private let waitQueue = dispatch_queue_create("wait", DISPATCH_QUEUE_SERIAL)
    lazy var layout:UICollectionViewFlowLayout = {
        let layou = UICollectionViewFlowLayout()
        layou.scrollDirection = .Horizontal
        layou.minimumLineSpacing = 0
        layou.itemSize = CGSize(width: ScreenWidth, height: self.PhotoScanHeight)
        return layou
    }()
    lazy var collectionView:UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: self.PhotoScanHeight), collectionViewLayout: self.layout)
        collectionView.registerNib(UINib(nibName: "MuliPageCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.pagingEnabled = true
        collectionView.bounces = false
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    var Photos = [UIImageView](){
        didSet{
            collectionView.reloadData()
        }
    }
    typealias SelectedAction = Int -> Void
    var actions = [SelectedAction]()//使用闭包的形式来传递操作
    
    var PhotoURLs = [String](){
        didSet{
            Photos.removeAll()
            var views = [UIImageView]()
            for x in PhotoURLs{
                let imgView = UIImageView()
                imgView.sd_setImageWithURL(NSURL(string: x))
                views.append(imgView)
            }
            Photos = views
            pageController.numberOfPages = Photos.count
        }
    }
    var currentPage = -1
    
    lazy var pageController:UIPageControl = {
        let page = UIPageControl()
        page.center = CGPoint(x: ScreenWidth / 2, y: self.PhotoScanHeight - 20)
        self.view?.addSubview(page)
        return page
    }()
    
    func autoScan(){
        currentPage = (currentPage + 1) % PhotoURLs.count
        pageController.currentPage = currentPage
        collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: currentPage,inSection:0), atScrollPosition: UICollectionViewScrollPosition.None, animated: true)
        dispatch_async(waitQueue) {
            [weak self] in
            NSThread.sleepForTimeInterval(2)
            dispatch_async(dispatch_get_main_queue(), {
                self?.autoScan()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(collectionView)
        autoScan()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = collectionView.frame.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        currentPage = Int(scrollView.contentOffset.x/scrollView.frame.width)
        pageController.currentPage = currentPage
    }

    // MARK: UICollectionViewDataSource

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Photos.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MuliPageCollectionViewCell
        cell.view = Photos[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row < actions.count){
            actions[indexPath.row](indexPath.row)
        }
    }
    
}
