//
//  GoodDetailViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import RealmSwift
class GoodDetailViewController: ShopBaseViewController,ButtonBuyDelegate {

    @IBOutlet weak var ViewInfoConstraint: NSLayoutConstraint!
    @IBOutlet weak var ConstraintLabelDetailLeft: NSLayoutConstraint!
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var ActiveTag: UIImageView!
    @IBOutlet weak var LabelDetail: UILabel!
    @IBOutlet weak var LabelRealPrice: UILabel!
    @IBOutlet weak var LabelOriPrice: UILabel!
    @IBOutlet weak var LabelSell: UILabel!
    @IBOutlet weak var ButtonGo: ButtonBuy!
    init() {
        super.init(nibName: "GoodDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: "GoodDetailViewController", bundle: nil)
    }
    
    let buttonChooseSize = UIButton()
    var object:Good!{
        didSet{
            LabelTitle.text = object.name
            if(object.defaultSize?.oriPrice == object.defaultSize?.realPrice){
                LabelOriPrice.hidden = true
            }
            LabelRealPrice.attributedText = object.defaultSize?.realPriceStr
            LabelOriPrice.attributedText = object.defaultSize?.oriPriceStr
            LabelSell.text = "已售\(object.sell)件"
            //需要判断具体活动,以及倒计时之类的
            if(object.sizes.count > 1){
                ButtonGo.hidden = true
                buttonChooseSize.hidden = false
            }else{
                ButtonGo.hidden = false
                buttonChooseSize.hidden = true
                ButtonGo.num = object.buied//考虑
            }

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       }
