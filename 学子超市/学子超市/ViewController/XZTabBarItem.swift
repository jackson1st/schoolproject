//
//  XZTabBarItem.swift
//  学子超市
//
//  Created by jason on 16/3/26.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class XZTabBarItem: UITabBarItem {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //解决高亮默认为蓝色的bug
        self.selectedImage = self.selectedImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.MainColor()], forState: .Selected)
    }
}
