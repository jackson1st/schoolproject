//
//  SearchViewController.swift
//  学子超市
//
//  Created by Jason on 16/3/26.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{

    lazy var ResultTableView:UITableView = {
       let tableview = UITableView()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.bounces = false
        tableview.frame = self.view.frame
        tableview.tag = 102
        self.view.addSubview(tableview)
        return tableview
    }()
    var tableView:UITableView!
    var data = [String]()
    var history = ["这是历史搜索","曾经搜索过","的词","这是历史搜索","曾经搜索过","的词"]
    var hotKey = ["这是热词","非常热","的词","这是热词","非常热","的词","这是热词","非常热","的词"]
    lazy var viewForHotKey:MuliPageHeaderView = {
        let view = MuliPageHeaderView(titles: self.hotKey, style: .HotKey)
        view.pageDelegate = self
        view.space = 20
        view.hilightColor = UIColor.MainColor()
        //进行网络请求，刷新热词
        return view
    }()
    lazy var buttonClear:UIView = {
       let btn = UIButton()
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        btn.setTitle("清空历史记录", forState: .Normal)
        btn.setTitleColor(UIColor.redColor(), forState: .Normal)
        btn.setTitleColor(UIColor.lightGrayColor(), forState: .Highlighted)
        btn.titleLabel?.font = UIFont.systemFontOfSize(14)
        btn.layer.cornerRadius = 4
        btn.layer.borderColor = UIColor.redColor().CGColor
        btn.layer.borderWidth = 0.7
        btn.addTarget(self, action: #selector(SearchViewController.buttonClearClicked), forControlEvents: .TouchUpInside)
        view.addSubview(btn)
        btn.frame.size = CGSize(width: 125, height: 30)
        btn.center = CGPoint(x: ScreenWidth/2, y: 20)
        return view
    }()
    func buttonClearClicked(){
        print("清除历史记录")
    }
    let searchResultViewController = SearchResultViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        initSearchController("输入商品名称")
        searchController!.searchResultsUpdater = nil
        searchController!.searchBar.delegate = self
        searchController!.searchBar.returnKeyType = .Search
        initTableView()
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.frame = view.frame
        tableView.tag = 101
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.keyboardDismissMode = .Interactive
        tableView.backgroundColor = UIColor.ViewMBKColor()
        self.view.addSubview(tableView)
    }
    
    override func searchUpdating(key: String?) {
        if(key == nil || key == ""){
            tableView.hidden = true
        }else{
            tableView.hidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

extension SearchViewController:MuliPageHeaderViewDelegate,UISearchControllerDelegate,UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        print(searchBar.text)
        getSearchResultOfKey(searchBar.text)
    }
    
    func pageSelectedOfIndex(index:Int){
        getSearchResultOfKey("123")
    }
    
    func getSearchResultOfKey(key:String?){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            let controller:SearchResultViewController? = SearchResultViewController()
            let navigation:UINavigationController? = self.navigationController!
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC))),dispatch_get_main_queue() , {
                navigation!.pushViewController(controller!, animated: true)
            })
        }
        
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func pageSelected(to: Int, from: Int) {
        //空实现
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(tableView.tag == 101){
            return 2
        }
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 102){
            return data.count
        }
        if(section == 0){
            return 1
        }
        return history.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(tableView.tag == 102){
            
        }
        let  cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        if(indexPath.section == 0){
            cell.tag = 199
            cell.contentView.addSubview(viewForHotKey)
            viewForHotKey.frame.origin = CGPoint(x: 0,y:0)
            viewForHotKey.frame.size = CGSize(width: ScreenWidth, height: 60)
        }else{
            //cell复用的问题,之后探讨
            if(cell.tag == 199){
               viewForHotKey.removeFromSuperview()
            }
            cell.textLabel?.text = history[indexPath.row]
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        getSearchResultOfKey("123")//测试
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView.tag == 101){
            let label = UILabel()
            label.textColor = UIColor.ButtonHotKeyColor()
            label.font = UIFont.systemFontOfSize(14)
            if(section == 0){
                label.text = "  热门搜索"
            }else{
                label.text = "  历史记录"
            }
            return label
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (tableView.tag == 101 && indexPath.section == 1){
            return 40
        }
        return 60
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(tableView.tag == 101 && section == 1){
            return 40
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(tableView.tag == 101 && section == 1){
            return buttonClear
        }
        return nil
    }
}
