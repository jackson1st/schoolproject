//
//  RedBagCheckViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RedBagCheckViewController: RedBagGetViewController {

    var style = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        if(style == 1){
            self.title = "我的红包"
        }else{
            self.title = "历史红包"
        }
        if(style == 1){
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "历史红包", style: .Plain, target: self, action: #selector(self.LookpastRedBag))
        }
    }
    

    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        textFiledID.resignFirstResponder()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if(style != 1){
            viewBinding.hidden = true
            tableView.frame = CGRectMake(0, 64, self.view.frame.width, self.view.frame.height - 64)
        }
    }
    
    func LookpastRedBag(){
        //或者通过请求链接来实现，完美
        let vc = RedBagCheckViewController()
        vc.style = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }

//    override func testData(){
//        if(style == 1){
//        for _ in 0...5{
//            redBags.append(RedBag(title: "满20减5", startTime: "2016-04-02", endTime: "2016-04-07", value: 5))
//            redBags.append(RedBag(title: "满30减3", startTime: "2016-04-02", endTime: "2016-05-07", value: 3))
//            }
//        }else{
//            for _ in 0...5{
//            redBags.append(RedBag(title: "满30减3", startTime: "2016-04-02", endTime: "2016-05-07", value: 3,state:false))
//            }
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as TicketTableViewCell
        cell.redBag = redBags[indexPath.row]
        cell.selectionStyle = .None
        return cell
    }
    

}
