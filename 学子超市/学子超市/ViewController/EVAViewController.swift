//
//  EVAViewController.swift
//  学子超市
//
//  Created by jason on 16/4/1.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class EVAViewController: BaseViewController ,UITableViewDelegate,UITableViewDataSource{

    var scoreView: UIView!
    var LabelScoreAll: UILabel!
    var LabelScoreGood: UILabel!
    var LabelScoreConvey: UILabel!
    var tableView:UITableView!
    
    var titleView:UIView!
    var checkButton:UIButton!
    
    var comments = [Comment]()
    var cellHeight = [CGFloat]()
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initScoreView()
        initTitleView()

        testData()
        calcHeight()
    }
    func testData(){
        for _ in 0...10{
             comments.append(Comment(userName: "张三", comContent: "", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1))
            comments.append(Comment(userName: "张三", comContent: "这是一条内容", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1))
            comments.append(Comment(userName: "李四", comContent: "这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1))
            comments.append(Comment(userName: "李四", comContent: "这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1,storeReply: (content:"我来回复",time:"03-11")))
            comments.append(Comment(userName: "王五", comContent: "这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1))
            comments.append(Comment(userName: "李四", comContent: "这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容这是一条内容", date: "03-11", photoURL: "http://ww3.sinaimg.cn/large/006bQeGsjw1f2h5x1efslj31120kutcu.jpg", scoreGood: 4.6, scoreSpeed: 4.1,storeReply: (content:"我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复我来回复",time:"03-11")))
        }
    }
    func calcHeight(){
        let height12 = ("123" as NSString).sizeWithAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12)]).height
        let height14 = ("123" as NSString).sizeWithAttributes([NSFontAttributeName:UIFont.systemFontOfSize(14)]).height
        for x in comments{
            var height = 29 + height12
            let size = (x.comContent as NSString).sizeWithAttributes([NSFontAttributeName:UIFont.systemFontOfSize(14)])
            let lineWidth = ScreenWidth - 25
            let line:Int = size.width / lineWidth > 3.0 ? 3 : Int(ceil(size.width / lineWidth))
            height += CGFloat(line) * height14
            if(x.storeReply != nil){
                height += 14 + height12
                let size = (x.storeReply!.content as NSString).sizeWithAttributes([NSFontAttributeName:UIFont.systemFontOfSize(14)])
                let lineWidth = ScreenWidth - 25
                let line:Int = size.width / lineWidth > 3.0 ? 3 : Int(ceil(size.width / lineWidth))
                height += CGFloat(line) * height14 + 10
            }
            cellHeight.append(height)
         }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height)
        scoreView.frame = CGRect(x: 0, y: -119, width: self.view.frame.width, height: 68)
        titleView.frame = CGRect(x: 0, y: -40, width: self.view.frame.width, height: 40)
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.contentInset = UIEdgeInsets(top: 119, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -119)
        tableView.backgroundColor = UIColor.ViewMBKColor()
        tableView.registerReusableCellInClass(EVATableViewCell)
        tableView.clearOtherLine()
        
        self.view.addSubview(tableView)
    }
    
    func initScoreView(){
        scoreView = UIView()
        scoreView.backgroundColor = UIColor.whiteColor()
        tableView.addSubview(scoreView)
        let width = ScreenWidth / 3
        //综合评分
        let view1 = UIView()
        view1.backgroundColor = UIColor.whiteColor()
        scoreView.addSubview(view1)
        view1.frame = CGRect(x: 0, y: 0, width: width, height: 68)
        let label1 = UILabel()
        label1.text = "综合评分"
        label1.textColor = UIColor.ButtonCancelColor()
        label1.font = UIFont.systemFontOfSize(14)
        view1.addSubview(label1)
        label1.sizeToFit()
        label1.center = CGPoint(x: width / 2, y:  19)
        LabelScoreAll = UILabel()
        LabelScoreAll.font = UIFont.systemFontOfSize(30)
        view1.addSubview(LabelScoreAll)
        LabelScoreAll.center = CGPoint(x: width / 2, y: 49)
        
        //商品评价
        let view2 = UIView()
        view2.backgroundColor = UIColor.whiteColor()
        scoreView.addSubview(view2)
        view2.frame = CGRect(x: width, y: 0, width: width, height: 68)
        let label2 = UILabel()
        label2.text = "商品质量"
        label2.textColor = UIColor.ButtonCancelColor()
        label2.font = UIFont.systemFontOfSize(14)
        view2.addSubview(label2)
        label2.sizeToFit()
        label2.center = CGPoint(x: width / 2, y:  19)
        LabelScoreGood = UILabel()
        LabelScoreGood.font = UIFont.systemFontOfSize(30)
        view2.addSubview(LabelScoreGood)
        LabelScoreGood.center = CGPoint(x: width / 2, y:  49)
        
        //配送服务
        let view3 = UIView()
        view3.backgroundColor = UIColor.whiteColor()
        scoreView.addSubview(view3)
        view3.frame = CGRect(x: width + width, y: 0, width: width, height: 68)
        let label3 = UILabel()
        label3.text = "配送服务"
        label3.textColor = UIColor.ButtonCancelColor()
        label3.font = UIFont.systemFontOfSize(14)
        view3.addSubview(label3)
        label3.sizeToFit()
        label3.center = CGPoint(x: width / 2, y:  19)
        LabelScoreConvey = UILabel()
        LabelScoreConvey.font = UIFont.systemFontOfSize(30)
        view3.addSubview(LabelScoreConvey)
        LabelScoreConvey.center = CGPoint(x: width / 2, y:  49)
        LabelScoreAll.addObserver(self, forKeyPath: "text", options: .New, context: nil)
        LabelScoreGood.addObserver(self, forKeyPath: "text", options: .New, context: nil)
        LabelScoreConvey.addObserver(self, forKeyPath: "text", options: .New, context: nil)
    }
    
    func initTitleView(){
        titleView = UIView()
        titleView.backgroundColor = UIColor.whiteColor()
        tableView.addSubview(titleView)
        let labelTitle = UILabel()
        labelTitle.text = "全部评价"
        labelTitle.textColor = UIColor.ButtonCancelColor()
        labelTitle.font = UIFont.systemFontOfSize(16)
        titleView.addSubview(labelTitle)
        labelTitle.sizeToFit()
        labelTitle.center.y = 20
        labelTitle.frame.origin.x = 15
        
        checkButton = UIButton()
        checkButton.setImage(UIImage(named: "checked"), forState: .Normal)
        checkButton.setImage(UIImage(named:"check"), forState: .Selected)
        checkButton.setTitle("有内容的评价", forState: .Normal)
        checkButton.setTitleColor(UIColor.ButtonCancelColor(), forState: .Normal)
        checkButton.titleLabel?.font = UIFont.systemFontOfSize(14)
        checkButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        titleView.addSubview(checkButton)
        checkButton.sizeToFit()
        checkButton.frame.origin.x = ScreenWidth - 120
        checkButton.center.y = 20
        checkButton.addTarget(self, action: #selector(self.checkButtonClicked), forControlEvents: .TouchUpInside)
    }
    
    func checkButtonClicked(){
        //做一些操作
        checkButton.selected = !checkButton.selected
    }
    
    func setScore(good:Double,convey:Double){
        LabelScoreGood.text = String(format:"%.1f",good)
        LabelScoreConvey.text = String(format:"%.1f",convey)
        LabelScoreAll.text = String(format:"%.1f",(good + convey)/2)
    }
    
    deinit{
        
        LabelScoreAll.removeObserver(self, forKeyPath: "text")
        LabelScoreGood.removeObserver(self, forKeyPath: "text")
        LabelScoreConvey.removeObserver(self, forKeyPath: "text")
        
    }
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if(keyPath == "text"){
            let label = object as! UILabel
            switch Int(Double(label.text!)! * 10) {
            case 40...50:
                label.textColor = UIColor.redColor()
            default:
                label.textColor = UIColor.yellowColor()
            }
            label.sizeToFit()
            label.center = CGPoint(x: ScreenWidth / 6, y:  49)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //tableView代理
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    var pastCell: EVATableViewCell?
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as EVATableViewCell
        cell.object = comments[indexPath.row]
        
        cell.selectionStyle = .None
        pastCell = cell
        return cell
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       
        return cellHeight[indexPath.row] + 1
    }
    

}
