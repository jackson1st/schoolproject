//
//  MyViewController.swift
//  学子超市
//
//  Created by jason on 16/4/12.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class MyViewController: BaseViewController {

    @IBOutlet weak var ImgViewHeadPhoto: UIButton!
    @IBOutlet weak var LabelUserName: UILabel!
    var tableView:UITableView!
    var titles = [["我的红包","我的兑换劵"],["我的订单","管理收货地址"]]
    var ticketNum = [5,10]
    lazy var redBagViewController:RedBagCheckViewController = {
        return RedBagCheckViewController()
    }()
    lazy var ticketGoodViewController:TicketGoodCheckViewController = {
        return TicketGoodCheckViewController()
    }()
    lazy var recAddressesViewController:RecAddressesViewController = {
        let viewController = RecAddressesViewController()
        viewController.mode = 2
        return viewController
    }()
    lazy var myDetailViewController:MyDetailViewController = {
        let viewController = MyDetailViewController()
        viewController.saveAction = {
           [weak self] img,name in
            self?.ImgViewHeadPhoto.setImage(img, forState: .Normal)
            self?.LabelUserName.text = name
        }
        return viewController
    }()
    
    lazy var orderStatusViewController:OrderStatusViewController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = story.instantiateViewControllerWithIdentifier("OrderStatusViewController") as! OrderStatusViewController
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        ImgViewHeadPhoto.layer.cornerRadius = 35
        ImgViewHeadPhoto.clipsToBounds = true
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.backgroundColor = UIColor.ViewMBKColor()
        tableView.clearOtherLine()
        self.view.addSubview(tableView)
        tableView.frame = CGRectMake(0, 150, self.view.frame.width, self.view.frame.height - 198)
        tableView.registerReusableCell(XZTableViewCell)
    }
    @IBAction func ButtonHeadPhotoClicked() {
        self.navigationController?.pushViewController(myDetailViewController, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

}

extension MyViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 1){
            return 10
        }else{
            return 0
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0){
                self.navigationController?.pushViewController(redBagViewController, animated: true)
            }else{
                self.navigationController?.pushViewController(ticketGoodViewController, animated: true)
            }
        default:
            if(indexPath.row == 0){
                self.navigationController?.pushViewController(self.orderStatusViewController, animated: true)
            }else{
                self.navigationController?.pushViewController(recAddressesViewController, animated: true)
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as XZTableViewCell
        cell.title = titles[indexPath.section][indexPath.row]
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0){
                cell.info = "\(ticketNum[indexPath.row])个红包"
            }else{
                cell.info = "\(ticketNum[indexPath.row])张兑换劵"
            }
        default:
            cell.accessoryType = .DisclosureIndicator
        }
        return cell
    }
}
