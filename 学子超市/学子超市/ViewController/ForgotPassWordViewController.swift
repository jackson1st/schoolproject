//
//  ForgotPassWordViewController.swift
//  学子超市
//
//  Created by jason on 16/4/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class ForgotPassWordViewController: RegisterViewController {

    var phoneNumber:String!{
        didSet{
            TextFiledPhone.text = phoneNumber
        }
    }
    
    override init(){
        super.init()
        self.title = "找回密码"
        ButtonRegister.setTitle("确定", forState: .Normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func ButtonGetPhoneDigClicked() {
        print("获取验证码")
        ButtonGetPhoneDig.hidden = true
        LabelWait.text = "1:00"
        LabelWait.hidden = false
        setTime(60)
        //针对发送不同的请求
    }
    
    override func Register() {
        print("确定")
    }
}
