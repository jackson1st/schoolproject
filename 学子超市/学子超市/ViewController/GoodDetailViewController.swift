//
//  GoodDetailViewController.swift
//  学子超市
//
//  Created by jason on 16/4/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import RealmSwift
class GoodDetailViewController: ShopBaseViewController,ButtonBuyDelegate {

    @IBOutlet weak var ConstraintViewInfoTop: NSLayoutConstraint!
    @IBOutlet weak var ConstraintLabelDetailLeft: NSLayoutConstraint!
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var ActiveTag: UIImageView!
    @IBOutlet weak var LabelDetail: UILabel!
    @IBOutlet weak var LabelRealPrice: UILabel!
    @IBOutlet weak var LabelOriPrice: UILabel!
    @IBOutlet weak var LabelSell: UILabel!
    @IBOutlet weak var ButtonGo: ButtonBuy!
    let buttonChooseSize = UIButton()
    var shopCartObject:ShopCartGood?
    var object:Commodity!{
        didSet{
            let specsGood = object.mGoodsSpecs.firstObject as! GoodsSpecs
            self.shopCartObject = ShopCartGood(good: object, specsID: specsGood.id, specDescription: specsGood.specDescription, price: specsGood.currentPrice)
            photo.sd_setImageWithURL(NSURL(string: specsGood.imageUrl))
            LabelTitle.text = object.name
            if(specsGood.promotivePrice == 0){
                LabelOriPrice.hidden = true
                LabelRealPrice.attributedText = specsGood.currentPriceStr
            }else{
                LabelRealPrice.attributedText = specsGood.promotivePriceStr
                LabelOriPrice.attributedText = specsGood.currentPriceStr
            }
            LabelSell.text = "已售\(specsGood.buySum)件"
            if(object.isMultiSpec == true){
                ButtonGo.hidden = true
                buttonChooseSize.hidden = false
            }else{
                ButtonGo.hidden = false
                buttonChooseSize.hidden = true
                ButtonGo.num = object.buied
            }
        }
    }
    
    var photo:UIImageView!
    
    init(){
        super.init(nibName: "GoodDetailViewController", bundle: nil)
        loadView()
        self.view.backgroundColor = UIColor.ViewMBKColor()
        self.view.sendSubviewToBack(contentView)
        ButtonGo.delegate = self
        self.ButtonGo.superview!.addSubview(buttonChooseSize)
        buttonChooseSize.setTitle("选择规格", forState: .Normal)
        buttonChooseSize.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        buttonChooseSize.titleLabel?.font = UIFont.systemFontOfSize(14)
        buttonChooseSize.layer.cornerRadius = 4
        buttonChooseSize.layer.borderWidth = 0.7
        buttonChooseSize.layer.borderColor = UIColor.lightGrayColor().CGColor
        buttonChooseSize.addTarget(self, action: #selector(self.chooseSize), forControlEvents: .TouchUpInside)
        ButtonGo.layoutIfNeeded()
        buttonChooseSize.frame = CGRect(x: ButtonGo.frame.origin.x + 15, y: ButtonGo.frame.origin.y + 6, width: ScreenWidth * 0.18, height: ScreenHeight * 0.033)
        initPhoto()
        self.title = "商品详情"
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    func initPhoto(){
        let BKView = UIView()
        self.view.addSubview(BKView)
        BKView.backgroundColor = UIColor.whiteColor()
        BKView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
        photo = UIImageView()
        self.view.addSubview(photo)
        photo.contentMode = UIViewContentMode.ScaleAspectFit
        photo.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   //该方法已经不会调用
    
    
    //处理购买
    func chooseSize(){
        NSNotificationCenter.defaultCenter().postNotificationName(NotiPushAlertSizeChoose, object: self, userInfo: ["object":self.object])
    }
    
    func dec(sender:UIButton,oldNum:Int,newNum:Int){
        object.buied -= 1
        ShopCartGood.decOne(self.shopCartObject!)
        NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":object.id,"num":newNum])
    }
    func inc(sender:UIButton,oldNum:Int,newNum:Int){
        object.buied += 1
        ShopCartGood.addOne(self.shopCartObject!.oneCopy())
        NSNotificationCenter.defaultCenter().postNotificationName(NotiGoodNumChange, object: nil, userInfo: ["goodID":object.id,"num":object.buied,"view":sender])
    }

    
    override func GoodNumChange(not: NSNotification) {
        super.GoodNumChange(not)
        let info = not.userInfo as! [String:AnyObject]
        let id = info["goodID"] as! String
        let num = info["num"] as! Int
            if(object.id == id && object.isMultiSpec == false){
                object.buied = num
                ButtonGo.num = num
            }else{
                object.buied = 0
        }
    }
    
    override func ShopCartClear() {
        object.buied = 0
        ButtonGo.num = 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
