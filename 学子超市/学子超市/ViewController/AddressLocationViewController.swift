//
//  AddressLocationViewController.swift
//  学子超市
//
//  Created by jason on 16/3/20.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit


class AddressLocationViewController: BaseViewController,MAMapViewDelegate {

    private let mapViewWidth:CGFloat = ScreenWidth
    private let mapViewHeight:CGFloat = ScreenHeight * 0.3
    
    struct address{
        var name:String
        var loc:String
        var point:AMapGeoPoint
        
        init(name:String,loc:String,point:AMapGeoPoint){
            self.name = name
            self.loc = loc
            self.point = point
        }
        
        init(poi:AMapPOI){
            self.name = poi.name
            self.loc = poi.address
            self.point = poi.location
        }
        
    }
    
    var muliAddressPageVC:MuliPageViewController!
    var SelectedAction:((name:String,point:AMapGeoPoint) -> Void)!
    
    var mapView:MAMapView!
    var annotionImgView:UIImageView!
    lazy var locationButton:UIButton! = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "locationButton"), forState: .Normal)
        btn.addTarget(self, action: #selector(AddressLocationViewController.needLocation), forControlEvents: .TouchUpInside)
        btn.sizeToFit()
        return btn
    }()
    var needUserLocation = true
    
    func needLocation(){
        needUserLocation = true
    }
    lazy var searchButton:UIButton! = {
        let btn = UIButton()
        btn.frame.size = CGSize(width: ScreenWidth,height: 30)
        btn.setImage(UIImage(named: "locSearch"), forState: .Normal)
        btn.addTarget(self, action: #selector(AddressLocationViewController.gotoSearch), forControlEvents: .TouchUpInside)
        btn.backgroundColor = UIColor.whiteColor()
        btn.layer.cornerRadius = 6
        return btn
    }()
    var searchVC:AddressSearchViewController?
    func gotoSearch(){
        if(searchVC == nil){
            searchVC = AddressSearchViewController()
            searchVC?.SelectedAction = {
                [weak self] name,point in
                self!.SelectedAction(name: name,point: point)
            }
        }
        self.navigationController?.pushViewController(self.searchVC!, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initMapView()
        initTableViewes()
        initmuliPage()
        self.navigationItem.titleView = searchButton
    }
    
    
    var locCoordinate:CLLocationCoordinate2D!
    var locCoordinates = [CLLocationCoordinate2D(),CLLocationCoordinate2D(),CLLocationCoordinate2D(),CLLocationCoordinate2D()]//存放每个tableview的定位地址，更新用
    var tableViewes = [UITableView]()
    var pageTitles = ["全部","学校","小区","写字楼"]
    var addressType = ["餐饮服务|商务住宅|生活服务|科教文化服务","1412","120302|120300|120301|120303|120304","120100|120200|120201|120202|120203"]
    var addressDate = [[address](),[address](),[address](),[address]()]
    var pageIndex = 0
    lazy var POISearch:AMapSearchAPI! = {
        let search = AMapSearchAPI()
        search.delegate = self
        return search
    }()
    lazy var POIRequest:AMapPOIAroundSearchRequest! = {
        let request = AMapPOIAroundSearchRequest()
        request.radius = 50000
        request.sortrule = 0
        return request
    }()
    func initmuliPage(){
        muliAddressPageVC = MuliPageViewController(titles: pageTitles, views: tableViewes)
        muliAddressPageVC.pageTitleFont = UIFont.systemFontOfSize(15)
        muliAddressPageVC.headViewHeight = 40
        muliAddressPageVC.pageAction = {
           [weak self]  index in
            self!.pageIndex = index
            if(self!.locCoordinate.isEqual(self!.locCoordinates[index]) == false){
                self!.locCoordinates[index] = self!.locCoordinate
                self!.tableViewes[index].startLoading()
                self!.POIRequest.location = AMapGeoPoint.locationWithLatitude(CGFloat(self!.locCoordinate.latitude), longitude: CGFloat(self!.locCoordinate.longitude))
                self!.POIRequest.types = self?.addressType[index]
                self!.POISearch.AMapPOIAroundSearch(self!.POIRequest)
            }
        }
        self.addChildViewController(muliAddressPageVC)
        self.view.addSubview(muliAddressPageVC.view)
        muliAddressPageVC.view.frame = CGRect(x: 0, y: mapView.frame.origin.y + mapView.frame.height, width: ScreenWidth, height: self.view.frame.height - mapView.frame.origin.y - mapView.frame.height)
    }
    
    func tableViewReloadDataAtIndex( index:Int){
        self.locCoordinates[index] = self.locCoordinate
        self.tableViewes[index].startLoading()
        self.POIRequest.location = AMapGeoPoint.locationWithLatitude(CGFloat(self.locCoordinate.latitude), longitude: CGFloat(self.locCoordinate.longitude))
        self.POIRequest.types = self.addressType[index]
        self.POISearch.AMapPOIAroundSearch(self.POIRequest)
    }
    
    func initTableViewes(){
        for i in 0 ..< pageTitles.count{
            let tableview = UITableView()
            tableview.delegate = self
            tableview.dataSource = self
            tableview.tag = i
            tableview.clearOtherLine()
            tableview.registerReusableCell(LocationAddressTableViewCell)
            tableViewes.append(tableview)
        }
    }
    
    func initMapView(){
        mapView = MAMapView()
        mapView.delegate = self
        mapView.userTrackingMode = .Follow
        let locPre = MAUserLocationRepresentation()
        locPre.fillColor = UIColor.MainColor()
        locPre.strokeColor = UIColor.whiteColor()
        locPre.lineWidth = 3
        mapView.updateUserLocationRepresentation(locPre)

        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.Denied && CLLocationManager.locationServicesEnabled()){
            mapView.showsUserLocation = true
        }
        mapView.zoomEnabled = true
        mapView.frame = CGRect(x: 0, y: 64, width: self.mapViewWidth, height: self.mapViewHeight)
        self.view.addSubview(mapView)
        
        annotionImgView = UIImageView(image: UIImage(named: "locWithShadow"))
        annotionImgView.sizeToFit()
        view.addSubview(annotionImgView)
        annotionImgView.frame.origin = CGPoint(x: self.mapViewWidth/2 - annotionImgView.frame.width/2, y: self.mapViewHeight/2 + 64 - annotionImgView.frame.height)
        //定位按钮
        view.addSubview(locationButton)
        locationButton.frame.origin = CGPoint(x: 15, y: 64 + mapViewHeight - 15 - locationButton.frame.width)
    }
    
    func mapView(mapView: MAMapView!, didUpdateUserLocation userLocation: MAUserLocation!, updatingLocation: Bool) {
        if(needUserLocation == true){
            needUserLocation = false
            mapView.setZoomLevel(16.1,animated: true)
            mapView.setCenterCoordinate(userLocation.coordinate, animated: true)
            locCoordinate = userLocation.coordinate
            tableViewReloadDataAtIndex(pageIndex)
        }
    }
    
    func mapView(mapView: MAMapView!, mapWillMoveByUser wasUserAction: Bool) {
        UIView.animateWithDuration(0.2) { [weak self] in
            self!.annotionImgView.frame.origin.y -= 30
        }
    }
    
    func mapView(mapView: MAMapView!, mapDidMoveByUser wasUserAction: Bool) {
        locCoordinate = mapView.centerCoordinate
        if(wasUserAction == true){
            tableViewReloadDataAtIndex(pageIndex)
        }
        UIView.animateWithDuration(0.2) { [weak self] in
            self!.annotionImgView.frame.origin.y += 30
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

extension AddressLocationViewController:UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate{
    
    func onPOISearchDone(request: AMapPOISearchBaseRequest!, response: AMapPOISearchResponse!) {
        addressDate[pageIndex].removeAll()
        for x in response.pois{
            addressDate[pageIndex].append(address(poi: x as! AMapPOI))
        }
        
        self.tableViewes[pageIndex].endLoading()
        self.tableViewes[pageIndex].reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressDate[pageIndex].count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(indexPath: indexPath) as LocationAddressTableViewCell
        cell.TextLabel?.text = addressDate[pageIndex][indexPath.row].name
        cell.DetailLabel?.text = addressDate[pageIndex][indexPath.row].name
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        SelectedAction(name: addressDate[pageIndex][indexPath.row].name,point: addressDate[pageIndex][indexPath.row].point)
        navigationController?.popViewControllerAnimated(true)
    }
}
