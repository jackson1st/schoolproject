//
//  EditRecAddressController.swift
//  学子超市
//
//  Created by jason on 16/3/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class EditRecAddressController: BaseViewController,UITableViewDelegate,UITableViewDataSource{

    var tableView:UITableView!
    var recAddress = RecAddress()
    var SaveAction: (RecAddress) -> Void = {
        object in
        
    }
    var mode:Int = 0//0表示新增    1表示修改
    
    lazy var textFieldName:UITextField = {
        let textfiled = UITextField()
        textfiled.placeholder = "请输入您的姓名"
        return textfiled
    }()
    lazy var textFieldPhone:UITextField = {
        let textfiled = UITextField()
        textfiled.keyboardType = .NumberPad
        textfiled.placeholder = "请输入您的手机号码"
        return textfiled
    }()
    lazy var buttonSexMan:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"selected"), forState: .Selected)
        btn.setImage(UIImage(named:"imSelected"), forState: .Normal)
        btn.setTitle(" 男", forState: .Normal)
        btn.setTitleColor(UIColor.MainColor(), forState: .Selected)
        btn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn.addTarget(self, action: #selector(EditRecAddressController.buttonSexClicked(_:)), forControlEvents: .TouchUpInside)
        btn.selected = true
        btn.tag = 101
        btn.sizeToFit()
        return btn
    }()
    lazy var buttonSexFemale:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"selected"), forState: .Selected)
        btn.setImage(UIImage(named:"imSelected"), forState: .Normal)
        btn.setTitle(" 女", forState: .Normal)
        btn.setTitleColor(UIColor.MainColor(), forState: .Selected)
        btn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn.addTarget(self, action: #selector(EditRecAddressController.buttonSexClicked(_:)), forControlEvents: .TouchUpInside)
        btn.tag = 102
        btn.sizeToFit()
        return btn
    }()
    var buttonSex:UIButton!
    func buttonSexClicked(sender:UIButton){
        buttonSex.selected = false
        buttonSex = sender
        buttonSex.selected = true
    }
    
    lazy var textFiledLocation:UITextField = {
       let textfield = UITextField()
        textfield.placeholder = "学校/小区等"
        textfield.userInteractionEnabled = false
        return textfield
    }()
    
    lazy var textFieldDetail:UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "门牌号/楼层号等详细地址"
        return textfield
    }()
    
    lazy var viewSex:UIView = {
        let view = UIView()
        view.addSubview(self.buttonSexMan)
        view.addSubview(self.buttonSexFemale)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSex = buttonSexMan
        initTableView()
        initSaveBarButton()
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(mode == 1){
            textFieldName.text = recAddress.receiverName
            textFieldPhone.text = recAddress.receiverPhone
            buttonSexClicked(recAddress.receiverSex == true ? buttonSexMan:buttonSexFemale)
            textFiledLocation.text = recAddress.locAddress
            textFieldDetail.text = recAddress.detailAddress
        }
    }
    
    func initSaveBarButton(){
        let barbuttonItem = UIBarButtonItem(title: "保存", style: .Plain, target: self, action: #selector(EditRecAddressController.save))
        self.navigationItem.rightBarButtonItem = barbuttonItem
    }
    
    func save(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "好的", style: .Cancel, handler: nil))
        if(textFieldName.text?.characters.count < 2){
            alert.message = "名字不能少于2个字"
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        recAddress.receiverName = textFieldName.text
        if(textFieldPhone.text == nil || textFieldPhone.text?.characters.count < 11){
            alert.message = "手机号码必须11位"
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        recAddress.receiverPhone = textFieldPhone.text
        recAddress.receiverSex = buttonSex.tag == 101
        if(textFiledLocation.text == nil || textFiledLocation.text == ""){
            alert.message = "请先定位"
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        if(textFieldDetail.text == nil || textFieldDetail.text == ""){
            alert.message = "请填写详细地址"
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        recAddress.detailAddress = textFieldDetail.text
        SaveAction(recAddress)
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.keyboardDismissMode = .Interactive
        tableView.registerReusableCell(XZTableViewCell)
        tableView.clearOtherLine()
        self.view.addSubview(tableView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        buttonSexMan.center.y = viewSex.frame.height / 2
        buttonSexFemale.center.y = viewSex.frame.height / 2
        buttonSexMan.frame.origin.x = 15
        buttonSexFemale.frame.origin.x = buttonSexMan.frame.maxX + 40
    }
    
    /**
     *  tableView代理
     */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as XZTableViewCell
        switch(indexPath.row){
        case 0:
            cell.LabelTitle.text = "我的姓名"
            cell.ViewExtend = textFieldName
            cell.selectionStyle = .None
        case 1:
            cell.LabelTitle.text = "手机号码"
            cell.ViewExtend = textFieldPhone
            cell.selectionStyle = .None
        case 2:
            cell.LabelTitle.text = "我的性别"
            cell.ViewExtend = viewSex
            cell.selectionStyle = .None
        case 3:
            cell.LabelTitle.text = "我的地址"
            cell.ViewExtend = textFiledLocation
        case 4:
            cell.LabelTitle.text = "详细地址"
            cell.ViewExtend = textFieldDetail
            cell.selectionStyle = .None
        default:
            break;
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row == 3 {
            let vc = AddressLocationViewController()
            vc.SelectedAction = {
                [weak self] name,point in
                self!.recAddress.locAddress = name
                self!.recAddress.coordinate = CLLocation(latitude: Double(point.latitude), longitude: Double(point.longitude))
                self!.textFiledLocation.text = name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
