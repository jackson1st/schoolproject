//
//  RecAddressesViewController.swift
//  学子超市
//
//  Created by jason on 16/3/24.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class RecAddressesViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    lazy var bottomButton:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.MainColor()
        btn.setTitle("新增收货地址", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.titleLabel?.font = UIFont.systemFontOfSize(18)
        btn.addTarget(self, action: #selector(RecAddressesViewController.bottomButtonAction), forControlEvents: .TouchUpInside)
        return btn
    }()
    var bottomButtonHeight:CGFloat = 40
    
    var selectAddressAction:(RecAddress) -> Void = {
        object in
    }
    lazy var editViewController:EditRecAddressController = {
        let vc = EditRecAddressController()
        vc.title = "修改资料"
        return vc
    }()
    func bottomButtonAction(){
        let vc = EditRecAddressController()
        vc.title = "新增收货地址"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var recAddresses = [RecAddress]()
    var selectedIndex:Int =  -1
    var tableView:UITableView!
    func initTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerReusableCell(RecAddressTableViewCell)
        tableView.clearOtherLine()
        tableView.bounces = false
        tableView.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: self.view.frame
            .height - bottomButtonHeight)
        self.view.addSubview(tableView)
    }
    var mode = 1//1表示选择收货地址，2表示查看和编辑收货地址
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "收货地址"
        initTableView()
        view.addSubview(bottomButton)
        bottomButton.snp_makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view)
            make.height.equalTo(self.bottomButtonHeight)
        }
        NetWorkManager.GetRecAddress({
           [weak self]  recaddress in
            if let sSelf = self {
                sSelf.recAddresses = recaddress
                sSelf.tableView.reloadData()
            }
        })
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if(self.navigationController?.navigationBarHidden == true){
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recAddresses.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as RecAddressTableViewCell
        cell.LabelName.text = recAddresses[indexPath.row].receiverName
        cell.LabelSex.text = recAddresses[indexPath.row].receiverSex == true ? "男":"女"
        cell.LabelPhone.text = recAddresses[indexPath.row].receiverPhone
        cell.LabelDetail.text = recAddresses[indexPath.row].detail
        if(mode == 1){
        cell.checkMark = indexPath.row == selectedIndex
        }else{
            cell.edit = true
        }
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(mode == 2){
            return true
        }
        return false
    }
    
    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "删除"
    }
    
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .Delete
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if(editingStyle == .Delete){
            print("删除")
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(mode == 1){
            if(indexPath.row != selectedIndex){
                selectedIndex = indexPath.row
            }
            self.selectAddressAction(recAddresses[selectedIndex])
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            editViewController.recAddress = recAddresses[indexPath.row]
            editViewController.mode = 1
            editViewController.SaveAction = {
                [weak self] object in
                self?.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            }
            self.navigationController?.pushViewController(editViewController, animated: true)
        }
    }

}
