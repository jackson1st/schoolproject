//
//  SuperMarketViewController.swift
//  学子超市
//
//  Created by jason on 16/3/30.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import RealmSwift
class SuperMarketViewController: ShopBaseViewController {

    let classWidth = ScreenWidth * 0.224
    let classHeight = ScreenHeight * 0.076
    
    var ClassTableView:UITableView!
    var GoodCollectionView:UICollectionView!
    var secondClassView:MuliPageHeaderView!
    var items = [Int:[Commodity]]()
    var indexs = [String:Int]()
    var classes = [CommodityClass]()
    var secondClass:[CommodityClass]!{
        didSet{
            var titles = [String]()
            for x in secondClass{
                titles.append(x.name)
            }
            secondClassView.setButtons(titles)
        }
    }
    
    var indexForClass = 0{
        didSet{
            secondClass = classes[indexForClass].subClass as! [CommodityClass]
        }
    }
    
    lazy var goodDetailViewController:GoodDetailViewController! = {
        return GoodDetailViewController()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initClassTableView()
        initCollectionView()
        initSecondClassView()
        NetWorkManager.GetClassesAndFirstInfo {[weak self] indexs,classes,items in
            if let sSelf = self {
                sSelf.classes = classes
                sSelf.indexForClass = 0
                sSelf.indexs = indexs
                sSelf.items[0] = items
                sSelf.GoodCollectionView.reloadData()
                sSelf.ClassTableView.reloadData()
                sSelf.ClassTableView.selectRowAtIndexPath(NSIndexPath(forRow: sSelf.indexForClass, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ClassTableView.frame = CGRect(x: 0, y: 0, width: classWidth, height: self.contentView.frame.height)
        GoodCollectionView.frame = CGRect(x: classWidth, y: 0, width: ScreenWidth - classWidth, height: self.contentView.frame.height)
        secondClassView.frame = CGRect(x: classWidth, y: 0, width: ScreenWidth - classWidth, height: 30)
    }
    
    override func GoodNumChange(not: NSNotification) {
        let info = not.userInfo as! [String:AnyObject]
        let ID = info["goodID"] as! String
        let num = info["num"] as! Int
        if let btn = info["view"] as? UIButton{
            self.addAitem(btn.convertRect(btn.bounds, toView: self.view).origin)
        }
        for i in 0..<items.count{
            for j in 0..<items[i]!.count{
                if(items[i]![j].id == ID && items[i]![j].isMultiSpec == false){
                    if i == indexForClass{
                    items[i]![j].buied = num
                    GoodCollectionView.reloadItemsAtIndexPaths([NSIndexPath(forRow: j, inSection: 0)])
                    }
                }
            }
        }
    }
    
    override func ShopCartClear() {
        for i in 0..<goods.count{
            goods[i].buied = 0
        }
        GoodCollectionView.reloadData()
    }
    
    func initClassTableView(){
        ClassTableView = UITableView()
        ClassTableView.delegate = self
        ClassTableView.dataSource = self
        ClassTableView.bounces = false
        ClassTableView.showsVerticalScrollIndicator = false
        ClassTableView.clearOtherLine()
        ClassTableView.registerReusableCellInClass(GoodClassTableViewCell)
        ClassTableView.backgroundColor = UIColor.ViewMBKColor()
        ClassTableView.layoutMargins = UIEdgeInsetsZero
        ClassTableView.separatorInset = UIEdgeInsetsZero
        self.contentView.addSubview(ClassTableView)
        
    }
    
    func initCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical
        layout.minimumLineSpacing = 1
        layout.itemSize = CGSize(width: ScreenWidth - classWidth,height: 90)
        self.GoodCollectionView = UICollectionView(frame: CGRect(x: classWidth, y: 0, width: ScreenWidth - classWidth, height: self.view.frame.height), collectionViewLayout: layout)//高度有坑
        GoodCollectionView.backgroundColor = UIColor.ViewMBKColor()
        GoodCollectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        GoodCollectionView.delegate = self
        GoodCollectionView.dataSource = self
        GoodCollectionView.bounces = false
        GoodCollectionView.registerNib(UINib(nibName: "GoodVCollectionViewCell",bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cellH")
        self.contentView.addSubview(GoodCollectionView)
    }
    
    func initSecondClassView(){
        secondClassView = MuliPageHeaderView()
        secondClassView.pageDelegate = self
        self.contentView.addSubview(secondClassView)
        secondClassView.bottomLineEnable = false
        secondClassView.hilightColor = UIColor.MainColor()
        secondClassView.shadowBottomLine.hidden = false
        secondClassView.TitleFont = UIFont.systemFontOfSize(14)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension SuperMarketViewController:UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,MuliPageHeaderViewDelegate{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return classes.count
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return classHeight
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return classHeight
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsetsZero
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as GoodClassTableViewCell
        cell.textLabel?.text = classes[indexPath.row].name
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Middle, animated: true)
        //加载
        if items[indexPath.row] == nil {
            
            NetWorkManager.GetItemsByBackID(classes[indexPath.row].id, Block: {[weak self]  indexs, items in
                indexs.forEach({ [weak self] index in
                    if let sSelf = self {
                        sSelf.indexs[index.0] = index.1
                    }
                })
                if let sSelf = self {
                    sSelf.items[indexPath.row] = items
                }
                self?.indexForClass = indexPath.row
                self?.GoodCollectionView.reloadData()
            })
            
        }else{
            
            self.indexForClass = indexPath.row
            self.GoodCollectionView.reloadData()
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items[indexForClass]!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellH", forIndexPath: indexPath) as! GoodVCollectionViewCell
        cell.mode = 2
        cell.object = items[indexForClass]![indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        goodDetailViewController.object = items[indexForClass]![indexPath.row]
        self.navigationController?.pushViewController(goodDetailViewController, animated: true)
    }
    
    
    func pageSelectedOfIndex(index:Int){
        
        GoodCollectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: indexs[secondClass[index].id]!, inSection: indexForClass), atScrollPosition: .Top, animated: true)
    }
    func pageSelected(to:Int,from:Int){
        
    }

}
