    //
//  NetWork.swift
//  学子超市
//
//  Created by Jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//
import Foundation
import Alamofire
import MBProgressHUD
//var path = "http://192.168.31.3:8080/stumarket/"
    let path = "http://10.128.6.217:8080/stumarket"
    //http://192.168.31.3:8080/stumarket/ShopGoods/getSGoodsOnShowType.do
public enum NetDestination:String{
    var url:String {
        get{
            return path + self.rawValue
        }
    }
    case GoodsInfoAndType = "ShopGoods/getGoodsInfoAndType.do" //获取店铺商品列表
    case AppGoodsByPId = "ShopGoods/getAppGoodsByPId.do" //获取商品大类信息
    case GoodsSpec = "ShopGoods/getGoodsSpec.do"//查询具体规格的商品
    case GoodOnShow = "ShopGoods/getSGoodsOnShowType.do"//请求展示类目
    case RBandGTSum = "AppShopActiManager/getEXAndRPSum.do"//获取可用红包和可用兑换劵的数量
    case CurrentRB = "AppShopActiManager/getCurrentRedPaperByTel.do"//获取当前可用红包
    case HistoryRB = "/AppShopActiManager/getPastRedPaperByTel.do"//历史红包
    case CurrentGT = "/AppShopActiManager/getCurrentEXByTel.do"//当前兑换劵
    case HistoryGT = "/AppShopActiManager/getPastExByTel.do"//历史兑换劵
    case GetRBadnGT = "/AppShopActiManager/getCustomerGet.do"//领取兑换劵或红包
    
    case Login = "/customer/loginByPw.do"
    case SendMsg = "/customer/getMsg.do"//发送验证码
    case LoginByMsg = "/customer/loginByMsg.do"//手机验证码登录
    case Register = "/customer/regist.do"
    case ModifyInfo = "/customer/modifyCustomer.do"//修改用户信息
    case SubmitOrder = "/order/PreAddOrder.do"//下单
    case ChangeRB = "/order/changeRedPapper.do"//更换红包
    case ChooseGT = "/order/chooseExchange.do"//使用兑换劵
    case CommitOrder = "/order/addOrder.do"//确定下单
    case Pay = "/order/payOrderMoney.do"//付款
    case CancelOrder = "/order/cancelOrder.do"//取消订单
    case OrderList = "/order/showCustomerOrder.do"//查询订单
    case RecAddressList = "/customer/getAllAddressDetail.do"//送货地址列表
    case AddRecAddress = "/customer/addAddress.do"//添加送货地址
}

public class NetWorkManager{
    class func GetClassesAndFirstInfo(Block:([String:Int],goodClass:[CommodityClass],goods:[Commodity])->Void){
        
//        let mutDict = NSMutableDictionary()
//        XZRequest(.GoodsInfoAndType, parameters: ["shopId":1]).XZResponse { (JSON) in
//            print("-----请求店铺商品列表------")
//            if JSON["message"] as! String == "success"{
//                
//            }else{
//                MBProgressHUD.showMessage(JSON["message"] as! String, view: nil)
//            }
//        }
        
        let jsonData = "{\"childrenTypeId\":[\"10\",\"11\"],\"itemsBack\":[{\"childrenList\":[{\"childrenList\":[],\"id\":\"10\",\"name\":\"速食面\",\"position\":0},{\"childrenList\":[],\"id\":\"11\",\"name\":\"泡面搭档\",\"position\":0}],\"id\":\"1\",\"name\":\"方便速食\",\"position\":1},{\"childrenList\":[{\"childrenList\":[],\"id\":\"12\",\"name\":\"八宝粥\",\"position\":0},{\"childrenList\":[],\"id\":\"13\",\"name\":\"水果罐头\",\"position\":0}],\"id\":\"2\",\"name\":\"罐头食品\",\"position\":2},{\"childrenList\":[{\"childrenList\":[],\"id\":\"14\",\"name\":\"葡萄酒\",\"position\":0},{\"childrenList\":[],\"id\":\"15\",\"name\":\"啤酒\",\"position\":0},{\"childrenList\":[],\"id\":\"16\",\"name\":\"鸡尾酒\",\"position\":0},{\"childrenList\":[],\"id\":\"28\",\"name\":\"RIO\",\"position\":0}],\"id\":\"3\",\"name\":\"畅饮酒类\",\"position\":3},{\"childrenList\":[{\"childrenList\":[],\"id\":\"31\",\"name\":\"奶茶\",\"position\":0}],\"id\":\"30\",\"name\":\"咖啡奶茶\",\"position\":0},{\"childrenList\":[{\"childrenList\":[],\"id\":\"33\",\"name\":\"奶茶\",\"position\":0}],\"id\":\"32\",\"name\":\"咖啡奶茶\",\"position\":6},{\"childrenList\":[{\"childrenList\":[],\"id\":\"17\",\"name\":\"水\",\"position\":0},{\"childrenList\":[],\"id\":\"18\",\"name\":\"饮料\",\"position\":0}],\"id\":\"4\",\"name\":\"饮料、水\",\"position\":4},{\"childrenList\":[{\"childrenList\":[],\"id\":\"19\",\"name\":\"酸奶\",\"position\":0},{\"childrenList\":[],\"id\":\"20\",\"name\":\"纯奶\",\"position\":0}],\"id\":\"5\",\"name\":\"牛奶乳品\",\"position\":5},{\"childrenList\":[],\"id\":\"6\",\"name\":\"冷冻酸奶\",\"position\":0},{\"childrenList\":[],\"id\":\"7\",\"name\":\"冰棒雪糕\",\"position\":0},{\"childrenList\":[],\"id\":\"8\",\"name\":\"新鲜面包\",\"position\":0},{\"childrenList\":[],\"id\":\"9\",\"name\":\"休闲零食\",\"position\":0}],\"GoodsAndCId\":[{\"cTypeId\":\"10\",\"shopinfoList\":[{\"backTypeGoodsPosition\":\"0\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"商品描述\",\"goodsSpecIds\":\"ss001605280008\",\"id\":\"sg001605280001\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/495625641/TB2n792mXXXXXbfXXXXXXXXXXXX_!!495625641.jpg_230x230.jpg\",\"isMultiSpec\":\"false\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":13.8,\"discount\":1,\"goodsInfoId\":\"sg001605280001\",\"id\":\"ss001605280008\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/495625641/TB2n792mXXXXXbfXXXXXXXXXXXX_!!495625641.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"10\",\"onshowTypeId\":\"0\",\"promotivePrice\":13.8,\"remark\":\"备注\",\"specDescription\":\"口味：水蜜桃\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"11\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"xff\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"1\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"1-2-18-25\",\"id\":\"1\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/495625641/TB2n792mXXXXXbfXXXXXXXXXXXX_!!495625641.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":200,\"code\":\"00000000\",\"currentPrice\":3.9,\"discount\":0,\"goodsInfoId\":\"1\",\"id\":\"2\",\"imageUrl\":\"https://gd4.alicdn.com/imgextra/i4/495625641/TB2ZkiVmXXXXXbYXXXXXXXXXXXX_!!495625641.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"2\",\"onshowTypeId\":\"2\",\"promotivePrice\":3.8,\"remark\":\"fer\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"1\",\"id\":\"18\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/820649583/TB2J.4fpXXXXXa0XFXXXXXXXXXX_!!820649583.jpg_230x230xz.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"1\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"1\",\"id\":\"25\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/893485989/TB2UptPpVXXXXX6XpXXXXXXXXXX_!!893485989.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":100,\"code\":\"000000000\",\"currentPrice\":3.9,\"discount\":0,\"goodsInfoId\":\"1\",\"id\":\"1\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1t.BdMXXXXXcJXFXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"1\",\"onshowTypeId\":\"1\",\"promotivePrice\":3.8,\"remark\":\"fernny\",\"specDescription\":\"口味：红烧牛肉\",\"state\":1,\"unit\":\"桶\"}],\"merchantGoodsInfoId\":\"1\",\"name\":\"康师傅桶面\",\"onshowTypeGoodsPosition\":\"1\",\"onshowTypeId\":\"2\",\"remark\":\"fernny\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"桶\"},{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"3-4-26\",\"id\":\"2\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"2\",\"id\":\"26\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"9\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"袋\"},{\"buySum\":80,\"code\":\"00000000\",\"currentPrice\":2.9,\"discount\":0.98,\"goodsInfoId\":\"2\",\"id\":\"4\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"4\",\"onshowTypeId\":\"2\",\"promotivePrice\":2.8,\"remark\":\"\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"袋\"}],\"merchantGoodsInfoId\":\"2\",\"name\":\"康师傅袋面\",\"onshowTypeGoodsPosition\":\"2\",\"onshowTypeId\":\"2\",\"remark\":\"\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"袋\"},{\"backTypeGoodsPosition\":\"3\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"13-14-27\",\"id\":\"8\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"8\",\"id\":\"14\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"9\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"16-17\",\"id\":\"15\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"15\",\"id\":\"16\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"10\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"11-12\",\"id\":\"7\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"7\",\"id\":\"11\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpghttps://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"fernny\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"5\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"}]},{\"cTypeId\":\"11\",\"shopinfoList\":[{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"11\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"5-24\",\"id\":\"3\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"3\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"3\",\"id\":\"24\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":3,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"1\",\"promotivePrice\":3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"包\"}],\"merchantGoodsInfoId\":\"3\",\"name\":\"双汇王中王优级火腿肠\",\"onshowTypeGoodsPosition\":\"3\",\"onshowTypeId\":\"1\",\"remark\":\"\",\"specNames\":\"nospec\",\"typeName\":\"泡面搭档\",\"unit\":\"包\"}]}],\"msg\":\"success\"}".dataUsingEncoding(NSUTF8StringEncoding)
        do {

            let JSON = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.AllowFragments)
//            print(JSON)
            var titles = [CommodityClass]()
            var items = [Commodity]()
            var indexs = [String:Int]()
            if let itemsBack = JSON["itemsBack"] as? NSArray{
                itemsBack.forEach({ (object) in
                    titles.append(CommodityClass.yy_modelWithDictionary(object as! [String:AnyObject])!)
                })
            }
            if let goodsAndCID = JSON["GoodsAndCId"] as? NSArray{
                var sum = 0
                for object in goodsAndCID{
                    if let object = object as? [String:AnyObject] {
                        let id = object["cTypeId"] as! String
                        indexs[id] = sum
                        if let shopList = object["shopinfoList"] as? [[String:AnyObject]]{
                            for x in shopList{
                                items.append(Commodity.yy_modelWithDictionary(x)!)
                            }
                        }
                    }
                    sum = items.count
                }
            }
            
            Block(indexs,goodClass: titles, goods: items)
        }catch let error{
            print(error)
        }
        
    }
    
    
    class func GetItemsByBackID(id:String,Block:(indexs:[String:Int],items:[Commodity])->Void){
        let jsonData = "{\"childrenTypeId\":[\"10\",\"11\"],\"GoodsAndCId\":[{\"cTypeId\":\"10\",\"shopinfoList\":[{\"backTypeGoodsPosition\":\"0\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"商品描述\",\"goodsSpecIds\":\"ss001605280008\",\"id\":\"sg001605280001\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i3/187880132479937231/TB20D_IgpXXXXXRXpXXXXXXXXXX_!!96228788-0-saturn_solar.jpg_230x230.jpg\",\"isMultiSpec\":\"false\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":13.8,\"discount\":1,\"goodsInfoId\":\"sg001605280001\",\"id\":\"ss001605280008\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i3/187880132479937231/TB20D_IgpXXXXXRXpXXXXXXXXXX_!!96228788-0-saturn_solar.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"10\",\"onshowTypeId\":\"0\",\"promotivePrice\":13.8,\"remark\":\"备注\",\"specDescription\":\"口味：水蜜桃\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"11\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"xff\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"1\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"1-2-18-25\",\"id\":\"1\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/i1/TB1gbuUHXXXXXc4aXXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":200,\"code\":\"00000000\",\"currentPrice\":3.9,\"discount\":0,\"goodsInfoId\":\"1\",\"id\":\"2\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i4/158080174504251847/TB2ELtUlXXXXXXvXXXXXXXXXXXX_!!0-saturn_solar.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"2\",\"onshowTypeId\":\"2\",\"promotivePrice\":3.8,\"remark\":\"fer\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"1\",\"id\":\"18\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i4/158080174504251847/TB2ELtUlXXXXXXvXXXXXXXXXXXX_!!0-saturn_solar.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"1\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"1\",\"id\":\"25\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/i1/TB1gbuUHXXXXXc4aXXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":100,\"code\":\"000000000\",\"currentPrice\":3.9,\"discount\":0,\"goodsInfoId\":\"1\",\"id\":\"1\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/TB1hdQwJVXXXXXpXpXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"1\",\"onshowTypeId\":\"1\",\"promotivePrice\":3.8,\"remark\":\"fernny\",\"specDescription\":\"口味：红烧牛肉\",\"state\":1,\"unit\":\"桶\"}],\"merchantGoodsInfoId\":\"1\",\"name\":\"康师傅桶面\",\"onshowTypeGoodsPosition\":\"1\",\"onshowTypeId\":\"2\",\"remark\":\"fernny\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"桶\"},{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"3-4-26\",\"id\":\"2\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"2\",\"id\":\"26\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"9\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"袋\"},{\"buySum\":80,\"code\":\"00000000\",\"currentPrice\":2.9,\"discount\":0.98,\"goodsInfoId\":\"2\",\"id\":\"4\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"4\",\"onshowTypeId\":\"2\",\"promotivePrice\":2.8,\"remark\":\"\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"袋\"}],\"merchantGoodsInfoId\":\"2\",\"name\":\"康师傅袋面\",\"onshowTypeGoodsPosition\":\"2\",\"onshowTypeId\":\"2\",\"remark\":\"\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"袋\"},{\"backTypeGoodsPosition\":\"3\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"13-14-27\",\"id\":\"8\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"8\",\"id\":\"14\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"9\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"16-17\",\"id\":\"15\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"15\",\"id\":\"16\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"10\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"11-12\",\"id\":\"7\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"7\",\"id\":\"11\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"fernny\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"5\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"}]},{\"cTypeId\":\"11\",\"shopinfoList\":[{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"11\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"5-24\",\"id\":\"3\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"true\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"3\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"3\",\"id\":\"24\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":3,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"1\",\"promotivePrice\":3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"包\"}],\"merchantGoodsInfoId\":\"3\",\"name\":\"双汇王中王优级火腿肠\",\"onshowTypeGoodsPosition\":\"3\",\"onshowTypeId\":\"1\",\"remark\":\"\",\"specNames\":\"nospec\",\"typeName\":\"泡面搭档\",\"unit\":\"包\"}]}],\"msg\":\"success\"}".dataUsingEncoding(NSUTF8StringEncoding)
        do{
            let JSON = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.AllowFragments)
            var items = [Commodity]()
            var indexs = [String:Int]()
            if let goodsAndCID = JSON["GoodsAndCId"] as? NSArray{
                var sum = 0
                for object in goodsAndCID{
                    if let object = object as? [String:AnyObject] {
                        let id = object["cTypeId"] as! String
                        indexs[id] = sum
                        if let shopList = object["shopinfoList"] as? [[String:AnyObject]]{
                            for x in shopList{
                                items.append(Commodity.yy_modelWithDictionary(x)!)
                            }
                        }
                    }
                    sum = items.count
                }
            }
            Block(indexs: indexs, items: items)
        }catch _{
            
        }
    }
    
    
    class func GetDisplayItemList(Block:(titles:[String],items:[[Commodity]])->Void){
        let jsonData = "{\"GoodsInfo\":[{\"onShowId\":\"2\",\"onShowName\":\"特价促销\",\"shopinfoList\":[{\"backTypeGoodsPosition\":\"1\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"1-2-18-25\",\"id\":\"1\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/2766195587/TB2cjB_ppXXXXX4XFXXXXXXXXXX_!!2766195587.jpg_230x230.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":200,\"code\":\"00000000\",\"currentPrice\":3.9,\"discount\":0,\"goodsInfoId\":\"1\",\"id\":\"2\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/i4/TB1JrXnKXXXXXchaXXXXXXXXXXX_!!0-item_pic.jpg_220x220.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"2\",\"onshowTypeId\":\"2\",\"promotivePrice\":3.8,\"remark\":\"fer\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"桶\"},{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"1\",\"id\":\"25\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/TB1N0OnJVXXXXbyXpXXXXXXXXXX_!!0-item_pic.jpg_220x220.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"桶\"}],\"merchantGoodsInfoId\":\"1\",\"name\":\"康师傅桶面\",\"onshowTypeGoodsPosition\":\"1\",\"onshowTypeId\":\"2\",\"remark\":\"fernny\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"桶\"},{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"10\",\"brandId\":\"\",\"description\":\"\",\"goodsSpecIds\":\"3-4-26\",\"id\":\"2\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/TB1nQ3aKpXXXXaTXpXXXXXXXXXX_!!0-item_pic.jpg_220x220.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":0,\"code\":\"000000\",\"currentPrice\":3,\"discount\":1,\"goodsInfoId\":\"2\",\"id\":\"26\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/TB1QqI.KXXXXXXaXVXXXXXXXXXX.jpg_220x220.jpg\",\"initInventory\":0,\"inventory\":100,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"9\",\"onshowTypeId\":\"2\",\"promotivePrice\":3,\"remark\":\"备注\",\"specDescription\":\"口味：香菇滑鸡\",\"state\":1,\"unit\":\"袋\"},{\"buySum\":80,\"code\":\"00000000\",\"currentPrice\":2.9,\"discount\":0.98,\"goodsInfoId\":\"2\",\"id\":\"4\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/TB1nQ3aKpXXXXaTXpXXXXXXXXXX_!!0-item_pic.jpg_220x220.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":0,\"maxBuy\":\"0\",\"merchantGoodsSpecId\":\"4\",\"onshowTypeId\":\"2\",\"promotivePrice\":2.8,\"remark\":\"\",\"specDescription\":\"口味：卤香牛肉\",\"state\":1,\"unit\":\"袋\"}],\"merchantGoodsInfoId\":\"2\",\"name\":\"康师傅袋面\",\"onshowTypeGoodsPosition\":\"2\",\"onshowTypeId\":\"2\",\"remark\":\"\",\"specNames\":\"口味\",\"typeName\":\"速食面\",\"unit\":\"袋\"},{\"backTypeGoodsPosition\":\"3\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"13-14-27\",\"id\":\"8\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/TB1mWihLpXXXXcBXVXXXXXXXXXX_!!0-item_pic.jpg_220x220.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"8\",\"id\":\"14\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/TB1GQYVJVXXXXafXXXXXXXXXXXX.jpg_220x220.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"9\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"16-17\",\"id\":\"15\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"15\",\"id\":\"16\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1T6RfHFXXXXb4XVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"remark\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"10\",\"backTypeId\":\"10\",\"brandId\":\"0\",\"description\":\"描述\",\"goodsSpecIds\":\"11-12\",\"id\":\"7\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/TB13kKaKXXXXXXEXXXXXXXXXXXX.jpg_220x220.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":100,\"code\":\"000000\",\"currentPrice\":6.8,\"discount\":0.88,\"goodsInfoId\":\"7\",\"id\":\"11\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/TB1.90mKXXXXXaXXVXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1000,\"isShow\":1,\"maxBuy\":\"1000\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":6.3,\"remark\":\"fernny\",\"specDescription\":\"nospec\",\"state\":1,\"unit\":\"计量单位\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"商品名称\",\"onshowTypeGoodsPosition\":\"5\",\"onshowTypeId\":\"0\",\"remark\":\"remark\",\"specNames\":\"nospec\",\"typeName\":\"子类目名称\",\"unit\":\"计量单位\"},{\"backTypeGoodsPosition\":\"2\",\"backTypeId\":\"21\",\"brandId\":\"0\",\"description\":\"鸡尾酒RIO500ml\",\"goodsSpecIds\":\"ss001605280006-ss001605280007\",\"id\":\"20\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i4/167920227101942294/TB2rvlLpFXXXXXqXpXXXXXXXXXX_!!0-saturn_solar.jpg_230x230.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":123,\"code\":\"0000\",\"currentPrice\":2.5,\"discount\":0.9,\"goodsInfoId\":\"20\",\"id\":\"ss001605280006\",\"imageUrl\":\"https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1PAYJIVXXXXXKXXXXXXXXXXXX_!!0-item_pic.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1220,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":2.3,\"remark\":\"fernny\",\"specDescription\":\"口味：草莓\",\"state\":1,\"unit\":\"瓶\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"鸡尾酒RIO\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"备注5\",\"specNames\":\"口味\",\"typeName\":\"鸡尾酒\",\"unit\":\"瓶\"},{\"backTypeGoodsPosition\":\"4\",\"backTypeId\":\"21\",\"brandId\":\"0\",\"description\":\"鸡尾酒RIO500ml\",\"goodsSpecIds\":\"ss001605280004-ss001605280005\",\"id\":\"23\",\"imageUrl\":\"https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/42867055/TB2T6DrXpXXXXcFXpXXXXXXXXXX_!!42867055.jpg_230x230.jpg\",\"isMultiSpec\":\"\",\"mGoodsSpecs\":[{\"buySum\":123,\"code\":\"0000\",\"currentPrice\":2.5,\"discount\":0.9,\"goodsInfoId\":\"23\",\"id\":\"ss001605280004\",\"imageUrl\":\"https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i1/104550108094276063/TB23Rq_dFXXXXXCXXXXXXXXXXXX_!!43830455-0-saturn_solar.jpg_230x230.jpg\",\"initInventory\":0,\"inventory\":1220,\"isShow\":1,\"maxBuy\":\"-1\",\"merchantGoodsSpecId\":\"0\",\"onshowTypeId\":\"2\",\"promotivePrice\":2.3,\"remark\":\"fernny\",\"specDescription\":\"口味：草莓\",\"state\":1,\"unit\":\"瓶\"}],\"merchantGoodsInfoId\":\"0\",\"name\":\"鸡尾酒RIO\",\"onshowTypeGoodsPosition\":\"0\",\"onshowTypeId\":\"0\",\"remark\":\"备注5\",\"specNames\":\"口味\",\"typeName\":\"鸡尾酒\",\"unit\":\"瓶\"}]},{\"onShowId\":\"4\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"5\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"6\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"7\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"13\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"so001605230000\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"so001605240000\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]},{\"onShowId\":\"so001605240001\",\"onShowName\":\"展示类目名称\",\"shopinfoList\":[]}]}".dataUsingEncoding(NSUTF8StringEncoding)
        do {
            let JSON = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.AllowFragments)
                var titles = [String]()
                var items = [[Commodity]]()
                if let goodsInfo = JSON["GoodsInfo"] as? NSArray{
                    for x in goodsInfo {
                        if let dict = x as? [String:AnyObject]{
                            if let title = dict["onShowName"] as? String{
                                titles.append(title)
                            }
                            var tmpItems = [Commodity]()
                            if let objects = dict["shopinfoList"] as? NSArray{
                                for y in objects {
                                    if let object = Commodity.yy_modelWithDictionary(y as! [String:AnyObject]){
                                        tmpItems.append(object)
                                    }
                                }
                            }
                            items.append(tmpItems)
                        }
                    }
                }
                Block(titles: titles, items: items)
        }catch let error{
            print(error)
        }
//        let current = NSDate()
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//        XZRequest(NetDestination.GoodOnShow, parameters: ["shopId":1,"sysTime":dateFormatter.stringFromDate(current)]).XZResponse { (JSON) in
//            if let msg = JSON["msg"] as? String where msg == "success"{
//                var titles = [String]()
//                            var items = [[Commodity]]()
//                            if let goodsInfo = JSON["GoodsInfo"] as? NSArray{
//                                for x in goodsInfo {
//                                    if let dict = x as? [String:AnyObject]{
//                                        if let title = dict["onShowName"] as? String{
//                                            titles.append(title)
//                                        }
//                                        var tmpItems = [Commodity]()
//                                        if let objects = dict["shopinfoList"] as? NSArray{
//                                            for y in objects {
//                                                if let object = Commodity.yy_modelWithDictionary(y as! [String:AnyObject]){
//                                                    tmpItems.append(object)
//                                                }
//                                            }
//                                        }
//                                        items.append(tmpItems)
//                                    }
//                                }
//                            }
//                            Block(titles: titles, items: items)
//                    }
//        }

    }
    
    class func GetRecAddress(block:([RecAddress])->Void){
        XZRequestID(.RecAddressList, parameters: ["telephone":"15159796933"]).XZResponse { (JSON) in
            if JSON["message"] as? String != "failure"{
                var addresses = [RecAddress]()
                if let array = JSON["addressDetailList"] as? NSArray{
                    for x in array {
                        let dict = x as! [String:AnyObject]
                        addresses.append(RecAddress.yy_modelWithDictionary(dict)!)
                    }
                    block(addresses)
                }
            }
         }
    }
    
    class func AddNewRecAddress(rec:RecAddress,Block:(Bool)->Void){
        
        XZRequestID(.AddRecAddress, parameters: ["telephone":"15159796933","detailAddress":rec.locAddress + rec.detailAddress,"name":rec.receiverName,"position":String(format: "%.12f-%12f", rec.coordinate.coordinate.latitude,rec.coordinate.coordinate.longitude),"receiverSex":rec.receiverSex == true ? "男" : "女","remark":rec.remark == nil ? "" : rec.remark!]).XZResponse { (JSON) in
            print(JSON)
        }
    }
}

func XZRequest(URLString: NetDestination, parameters: [String : AnyObject]?) -> Request{
    
    return Alamofire.request(.POST, URLString.url, parameters: parameters, encoding: .JSON, headers: nil)
}
func XZRequestID(URLString: NetDestination, parameters: [String : AnyObject]?) -> Request{
    //将ID加入
    return Alamofire.request(.POST, URLString.url, parameters: parameters, encoding: .JSON, headers: nil)
}

extension Request{
    
    func XZResponse(Block:(JSON:[String:AnyObject])->Void) -> Self{
        return responseJSON(completionHandler: { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? [String:AnyObject]{
                    Block(JSON:json)
                }
            }else{
                 Block(JSON:["message":"failure"])
            }
        })
    }
}