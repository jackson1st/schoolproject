//
//  ViewController.swift
//  学子超市
//
//  Created by jason on 3/13/16.
//  Copyright © 2016 jason. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SnapKit
import RealmSwift
import YYModel
class ViewController: ShopBaseViewController,MAMapViewDelegate {
    var mapView:MAMapView!
    
    //活动
    var ButtonActiveOne: UIButton!
    var ButtonActiveTwo: UIButton!
    var ButtonActiveThd: UIButton!
    var ButtonActiveFour: UIButton!
    var ViewForActive: UIView!
    func initActive(){
        //网络请求获取活动,或者固有活动
        //先静态处理
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        self.collectionView.addSubview(view)
        view.frame = CGRect(x: 0, y: -80, width: ScreenWidth, height: 80)
        ButtonActiveOne = UIButton()
        ButtonActiveOne.setTitle("领红包", forState: .Normal)
        ButtonActiveOne.setImage(UIImage(named: "Active_RedBag"), forState: .Normal)
        ButtonActiveOne.setTitleColor(UIColor.blackColor(), forState: .Normal)
        ButtonActiveOne.verticalImgAndTitle(5)
        ButtonActiveOne.tag = 101
        ButtonActiveOne.addTarget(self, action: #selector(self.buttonActiveClicked(_:)), forControlEvents: .TouchUpInside)
        ButtonActiveTwo = UIButton()
        ButtonActiveTwo.setTitle("秒杀", forState: .Normal)
        ButtonActiveTwo.setImage(UIImage(named: "Active_ms.png"), forState: .Normal)
        ButtonActiveTwo.setTitleColor(UIColor.blackColor(), forState: .Normal)
        ButtonActiveTwo.verticalImgAndTitle(5)
        ButtonActiveThd = UIButton()
        ButtonActiveThd.setTitle("特价", forState: .Normal)
        ButtonActiveThd.setTitleColor(UIColor.blackColor(), forState: .Normal)
        ButtonActiveThd.setImage(UIImage(named: "Active_specialPrice.png"), forState: .Normal)
        ButtonActiveThd.verticalImgAndTitle(5)
        ButtonActiveFour = UIButton()
        ButtonActiveFour.setTitle("其他", forState: .Normal)
        ButtonActiveFour.setTitleColor(UIColor.blackColor(), forState: .Normal)
        ButtonActiveFour.setImage(UIImage(named: "Active_Other.png"), forState: .Normal)
        ButtonActiveFour.verticalImgAndTitle(5)
        view.addSubview(ButtonActiveOne)
        view.addSubview(ButtonActiveTwo)
        view.addSubview(ButtonActiveThd)
        view.addSubview(ButtonActiveFour)
        let width = ScreenWidth/4
        ButtonActiveOne.frame = CGRect(x: 0, y: 0, width: width, height: 80)
        ButtonActiveTwo.frame = CGRect(x: width, y: 0, width: width, height: 80)
        ButtonActiveThd.frame = CGRect(x: 2 * width, y: 0, width: width, height: 80)
        ButtonActiveFour.frame = CGRect(x: 3 * width, y: 0, width: width, height: 80)
    }
    
    func buttonActiveClicked(sender:UIButton){
        if(sender.tag == 101){
            let vc = RedBagGetViewController()
            self.presentViewController(XZNavigationController(rootViewController: vc), animated: true, completion: nil)
        }
    }
    
    lazy var barItemSearch:UIBarButtonItem = {
        return  UIBarButtonItem(image: UIImage(named:"Search"), style: .Plain, target: self, action: #selector(ViewController.pushSearchVC))
    }()
    lazy var searchVC: SearchViewController = {
        let vc = SearchViewController()
        return vc
    }()
    //如果不放在view里面，那么图标将会消失
    lazy var buttonStoreChoose:UIView = {
        let view = UIView()
        let btn = UIButton()
        btn.setImage(UIImage(named: "IconLocation"), forState: .Normal)
        btn.setTitle("点击定位", forState: .Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.setTitleColor(UIColor.colorWithRGB(168, g: 140, b: 73), forState: .Highlighted)
        btn.addTarget(self, action: #selector(self.chooseStore), forControlEvents: .TouchUpInside)
        btn.sizeToFit()
        view.addSubview(btn)
        btn.frame.origin = CGPoint(x: 0, y: 0)
        view.frame.size = btn.frame.size
        return view
    }()
    
    static var storeChooseViewController:UINavigationController = {
        let vc = StoreChooseViewController()
        vc.StoreChangeAction.append({
            print("主界面更新")
        })
        return  UINavigationController(rootViewController:vc)
    }()
    
    func chooseStore(){
        self.presentViewController(ViewController.storeChooseViewController, animated: true, completion: nil)
    }
    
    func pushSearchVC(){
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    var photoScan:PhotoScanViewController!
    lazy var goodDetailViewController:GoodDetailViewController! = {
        return GoodDetailViewController()
    }()
    var collectionView:UICollectionView!
    var headerTitles = [String]()
    var displayGoods = [[Commodity]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        initPhotoScan()
        initMapView()
        self.navigationItem.titleView = buttonStoreChoose
        self.navigationItem.rightBarButtonItem = barItemSearch
        initActive()
//        setNeedsStatusBarAppearanceUpdate()
        askDisplayItems()
    }
    
    func askDisplayItems(){
        NetWorkManager.GetDisplayItemList {[weak self] titles, items in
            if let sSelf = self{
                sSelf.headerTitles = titles
                sSelf.displayGoods = items
                sSelf.collectionView.reloadData()
            }
        }
    }
    
    override func tabBarOK() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        self.restorationIdentifier = "Home"
        super.viewDidAppear(animated)
    }
    

    
    func initCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 8
        layout.headerReferenceSize = CGSizeMake(0, 25)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: self.view.frame.height), collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.ViewMBKColor()
        collectionView.contentInset = UIEdgeInsets(top: 218, left: 0, bottom: 0, right: 0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bounces = false
        collectionView.registerNib(UINib(nibName: "GoodVCollectionViewCell",bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cellH")
        collectionView.registerClass(HomeCollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        self.contentView.addSubview(collectionView)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.frame.size.height = self.contentView.frame.height
    }
    
    func initPhotoScan(){
        photoScan = PhotoScanViewController()
        photoScan.PhotoURLs = ["http://m.360buyimg.com/mobilecms/s720x350_jfs/t2503/321/2926477381/92972/3c7fd254/56f4ddfaNa94fb637.jpg","http://m.360buyimg.com/mobilecms/s720x350_jfs/t2503/321/2926477381/92972/3c7fd254/56f4ddfaNa94fb637.jpg","http://m.360buyimg.com/mobilecms/s720x350_jfs/t2503/321/2926477381/92972/3c7fd254/56f4ddfaNa94fb637.jpg","http://m.360buyimg.com/mobilecms/s720x350_jfs/t2503/321/2926477381/92972/3c7fd254/56f4ddfaNa94fb637.jpg"]
        self.addChildViewController(photoScan)
        self.collectionView.addSubview(photoScan.view)
        photoScan.view.frame.origin = CGPoint(x: 0,y: -280)
    }
    
    func initMapView(){
        mapView = MAMapView()
        mapView.delegate = self
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.Denied && CLLocationManager.locationServicesEnabled()){
            mapView.showsUserLocation = true
        }
    }
    
    //接收购物车数量变化的通知，以此来更新当前页面的购物车数据模型
    override func GoodNumChange(not: NSNotification) {
        let info = not.userInfo as! [String:AnyObject]
        let ID = info["goodID"] as! String
        let num = info["num"] as! Int
        if let btn = info["view"] as? UIButton{
            self.addAitem(btn.convertRect(btn.bounds, toView: self.view).origin)
        }
        for i in 0..<displayGoods.count{
            for j in 0..<displayGoods[i].count{
                if(displayGoods[i][j].id == ID && displayGoods[i][j].isMultiSpec == false){
                    displayGoods[i][j].buied = num
                    collectionView.reloadItemsAtIndexPaths([NSIndexPath(forRow: j, inSection: i)])
                    break
                }else{
                    displayGoods[i][j].buied = 0
                }
            }
        }
    }
    
    //清空购物车
    override func ShopCartClear() {
        for x in displayGoods{
            for y in x{
                y.buied = 0
            }
        }
        collectionView.reloadData()
    }
    //后期完善
    func mapView(mapView: MAMapView!, didUpdateUserLocation userLocation: MAUserLocation!) {
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.setDouble(userLocation.location.coordinate.latitude, forKey: "latitude")
        userDefault.setDouble(userLocation.location.coordinate.longitude, forKey: "longitude")
        userDefault.synchronize()
        mapView.showsUserLocation = false
    }

}

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((ScreenWidth - 20) / 2 - 4, 230)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        goodDetailViewController.object = displayGoods[indexPath.section][indexPath.row]
        self.navigationController?.pushViewController(goodDetailViewController, animated: true)
    }
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return headerTitles.count
    }
    
     func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let headView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) as! HomeCollectionHeaderView
        headView.setTitle(headerTitles[indexPath.row])
        return headView
    }
    
    
    
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return displayGoods[section].count
    }
    
     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellH", forIndexPath: indexPath) as! GoodVCollectionViewCell
        cell.object = displayGoods[indexPath.section][indexPath.row]
        return cell
    }
    

}

