//
//  SuperMarketModel.swift
//  学子超市
//
//  Created by Jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import YYModel
import RealmSwift
class Commodity:NSObject,YYModel{
    var id:String!
    var name:String!
    var backTypeGoodsPosition:String!//后台类目位置
    var backTypeId:String!//后台类目id
    var brandId:String!//品牌id
    var newDescription:String!//商品描述
    var goodsSpecIds:String!//子规格id串
    var imageUrl:String!
    var isMultiSpec:Bool = false
    var mGoodsSpecs:NSArray!
    var merchantGoodsInfoId:String!//商品基础商品id
    var onshowTypeGoodsPosition:String!//展示类目位置
    var onshowTypeId:String!//展示类目id
    var remark:String!
    var specNames:String!//规格描述
    var typeName:String!//类目名称
    var unit:String!
    
    var buied:Int = 0
    var sizeNames = [(title:String,detail:[String])]()
    
    static func modelContainerPropertyGenericClass() -> [String : AnyObject]?{
        return ["mGoodsSpecs":GoodsSpecs.self]
    }
    
    static func modelCustomPropertyMapper() -> [String : AnyObject]?{
        return ["newDescription":"description"]
    }

    static func modelPropertyBlacklist() -> [String]?{
        return ["buied","sizeNames"]
    }
    
    func modelCustomTransformFromDictionary(dic: [NSObject : AnyObject]) -> Bool{
        self.buied = 0
        var dict = [String:Set<String>]()
        for object in self.mGoodsSpecs{
            if let object = object as? GoodsSpecs{
                let sizes = object.specDescription.componentsSeparatedByString("-")//得到规格名:规格
                for strs in sizes{
                    let newStr = strs.componentsSeparatedByString("：")
                    if newStr.count >= 2 {
                        if let _ = dict[newStr[0]]{
                           dict[newStr[0]]!.insert(newStr[1])
                        }else{
                            dict[newStr[0]] = Set<String>()
                            dict[newStr[0]]!.insert(newStr[1])
                        }
                    }
                    
                }
            }
        }
        sizeNames = dict.map({(title:$0.0,detail:Array($0.1))})
        return true
    }
    
}
class GoodsSpecs:NSObject,YYModel{
    var id:String!
    var buySum:Int = 0//销量
    var code:String!
    var currentPrice:Double = 0.0
    var discount:Double = 0.0
    var goodsInfoId:String!//商品基础ID
    var imageUrl:String!
    var initInventory:Int = 0//初始库存
    var inventory:Int = 0//库存
    var maxBuy:Int = 0//限购，-1表示不限购
    var merchantGoodsSpecId:String!//商家规格id
    var onshowTypeId:String!
    var promotivePrice:Double = 0.0
    var remark:String!
    var specDescription:String!//具体规格描述 规格名:规格详情
    var state:Int = 0
    var unit:String!
    
    var promotivePriceStr:NSMutableAttributedString{
        get{
            let str = "¥" + String(format: "%.1f", promotivePrice)
            let attStr = NSMutableAttributedString(string: str)
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(0, 1))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(16),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(1, str.characters.count - 1))
            return attStr
        }
    }
    var currentPriceStr:NSMutableAttributedString{
        get{
            let str = "¥" + String(format: "%.1f", currentPrice)
            let attStr = NSMutableAttributedString(string: str)
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(10),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(0, 1))
            attStr.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(12),NSForegroundColorAttributeName:UIColor.redColor()], range: NSMakeRange(1, str.characters.count - 1))
            attStr.addAttributes([NSStrikethroughStyleAttributeName:1], range: NSMakeRange(0, str.characters.count))
            attStr.addAttribute( NSStrikethroughColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, str.characters.count))
            attStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, str.characters.count))
            return attStr
        }
    }
}

class ShopCartGood:Object{
    dynamic var id = ""
    dynamic var goodID = ""
    dynamic var specID = ""
    dynamic var name = ""
    dynamic var num = 0
    dynamic var specDescription = ""
    dynamic var currentPrice = 0.0
    var displayName:String!{
        get{
            return self.name + "(" + self.specDescription + ")"
        }
    }
    var displayNum:String!{
        get{
            return "x\(num)"
        }
    }
    var displayPrice:String!{
        get{
            if (currentPrice * 10)%10 != 0 {
                return String(format: "¥%.1f", currentPrice)
            }
            return String(format: "¥%.f", currentPrice)
        }
    }
    convenience init(good:Commodity,specsID:String,specDescription:String,price:Double){
        self.init()
        self.id = NSUUID().UUIDString
        self.goodID = good.id
        self.name = good.name
        self.num = 1
        self.specID = specsID
        self.specDescription = specDescription
        self.currentPrice = price
    }
    
     func oneCopy() -> ShopCartGood {
        let objectCopied = ShopCartGood()
        objectCopied.id = self.id
        objectCopied.goodID = self.goodID
        objectCopied.specID = self.specID
        objectCopied.name = self.name
        objectCopied.num = self.num
        objectCopied.currentPrice = self.currentPrice
        objectCopied.specDescription = self.specDescription
        return objectCopied
    }
    
    class func getAll() -> Results<ShopCartGood>{
        let realm = try! Realm()
        return realm.objects(ShopCartGood)
    }
    
    class func addOne(good:ShopCartGood){
        if let result = getAll().filter("self.goodID = %@ and self.specID = %@",good.goodID,good.specID).first{
            let realm = try! Realm()
            try! realm.write({
                result.num += 1
            })
        }else{
            let realm = try! Realm()
           try! realm.write({
                realm.add(good)
            })
        }
    }
    
    class func decOne(good:ShopCartGood){
        if let result = getAll().filter("self.goodID = %@ and self.specID = %@",good.goodID,good.specID).first{
            let realm = try! Realm()
            try! realm.write({
                result.num -= 1
                if result.num == 0{
                    realm.delete(result)
                }
            })
        }
    }
    
    
}

class CommodityClass:NSObject,YYModel{
    var id:String!
    var name:String!
    var subClass:NSArray!
    static func modelContainerPropertyGenericClass() -> [String : AnyObject]? {
        return ["subClass":CommodityClass.self]
    }
    
    static func modelCustomPropertyMapper() -> [String : AnyObject]?{
        return ["subClass":"childrenList"]
    }
    
    func modelCustomTransformFromDictionary(dic: [NSObject : AnyObject]) -> Bool{
        print(dic)
        return true
    }
}
