//
//  JSItemListResultTableViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class JSItemListResultTableViewController: UITableViewController,UISearchResultsUpdating {

    
    var filterString:String!{
        didSet{
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell?.textLabel?.text = "123"
        return cell!
    }

    func updateSearchResultsForSearchController(searchController: UISearchController){
        guard searchController.active else {return}
        filterString = searchController.searchBar.text
        
    }
}
