//
//  JSNavigationController.swift
//  办公管理系统
//
//  Created by jason on 16/5/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class JSNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont(name: LightLanTingHeiFontName, size: 20)!,NSForegroundColorAttributeName:UIColor.MidBlackColor() ]
        self.navigationBar.tintColor = UIColor.MainColor()
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        viewController.hidesBottomBarWhenPushed = true
        super.pushViewController(viewController, animated: animated)
    }

}
