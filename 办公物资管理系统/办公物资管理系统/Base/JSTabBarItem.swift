//
//  JSTabBarItem.swift
//  办公物资管理系统
//
//  Created by jason on 16/5/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class JSTabBarItem: UITabBarItem {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.image = self.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.selectedImage = self.image
        self.imageInsets.top = 5
        self.imageInsets.bottom = -5
    }
}
