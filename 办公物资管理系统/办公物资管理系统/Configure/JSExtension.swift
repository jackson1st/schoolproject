//
//  JSExtension.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIButton{
    
    func verticalImgAndTitle(space:CGFloat){
        sizeToFit()
        let imgSize = self.imageView!.frame.size
        var titleSize = self.titleLabel!.frame.size
        let textSize = self.titleLabel?.text!.sizeWithAttributes([NSFontAttributeName:titleLabel!.font])
        let frameSize = CGSize(width: ceil(textSize!.width), height: ceil(textSize!.height))
        if(titleSize.width + 0.5 < frameSize.width){
            titleSize.width = frameSize.width
        }
        let totalHeight = imgSize.height + titleSize.height + space
        self.imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight-imgSize.height), 0, 0,-titleSize.width)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -imgSize.width , -(totalHeight - titleSize.height), 0)
    }
    
}

protocol Reusable: class {
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
}

extension Reusable {
    static var reuseIdentifier: String { return String(Self) }
    static var nib: UINib? { return UINib(nibName: String(Self), bundle: NSBundle.mainBundle())}
}


extension MBProgressHUD{
    
    class func show(text:String,icon:String,  view:UIView?){
        var toView = view
        if toView == nil{
            toView = UIApplication.sharedApplication().windows.last!
        }
        let hud = MBProgressHUD.showHUDAddedTo(toView, animated: true)
        hud.labelText = text
        hud.customView = UIImageView(image: UIImage(named: "MBProgressHUD.bundle/\(icon)"))
        hud.mode = .CustomView
        hud.removeFromSuperViewOnHide = true
        hud.hide(true, afterDelay: 0.7)
    }
    
    class func showSuccess(success:String, toView:UIView?){
        self.show(success, icon: "success.png", view: toView)
    }
    
    class func showError(error:String, toView:UIView?){
        self.show(error, icon: "error.png", view: toView)
    }
    
    class func showMessage(msg:String?,view:UIView?) -> MBProgressHUD{
        var toView = view
        if toView == nil{
            toView = UIApplication.sharedApplication().windows.last!
        }
        let hud = MBProgressHUD.showHUDAddedTo(toView, animated: true)
        hud.removeFromSuperViewOnHide = true
        hud.dimBackground = false
        return hud
    }
}



extension UITableView {
    
    func clearOtherLine(){
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        self.tableFooterView = view
    }
    
    func registerReusableCell<T: UITableViewCell where T: Reusable>(_: T.Type) {
        if let nib = T.nib {
            self.registerNib(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.registerClass(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    func registerReusableCellInClass<T: UITableViewCell where T: Reusable>(_: T.Type) {
        
        self.registerClass(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    
    func dequeueReusableCell<T: UITableViewCell where T: Reusable>(indexPath indexPath: NSIndexPath) -> T {
        return self.dequeueReusableCellWithIdentifier(T.reuseIdentifier, forIndexPath: indexPath) as! T
    }
    
    func registerReusableHeaderFooterView<T: UITableViewHeaderFooterView where T: Reusable>(_: T.Type) {
        if let nib = T.nib {
            self.registerNib(nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        } else {
            self.registerClass(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView where T: Reusable>() -> T? {
        return self.dequeueReusableHeaderFooterViewWithIdentifier(T.reuseIdentifier) as! T?
    }
}