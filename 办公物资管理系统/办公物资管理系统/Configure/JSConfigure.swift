//
//  JSConfigure.swift
//  办公管理系统
//
//  Created by jason on 16/5/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
let ScreenHeight = UIScreen.mainScreen().bounds.height
let ScreenWidth = UIScreen.mainScreen().bounds.width
let LightLanTingHeiFontName = "FZLanTingHei-L-GBK-M"
let NormalLanTingHeiFontName = "FZLanTingHei-R-GBK"
extension UIColor{
    convenience init(R:CGFloat,G:CGFloat,B:CGFloat, alpha: CGFloat){
        self.init(red: R/255.0, green: G/255.0, blue: B/255.0, alpha: alpha)
    }
    convenience init(RGB:Int,alpha:CGFloat){
        let r = CGFloat((RGB & 0xFF0000)>>16)
        let g = CGFloat((RGB & 0xFF00)>>8)
        let b =  CGFloat((RGB & 0xFF))
        self.init(R:r,G:g,B:b,alpha: alpha)
    }
    
    class func BackgroundColor() -> UIColor{
        return UIColor(RGB: 0xf2f2f2, alpha: 1)
    }

    //---------三个级别的黑色字体---------
    //高级黑
    class func HightBlackColor() -> UIColor{
        return UIColor(RGB: 0x333333,alpha: 1)
    }
    //中级黑
    class func MidBlackColor() -> UIColor{
        return UIColor(RGB: 0x666666, alpha: 1)
    }
    //低级黑
    class func LowBlackColor() -> UIColor{
        return UIColor(RGB: 0x999999, alpha: 1)
    }
    //文本黑
    class func TxtBlackColor() -> UIColor{
        return UIColor(RGB: 0x4a4a4a, alpha: 1)
    }
    
    class func MainColor() -> UIColor{
        //蓝色
        return UIColor(RGB: 0x0076FF,alpha: 1)
    }

    
    //---------三种级别颜色---------
    //危险级别
    class func DangerColor() -> UIColor{
        return UIColor(RGB: 0xFF6666, alpha: 1)
    }
    //警告级别
    class func WarnColor() -> UIColor{
        return UIColor(RGB: 0xffcc33, alpha: 1)
    }
    //安全级别
    class func SafeColor() -> UIColor{
        return UIColor(RGB: 0x99cc33, alpha: 1)
    }
}
