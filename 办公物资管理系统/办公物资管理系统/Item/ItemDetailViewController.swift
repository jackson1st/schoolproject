//
//  ItemDetailViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/6.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import KeyboardMan
class ItemDetailViewController: JSViewController {

    @IBOutlet weak var ConstraintTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var item:Item!
    var identityCode:String?
    var model = 0{//0表示查看模式，1表示编辑模式
        didSet{
            if tableView != nil {
                self.tableView.reloadData()
            }
        }
    }
    var keyboardMan = KeyboardMan()
    
    var ActionSave:((Item)->Void)!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "物品详情"
        tableViewConfigure()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "编辑", style: .Plain, target: self, action: #selector(self.EditButtonClicked))
        self.keyboardMan.animateWhenKeyboardAppear = {
            appearPostIndex, keyboardHeight, keyboardHeightIncrement in
            self.ConstraintTableViewBottom.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
        self.keyboardMan.animateWhenKeyboardDisappear = {
            keyboardHeight in
            self.ConstraintTableViewBottom.constant = 0
        }
    }
    
    func tableViewConfigure(){
        tableView.registerReusableCell(ItemDetailTableViewCell)
    }
    
    func EditButtonClicked(){
        if model != 0 {
            model = 0
            self.navigationItem.rightBarButtonItem?.title = "编辑"
        }else{
            model = 1
            self.navigationItem.rightBarButtonItem?.title = "完成"
        }
    }
}

extension ItemDetailViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if model == 1 {
            return 14
        }
        return item == nil ? 0 : 14
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ItemDetailTableViewCell
        cell.LabelTitle.textColor = UIColor(RGB: 0x999999, alpha: 1)
        cell.TextFieldContent.enabled = false
        switch indexPath.row {
        case 0:
            cell.LabelTitle.text = "物品编号"
            if item.id != nil{
                cell.TextFieldContent.text = item?.id
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "由系统自动生成"
            }
        case 1:
            cell.LabelTitle.text = "物品分类"
            if item.id != nil {
                cell.TextFieldContent.text = item?.class_name
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "点击选择分类"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
            }
        case 2:
            cell.LabelTitle.text = "物品名称"
            if item.id != nil {
                cell.TextFieldContent.text = item?.name
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品名称"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 3:
            cell.LabelTitle.text = "物品规格"
            if item.id != nil {
                cell.TextFieldContent.text = item?.specification
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品规格"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 4:
            cell.LabelTitle.text = "识别码"
            cell.TextFieldContent.text = identityCode ?? (item?.identifyCode)!
        case 5:
            cell.LabelTitle.text = "物品型号"
            if item.id != nil {
                cell.TextFieldContent.text = item?.model
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品型号"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 6:
            cell.LabelTitle.text = "物品单位"
            if item.id != nil {
                cell.TextFieldContent.text = item?.unit
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品单位"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 7:
            cell.LabelTitle.text = "物品单价"
            if item.id != nil {
                cell.TextFieldContent.text = String(format: "%.1f元",(item?.unit_price)!)
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品单价"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 8:
            cell.LabelTitle.text = "库存数量"
            if item.id != nil {
                cell.TextFieldContent.text = String(format: "%d",(item?.stock_quantity)!) + (item?.unit)!
                
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入库存数量"
            }
        case 9:
            cell.LabelTitle.text = "库存金额"
            if item.id != nil {
                cell.TextFieldContent.text = String(format: "%d元",(item?.stock_price)!)
            }else{
                cell.TextFieldContent.text = "0元"
            }
        case 10:
            cell.LabelTitle.text = "上限数量"
            if item.id != nil {
                cell.TextFieldContent.text = String(format: "%d",(item?.max_number)!) + (item?.unit)!
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品上限数量"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 11:
            cell.LabelTitle.text = "下限数量"
            if item.id != nil {
                cell.TextFieldContent.text = String(format: "%d",(item?.min_number)!) + (item?.unit)!
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "输入物品下限数量"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        case 12:
            cell.LabelTitle.text = "供应商"
            if item.id != nil {
                cell.TextFieldContent.text = item?.supplier_name
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "点击选择供应商"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
            }
        case 13:
            cell.LabelTitle.text = "备注"
            if item.id != nil {
                cell.TextFieldContent.text = item?.remarks
            }else{
                cell.TextFieldContent.text = nil
                cell.TextFieldContent.placeholder = "点击输入备注"
            }
            if model == 1{
                cell.LabelTitle.textColor = UIColor(RGB: 0x0076FF, alpha: 1)
                cell.TextFieldContent.enabled = true
            }
        default:
            break
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}