//
//  ItemTableViewCell.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var LabelSupply: UILabel!
    @IBOutlet weak var LabelSizeName: UILabel!
    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelPrice: UILabel!
    @IBOutlet weak var LabelStore: UILabel!
    var item:Item!{
        didSet{
            LabelName.text = item.name
            LabelSizeName.text = item.specification
            LabelSupply.text = item.supplier_name
            LabelPrice.text = "¥ " + String(format: "%.1f",item.stock_price)
            LabelStore.text = "库存\(item.stock_quantity)件"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
