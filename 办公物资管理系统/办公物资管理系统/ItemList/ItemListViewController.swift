//
//  ItemListViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import MJRefresh
//按分类ID查找物品
class ItemListViewController: JSItemListResultTableViewController {

    override var filterString:String?{
        didSet{
            if filterString == "" || filterString == nil {
                self.visibleItems = items
            }else{
                self.visibleItems = items.filter{return $0.name.containsString(filterString!)}
                self.tableView.reloadData()
            }
        }
    }
    
    var items = [Item](){
        didSet{
            self.visibleItems = items
        }
    }
    var classID = ""
    var visibleItems = [Item]()

    var searchController:UISearchController!
    lazy var itemDetailViewController:ItemDetailViewController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        return story.instantiateViewControllerWithIdentifier("ItemDetailViewController") as! ItemDetailViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        naviagtionItemConfigure()
        searchControllerConfigure()
        tableViewConfigure()
        GetData()
    }
    
    func naviagtionItemConfigure(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新增", style: .Plain, target: self, action: #selector(self.AddNewItem))
    }
    
    func AddNewItem(){
        self.itemDetailViewController.item = self.items[0]
        self.itemDetailViewController.model = 1
        
        self.itemDetailViewController.ActionSave = {[weak self]
            item in
           //直接向服务器请求创建
        }
    }
    
    func GetData(){
        NetWorkManager.getItemList("1") { (items) in
            self.items = items
            self.tableView.reloadData()
        }
    }
    
    func tableViewConfigure(){
        tableView.registerReusableCell(ItemTableViewCell)
        tableView.layoutMargins = UIEdgeInsetsZero
        tableView.separatorInset = UIEdgeInsetsZero
    }

    func searchControllerConfigure(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ItemTableViewCell
        cell.item = visibleItems[indexPath.row]
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsetsZero
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.itemDetailViewController.item = self.items[indexPath.row]
        self.itemDetailViewController.model = 0
        self.itemDetailViewController.ActionSave = {
            item in
            
        }
        self.navigationController?.pushViewController(self.itemDetailViewController, animated: true)
        
    }
}
