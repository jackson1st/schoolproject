//
//  Item.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import RealmSwift
public class Item:Object{
    dynamic var name:String!
    dynamic var id:String!
    dynamic var class_name:String = ""
    dynamic var specification:String = ""
    dynamic var model:String = ""
    dynamic var unit:String = ""
    dynamic var unit_price:Double = 0.0
    dynamic var stock_quantity:Int = 0
    dynamic var stock_price:Double = 0.0
    dynamic var max_number:Int = 0
    dynamic var min_number:Int = 0
    dynamic var warn:Int = 0
    dynamic var supplier_id:String = ""
    dynamic var supplier_name:String = ""
    dynamic var remarks:String = ""
    dynamic var identifyCode:String = ""
    dynamic var newCreate = false //用来表示该物品在最后阶段是否要向服务器请求创建
    convenience init(dict:[String:AnyObject]){
        self.init()
        dict.forEach { (tmp) in
            self.setValue(tmp.1, forKey: tmp.0)
        }
    }
}

class People:NSObject{
    var name:String!
    var id:String!
    var departName:String!
}


//型号、识别码（定）、单位、单价、库存数量（定），库存金额（定）、上限数量、下限数量、供应商名称、备注