//
//  SearchItemViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/10.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class SearchItemViewController: JSItemListResultTableViewController {

    var model = 0//0:按名字查找 1：按识别码来查找
    override var filterString:String?{
        didSet{
            //发起网络请求
            if filterString == "" || filterString == nil {
                
                self.visibleItems = items
                
            }else{
                
                self.visibleItems = items.filter{return $0.name.containsString(filterString!)}
                
            }
            if self.tableView != nil{
                self.tableView.reloadData()
            }
        }
    }
    var items = [Item](){
        didSet{
            self.visibleItems = items
        }
    }
    var visibleItems = [Item]()
    var otherItem:Item?//多创建出来的Item
    var selectedAction:((Item) -> Void)!
    lazy var itemDetailViewController:ItemDetailViewController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        return story.instantiateViewControllerWithIdentifier("ItemDetailViewController") as! ItemDetailViewController
    }()
    var searchController:UISearchController!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchControllerConfigure()
        GetData()
        tableViewConfigure()
        naviagtionItemConfigure()
    }
    
    func naviagtionItemConfigure(){
        if model == 1{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新增", style: .Plain, target: self, action: #selector(self.AddNewItem))
        }
    }
    
    func searchControllerConfigure(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false
        
    }
    
    func tableViewConfigure(){
        tableView.registerReusableCell(ItemTableViewCell)
        tableView.layoutMargins = UIEdgeInsetsZero
        tableView.separatorInset = UIEdgeInsetsZero
    }
    
    func AddNewItem(){
        self.itemDetailViewController.item = self.items[0]
        self.itemDetailViewController.model = 1
        self.itemDetailViewController.ActionSave = {[weak self]
            item in
            if let sSelf = self {
                sSelf.dismissViewControllerAnimated(true, completion: {
                    sSelf.selectedAction(item)
                })
            }
        }
    }
    
    func GetData(){
        if model == 1{
            searchController.searchBar.hidden = true
            NetWorkManager.getItemList("1") { (items) in
                self.items = items
                if self.otherItem != nil {
                    self.visibleItems.append(self.otherItem!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ItemTableViewCell
        cell.item = visibleItems[indexPath.row]
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsetsZero
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 80
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.dismissViewControllerAnimated(true, completion: { [weak self] in
            if let sSelf = self{
                sSelf.selectedAction(sSelf.items[indexPath.row])
            }
        })
        
    }


}
