//
//  IOHeaderViewPeople.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/7.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class IOHeaderViewPeople: UIView {

    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelIn: UILabel!
    @IBOutlet weak var LabelOut: UILabel!
    var selectedAction:(()->Void)?
    
    func fillContent(name:String,inData:Int,outData:Int,selectedAction:()->Void){
        LabelName.text = name
        if inData != 0{
            LabelIn.text = String(format: "借入: %d",inData)
        }else{
            LabelIn.text = nil
        }
        if inData != 0{
            LabelOut.text = String(format: " 借出: %d",outData)
        }else{
            LabelOut.text = nil
        }
        self.selectedAction = selectedAction
    }
    @IBAction func ButtonSelectedClicked(sender: AnyObject) {
        if let action = selectedAction{
            action()
        }
    }
    
    class func initFromxib() -> IOHeaderViewPeople{
        return NSBundle.mainBundle().loadNibNamed("IOHeaderViewPeople", owner: nil, options: nil)[0] as! IOHeaderViewPeople
    }
}
