//
//  IODetailFilterViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/7.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import JTCalendar
class IODetailFilterViewController: JSViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var CalenderMenuView: JTCalendarMenuView!
    @IBOutlet weak var CalenderContentView: JTHorizontalCalendarView!
    @IBOutlet weak var LabelChooseDateStart: UILabel!
    @IBOutlet weak var LabelChooseDateEnd: UILabel!
    @IBOutlet weak var ButtonDateStart: UIButton!
    @IBOutlet weak var ButtonDateEnd: UIButton!
    let calendarManager = JTCalendarManager()
    var filterContent:(one:Int,two:Int,dateStart:NSDate,dateEnd:NSDate)!{
        didSet{
            if tableView != nil {
                tableView.reloadData()
            }
        }
    }
    var selectedTypeCell:UITableViewCell?
    var saveAction:((value:(one:Int,two:Int,dateStart:NSDate,dateEnd:NSDate)!)->Void)!
    var lastBtn:UIButton!
    var currentDateStyle = "开始时间"
    var titles = [["按商品汇总","按部门汇总","按人员汇总"],["入库记录","出库记录"]]
    var headTitles = ["汇总方式","出库&入库","日期选择"]
    lazy var dateFormatter:NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    var dateStart:NSDate = NSDate(){
        didSet{
            LabelChooseDateStart.text = "开始: " + dateFormatter.stringFromDate(dateStart)
        }
    }
    var dateEnd:NSDate!{
        didSet{
            LabelChooseDateEnd.text = "结束: " + dateFormatter.stringFromDate(dateEnd)
        }
    }
    var currentDate:NSDate{
        get{
            if currentDateStyle == "开始时间"{
                return dateStart
            }else{
                return dateEnd
            }
        }
        set{
            if currentDateStyle == "开始时间"{
                 dateStart = newValue
                 filterContent.2 = newValue
            }else{
                 dateEnd = newValue
                 filterContent.3 = newValue
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarConfigure()
        navigationItemButtonConfigure()
        lastBtn = ButtonDateStart
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.dateEnd = self.filterContent.3
        self.dateStart = self.filterContent.2
        self.calendarManager.setDate(currentDate)
    }
    
    func navigationItemButtonConfigure(){
        self.title = "筛选"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: .Plain, target: self, action: #selector(self.cancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "确定", style: .Plain, target: self, action: #selector(self.save))
    }
    
    func cancel(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func save(){
        saveAction(value: (filterContent))
        cancel()
    }
    
    func calendarConfigure(){
        calendarManager.delegate = self
        calendarManager.menuView = CalenderMenuView
        calendarManager.contentView = CalenderContentView
        calendarManager.setDate(NSDate())
        dateStart = filterContent.dateStart
        dateEnd = filterContent.dateEnd
        calendarManager.setDate(dateStart)
    }
    
    @IBAction func ButtonTimeSwitchClicked(sender: UIButton) {
        currentDateStyle = sender.currentTitle!
        lastBtn.selected = false
        lastBtn = sender
        lastBtn.selected = true
    }

    
    private func checkChooseDayOK(date:NSDate) -> Bool{
        if(currentDateStyle == "开始时间"){
            return dateEnd.timeIntervalSinceDate(date) >= 0
        }else{
            return date.timeIntervalSinceDate(dateStart) >= 0
        }
    }
    
}

extension IODetailFilterViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 3
        }
        
        if section == 1{
            return 2
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell?.textLabel?.text = titles[indexPath.section][indexPath.row]
        cell?.accessoryType = .None
        if filterContent.one == indexPath.row && indexPath.section == 0 {
            cell?.accessoryType = .Checkmark
        }
        if indexPath.section == 1 && (filterContent.two & (indexPath.row + 1) == (indexPath.row + 1)){
            cell?.accessoryType = .Checkmark
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headTitles[section]
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            self.filterContent.one = indexPath.row
            if let cell = self.selectedTypeCell {
                cell.accessoryType = .None
            }
            self.selectedTypeCell = tableView.cellForRowAtIndexPath(indexPath)
            self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .None)
        }else{
            if self.filterContent.two ^ (indexPath.row + 1) != 0{
                self.filterContent.two ^= (indexPath.row + 1)
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            }
        }
    }
    
}

extension IODetailFilterViewController:JTCalendarDelegate{
    
    func calendar(calendar: JTCalendarManager!, prepareMenuItemView menuItemView: UIView!, date: NSDate!) {
        func StringFromdateFormatterWithYearAndMonth(date:NSDate) -> String{
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy年MM月"
            return formatter.stringFromDate(date)
        }
        if let label = menuItemView as? UILabel{
            label.text = StringFromdateFormatterWithYearAndMonth(date)
        }
    }
    
    func calendar(calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        if let dayView = dayView as? JTCalendarDayView{
            if calendarManager.dateHelper.date(NSDate(), isTheSameDayThan: dayView.date){
                dayView.circleView.hidden = true
                dayView.textLabel.textColor = UIColor.DangerColor()
            }else
                if !calendarManager.dateHelper.date(CalenderContentView.date, isTheSameMonthThan: dayView.date) {
                    dayView.circleView.hidden = true
                    dayView.textLabel.textColor = UIColor.LowBlackColor()
                }else{
                    dayView.circleView.hidden = true
                    dayView.textLabel.textColor = UIColor.MidBlackColor()
            }
            if  calendarManager.dateHelper.date(currentDate, isTheSameDayThan: dayView.date){
                dayView.circleView.hidden = false
                dayView.circleView.backgroundColor = UIColor.MainColor()
                dayView.textLabel.textColor = UIColor.whiteColor()
            }
        }
    }
    
    func calendar(calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        if let dayView = dayView as? JTCalendarDayView{
            guard checkChooseDayOK(dayView.date) == true else {return}
            currentDate = dayView.date
            dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
            UIView.transitionWithView(dayView, duration: 0.3, options: UIViewAnimationOptions.CurveEaseIn,  animations: { [weak self] in
                dayView.circleView.transform = CGAffineTransformIdentity
                self?.calendarManager.reload()
                }, completion: nil)
            if !self.calendarManager.dateHelper.date(CalenderContentView.date, isTheSameMonthThan: dayView.date){
                if CalenderContentView.date.compare(dayView.date) == .OrderedAscending{
                    CalenderContentView.loadNextPageWithAnimation()
                }else{
                    CalenderContentView.loadPreviousPageWithAnimation()
                }
            }
            
        }
    }

}
