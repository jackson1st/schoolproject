//
//  IOHeaderViewItem.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/7.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class IOHeaderViewItem: UIView {

    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelSize: UILabel!
    @IBOutlet weak var LabelSupplyName: UILabel!
    @IBOutlet weak var LabelIn: UILabel!
    @IBOutlet weak var LabelOut: UILabel!
    @IBOutlet weak var LabelPrice: UILabel!
    @IBOutlet weak var LabelStock: UILabel!
    var selectedAction:(()->Void)?
    func fillContent(name:String,size:String,supplyName:String,inData:Int,outData:Int,price:Double,stock:Int,unit:String,selectedAction:()->Void){
        self.LabelName.text = name
        self.LabelSize.text = "规格:" + size
        self.LabelSupplyName.text = "供应商:" + supplyName
        if inData != 0{
            self.LabelIn.text = String(format: "入库%d", inData)
        }
        else{
            LabelIn.text = nil
        }
        if outData != 0{
            self.LabelOut.text = String(format: "出库%d", outData)
        }else{
            LabelOut.text = nil
        }
        self.LabelPrice.text = String(format: "¥%.1f", price)
        self.LabelStock.text = String(format: "库存 %d ", stock) + unit
        self.selectedAction = selectedAction
    }
    @IBAction func ButtonSelectedClicked(sender: AnyObject) {
        if let action = self.selectedAction{
            action()
        }
    }
    
    class func initFromxib() -> IOHeaderViewItem{
        return NSBundle.mainBundle().loadNibNamed("IOHeaderViewItem", owner: nil, options: nil)[0] as! IOHeaderViewItem
    }
}
