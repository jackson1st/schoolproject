//
//  IODetailViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/7.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class IODetailViewController: JSViewController{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var LabelTimeRange: UILabel!
    var headerViewPeople = [IOHeaderViewPeople]()
    var headerViewItem = [IOHeaderViewItem]()
    var itemIOList = [ItemIOList](){
        didSet{
            visibItemIOList = itemIOList
        }
    }
    
    var visibItemIOList = [ItemIOList](){
        didSet{
            for _ in 0..<max(0,visibItemIOList.count - headerViewItem.count) {
                headerViewItem.append(IOHeaderViewItem.initFromxib())
                itemIOListSelected.append(false)
            }
            for i in 0..<visibItemIOList.count{
                headerViewItem[i].fillContent(visibItemIOList[i].name, size: visibItemIOList[i].specification, supplyName: visibItemIOList[i].supplier_name, inData: visibItemIOList[i].inData, outData: visibItemIOList[i].outData, price: visibItemIOList[i].unit_price, stock: visibItemIOList[i].stock_quantity, unit: visibItemIOList[i].unit, selectedAction: {[weak self] in
                    self?.itemIOListSelected[i] = !self!.itemIOListSelected[i]
                    self?.tableView.reloadSections(NSIndexSet(index: i), withRowAnimation: UITableViewRowAnimation.Automatic)
                    })
                itemIOListSelected[i] = false
            }
            self.tableView.reloadData()
        }
    }
    var itemIOListSelected = [Bool]()
    
    
    var peopleIOList = [PeopleIOList](){
        didSet{
            visibPeopleList = peopleIOList
        }
    }
    var visibPeopleList = [PeopleIOList](){
        didSet{
            for _ in 0..<max(0,visibPeopleList.count - headerViewPeople.count){
                headerViewPeople.append(IOHeaderViewPeople.initFromxib())
                peopleIOListSelected.append(false)
            }
            for i in 0..<visibPeopleList.count{
                headerViewPeople[i].fillContent(visibPeopleList[i].name, inData: visibPeopleList[i].inData, outData: visibPeopleList[i].outData, selectedAction: { [weak self] in
                    self?.peopleIOListSelected[i] = !self!.peopleIOListSelected[i]
                    self?.tableView.reloadSections(NSIndexSet(index: i), withRowAnimation: .Automatic)
                    
                    })
                peopleIOListSelected[i] = false
            }
            self.tableView.reloadData()
        }
    }
    var peopleIOListSelected = [Bool]()
    
    
    lazy var detailFilterViewController:IODetailFilterViewController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = story.instantiateViewControllerWithIdentifier("IODetailFilterViewController") as! IODetailFilterViewController
        vc.saveAction = {
            tuple in
            self.filterContent = (tuple.0,tuple.1,tuple.2,tuple.3)
        }
        return vc
    }()
    var searController:UISearchController!
    var filterString:String?{
        didSet{
            if filterString == nil || filterString == "" {
                if filterContent.0 == 0{
                    visibItemIOList = self.itemIOList
                }else{
                    visibPeopleList = self.peopleIOList
                }
            }else{
                if filterContent.0 == 0{
                    visibItemIOList = self.itemIOList.filter{$0.name.containsString(filterString!)}
                }else{
                    visibPeopleList = self.peopleIOList.filter{$0.name.containsString(filterString!)}
                }
            }
        }
    }
    lazy var dateFormatter:NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    var filterContent:(one:Int,two:Int,dateStart:NSDate,dateEnd:NSDate)!{
        didSet{
            LabelTimeRange.text = dateFormatter.stringFromDate(filterContent.2) + " 至 " + dateFormatter.stringFromDate(filterContent.3)
            NetWorkManager.getIOList(filterContent, block: { [weak self] list in
                if let sSelf = self {
                    if sSelf.filterContent.0 == 0{
                        if let newList = list as? [ItemIOList]{
                            sSelf.itemIOList = newList
                        }
                    }else{
                        if let newList = list as? [PeopleIOList]{
                            sSelf.peopleIOList = newList
                        }
                    }
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterContent = (one:0,two:1,dateStart:NSDate(),dateEnd:NSDate())
        searchControllerConfigure()
        self.title = "明细"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "筛选", style: .Plain, target: self, action: #selector(self.buttonFilterClicked))
        tableViewConfigure()
    }
    
    func tableViewConfigure(){
        tableView.separatorStyle = .None
    }
    
    func searchControllerConfigure(){
        searController = UISearchController(searchResultsController: nil)
        searController.searchResultsUpdater = self
        searController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searController.searchBar
    }
    
    func buttonFilterClicked(){
        detailFilterViewController.filterContent = (one:self.filterContent.one,two:self.filterContent.two,dateStart:self.filterContent.2,dateEnd:self.filterContent.3)
        self.presentViewController(JSNavigationController(rootViewController: detailFilterViewController), animated: true, completion: nil)
    }

    
}

extension IODetailViewController:UISearchResultsUpdating,UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if filterContent.one == 0 {
            return self.visibItemIOList.count
        }else{
            return self.visibPeopleList.count
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterContent.one == 0{
            if itemIOListSelected[section] == true{
                return visibItemIOList[section].list.count
            }else{
                return 0
            }
        }else{
            if peopleIOListSelected[section] == true{
                return visibPeopleList[section].list.count
            }else{
                return 0
            }

        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! IODetailTableViewCell
        if filterContent.0 == 0{
            cell.fillContent(itemIOList[indexPath.section].realList[indexPath.row].ItemBriefStr)
        }else{
            cell.fillContent(peopleIOList[indexPath.section].realList[indexPath.row].PeopleBriefStr)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if filterContent.0 == 0{
            return headerViewItem[section]
        }
        
        return headerViewPeople[section]
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if filterContent.0 == 0{
            return 82
        }
        
        return 42
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController){
        guard searController.active else {return}
        self.filterString = searController.searchBar.text
    }
}
