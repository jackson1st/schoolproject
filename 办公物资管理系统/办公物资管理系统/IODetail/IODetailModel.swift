//
//  IODetailModel.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/8.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import YYModel
class  PeopleIOList:NSObject {
    var name:String!
    var inData:Int = 0
    var outData:Int = 0
    var list:NSArray!
    
    lazy var realList:[IODetailItem] = {
        return self.list as! [IODetailItem]
    }()
    
    static func modelContainerPropertyGenericClass() -> [String : AnyObject]?{
        return ["list":IODetailItem.self]
    }
    
    static func modelPropertyBlacklist() -> [String]?{
        return ["inData","outData","realList"]
    }
    
    func modelCustomTransformFromDictionary(dic: [NSObject : AnyObject]) -> Bool{
        
        list.forEach { (x) in
            if let object = x as? IODetailItem{
                if object.inout_class == 0{
                    //出库
                    outData += 1
                }else{
                    //入库
                    inData += 1
                    
                }
            }
        }
        
        return true
    }

}

class ItemIOList:NSObject,YYModel{
    var name = ""
    var id:String = ""
    var class_name:String = ""
    var specification:String = ""
    var model:String = ""
    var unit:String = ""
    var unit_price:Double = 0.0
    var stock_quantity:Int = 0
    var stock_price:Double = 0.0
    var max_number:Int = 0
    var min_number:Int = 0
    var warn:Int = 0
    var supplier_id:String = ""
    var supplier_name:String = ""
    var remarks:String = ""
    var identifyCode:String = ""
    var inData:Int = 0
    var outData:Int = 0
    var list:NSArray!
    lazy var realList:[IODetailItem] = {
        return self.list as! [IODetailItem]
    }()
    
    static func modelContainerPropertyGenericClass() -> [String : AnyObject]?{
        return ["list":IODetailItem.self]
    }
    
    static func modelPropertyBlacklist() -> [String]?{
        return ["inData","outData","realList"]
    }
    
    func modelCustomTransformFromDictionary(dic: [NSObject : AnyObject]) -> Bool{
        list.forEach { (x) in
            if let object = x as? IODetailItem{
               if object.inout_class == 0{
                    //出库
                    outData += 1
               }else{
                    //入库
                    inData += 1
                
                }
            }
        }
        return true
    }
    
}

class IODetailItem:NSObject,YYModel {
    var id:String!
    var orderID:String!//订单编号
    var tag:String?//用于还款标记
    var inout_class:Int = 0
    var detail_class:Int = 0
    var class_id:String!
    var item_id:String!
    var item_name:String!
    var specifications:String!
    var model:String!
    var unit:String!
    var unit_price:Double = 0
    var quantity:Int = 0
    var sum_price:Double = 0
    var date:Double = 0.0
    var stock_quantity:Double = 0.0
    var demarks:String!
    var operator_staff:String!
    var object_name:String!
    var object_id:String!
    var object_class:String!//执行单位类型
    static var dateFormatter:NSDateFormatter {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd hh:mm"
        return dateFormatter
    }
    var timeStr:String {
        get{
            return IODetailItem.dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: date))
        }
    }
    var numStr:String{
        get{
            return (inout_class == 1 ? "+" : "-") + "\(quantity)"
        }
    }
    var ItemBriefStr:(String,String,String)!{
        get{
            var second = ""
            switch inout_class {
            case 1:
                switch detail_class {
                case 0:
                    second = "购入"
                case 1:
                    second = "归还人: " + object_name
                case 2:
                    second = "归还部门: " + object_name
                default:
                    break
                }
            case -1:
                switch detail_class {
                case 1:
                    second = "借出人: " + object_name
                case 2:
                    second = "借出部门: " + object_name
                default:
                    break
                }
            default:
                break
            }
            return (timeStr,second,numStr)
        }
    }
    
    var PeopleBriefStr:(String,String,String)!{
        get{
            return (item_name,timeStr,numStr)
        }
    }
    
    
    
}