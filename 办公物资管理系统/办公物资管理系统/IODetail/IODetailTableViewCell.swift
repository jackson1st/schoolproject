//
//  IODetailTableViewCell.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/7.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class IODetailTableViewCell: UITableViewCell {

    @IBOutlet weak var LabelFirst: UILabel!
    @IBOutlet weak var LabelSecond: UILabel!
    @IBOutlet weak var LabelThird: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func fillContent(content:(String,String,String)){
        LabelFirst.text = content.0
        LabelSecond.text = content.1
        LabelThird.text = content.2
    }
}
