//
//  BuyItemTableViewCell.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/10.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit

class BuyItemTableViewCell: UITableViewCell,Reusable {

    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var LabelPrice: UILabel!
    @IBOutlet weak var LabelSize: UILabel!
    @IBOutlet weak var LabelSupplyName: UILabel!
    @IBOutlet weak var TextFieldNum: UITextField!
    @IBOutlet weak var LabelStock: UILabel!
    @IBOutlet weak var LabelTip: UILabel!
    @IBOutlet weak var ButtonAdd: UIButton!
    @IBOutlet weak var ButtonDec: UIButton!
    var model = 0{
    //两种情况，一种是明确物品，第二种是物品还没添加，需要添加物品。
        didSet{
            if model == 0 {
                LabelTip.hidden = true
                LabelPrice.hidden = false
                LabelSize.hidden = false
                LabelSupplyName.hidden = false
                TextFieldNum.hidden = false
                ButtonAdd.hidden = false
                ButtonDec.hidden = false
                LabelStock.hidden = false
            }else{
                LabelTip.hidden = false
                LabelPrice.hidden = true
                LabelSize.hidden = true
                LabelSupplyName.hidden = true
                TextFieldNum.hidden = true
                ButtonAdd.hidden = true
                ButtonDec.hidden = true
                LabelStock.hidden = true
            }
        }
    }
    var num = 0{
        didSet{
            TextFieldNum.text = "\(num)"
        }
    }
    var item:(Item,Int)!{
        didSet{
            let tmpItem = item.0
            let num = item.1
            if tmpItem.name == nil{
                model = 1
                LabelName.text = "识别码: " + tmpItem.identifyCode
            }else{
                model = 0
                LabelName.text = tmpItem.name
                LabelPrice.text = String(format: "¥ %.1f", tmpItem.unit_price)
                LabelSize.text = tmpItem.specification
                LabelSupplyName.text = tmpItem.supplier_name
                LabelStock.text = String(format: "库存 %d%@", tmpItem.stock_quantity,tmpItem.unit)
                self.num = num
            }
        }
    }
    var numChangeAction:((Int) -> Void)!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func ButtonNumClicked(sender: UIButton) {
        if sender.tag == 101{
            if num > 1{
                num -= 1
            }
        }else{
            if num < 99{
                num += 1
            }
        }
        numChangeAction(num)
    }
}
