//
//  InBuyViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/10.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import MBProgressHUD
class InBuyViewController: JSViewController {

    var tableView:UITableView!
    var itemList = [(Item,Int)]()
    var actionContinueAdd:((Int)->Void)!
    lazy var itemDetailViewController:ItemDetailViewController = {
        let story = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = story.instantiateViewControllerWithIdentifier("ItemDetailViewController") as! ItemDetailViewController
        vc.model = 1
        return vc
    }()
    
    lazy var itemListViewController:SearchItemViewController = {
        let vc = SearchItemViewController()
        vc.model = 1
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "购入确认"
        tableViewConfigure()
        navigationItemConfigure()
    }
    
    func tableViewConfigure(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.snp_makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(self.view)
        }
        tableView.registerReusableCell(BuyItemTableViewCell)
        tableView.separatorStyle = .None
    }
    
    func navigationItemConfigure(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "补充", style: .Plain, target: self, action: #selector(self.continueAddItem))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完成", style: .Plain, target: self, action: #selector(self.complete))
    }
    
    func complete(){
        //发送网络请求
        for x in self.itemList{
            if x.0.name == nil {
                MBProgressHUD.showError("还有新物品未创建", toView: nil)
                return
            }
        }
        
        self.dismissViewControllerAnimated(true) { [weak self] in
             self?.actionContinueAdd(3)//3代表入库结束
        }
        MBProgressHUD.showSuccess("入库完成", toView: nil)
       
    }
    
    func continueAddItem(){
        
        let alertController = UIAlertController(title: "添加方式", message: "您想通过什么方式添加", preferredStyle: .ActionSheet)
        alertController.addAction(UIAlertAction(title: "扫码添加", style: .Default, handler: { [weak self] action in
            if let sSelf = self{
                sSelf.actionContinueAdd(1)
            }
        }))
        alertController.addAction(UIAlertAction(title: "手动输入", style: .Default, handler: { [weak self] action in
            if let sSelf = self {
                sSelf.actionContinueAdd(2)
            }
        }))
        alertController.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    
}

extension InBuyViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as BuyItemTableViewCell
        cell.item = self.itemList[indexPath.row]
        cell.numChangeAction = {
            num in
            self.itemList[indexPath.row].1 = num
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.itemList[indexPath.row].0.name == nil{
                let alertController = UIAlertController(title: "新物品", message: "是否创建", preferredStyle: .ActionSheet)
                alertController.addAction(UIAlertAction(title: "创建新物品", style: .Default, handler: { [weak self] action in
                    if let sSelf = self{
                        sSelf.itemDetailViewController.item = sSelf.itemList[indexPath.row].0
                        sSelf.itemDetailViewController.ActionSave = {
                            item in
                            sSelf.itemList[indexPath.row].0 = item
                            sSelf.itemList[indexPath.row].1 = 1
                            sSelf.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                        }
                        sSelf.navigationController?.pushViewController(sSelf.itemDetailViewController, animated: true)
                    }
                    }))
                alertController.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(title: "提示", message: nil, preferredStyle: .ActionSheet)
            alertController.addAction(UIAlertAction(title: "其他物品", style: .Default, handler: { [weak self] action in
                if let sSelf = self{
                   sSelf.itemListViewController.filterString = sSelf.itemList[indexPath.row].0.identifyCode
                    sSelf.itemListViewController.otherItem = sSelf.itemList[indexPath.row].0
                    sSelf.itemListViewController.selectedAction = {
                        item in
                        sSelf.itemList[indexPath.row].0 = item
                        sSelf.itemList[indexPath.row].1 = 1
                        sSelf.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    }
                }
            }))
            alertController.addAction(UIAlertAction(title: "修改价格", style: .Default, handler: { [weak self] action in
                if let sSelf = self{
                    let PriceAlertController = UIAlertController(title: "修改价格", message: nil, preferredStyle: .Alert)
                    PriceAlertController.addTextFieldWithConfigurationHandler({ (textField) in
                        textField.text = String(format: "%.1f",sSelf.itemList[indexPath.row].0.unit_price)
                    })
                    PriceAlertController.addAction(UIAlertAction(title: "确定", style: .Default, handler: { _ in
                         let price = (PriceAlertController.textFields![0].text! as NSString).doubleValue
                         sSelf.itemList[indexPath.row].0.unit_price = price
                        sSelf.itemList[indexPath.row].0.newCreate = true
                        sSelf.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    }))
                    PriceAlertController.addAction(UIAlertAction(title: "取消", style: .Cancel, handler:nil))
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { 
                        NSThread.sleepForTimeInterval(0.5)
                        dispatch_sync(dispatch_get_main_queue(), { 
                            sSelf.presentViewController(PriceAlertController, animated: true, completion: nil)
                        })
                    })
                    
                }
            }))
            alertController.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)

        }
    }
    

}
