//
//  InandOutViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/9.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import AudioToolbox
class InandOutViewController: JSViewController {

    @IBOutlet weak var ButtonBuy: UIButton!
    @IBOutlet weak var ButtonReturn: UIButton!
    @IBOutlet weak var ButtonOut: UIButton!
    @IBOutlet weak var StackContentView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    var LabelTip:UILabel!
    var soundID = SystemSoundID(kSystemSoundID_Vibrate)
    var codeScanViewController:CodeScanViewController!
    var constraintHeight:NSLayoutConstraint!
    var codeContentView:UIView!
    var items = [(Item,Int)]()
    var model = 1{
        //三种模式 1:购入 2:还入 3:出库
        didSet{
            switch model {
                case 1:
                    self.title = "入库登记"
                    self.tip = "请扫描物品识别码"
                    self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "结束", style: .Plain, target: self, action: #selector(self.ModeFinishInBuy))]
                case 2:
                    self.title = "物品归还"
                    self.tip = "请先扫描归还人或单位识别码"
                    self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "结束", style: .Plain, target: self, action: #selector(self.ModeFinishReturn))]
                case 3:
                    self.title = "物品借出"
                    self.tip = "请先扫描借出人或单位识别码"
                    self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "结束", style: .Plain, target: self, action: #selector(self.ModeFinishOut))]
                default:break
            }
        }
    }
    var tip:String?{
        get{
            return LabelTip.text
        }
        set{
            LabelTip.text = newValue
        }
    }
    
    lazy var inBuyViewController:InBuyViewController = {
        let vc = InBuyViewController()
        vc.actionContinueAdd = {
            tag in
            
            
        }
        return vc
    }()
    lazy var searchItemViewController:SearchItemViewController = {
        let vc = SearchItemViewController()
        vc.model = 0
        vc.selectedAction = {[weak self]
            item in
            if let sSelf = self {
                sSelf.items.append((item,1))
            }
        }
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeScanViewControllerConfigure()
        ButtonConfigure()
        ViewTipConfigure()
        self.title = "入库&出库"
    }
    
    func ViewTipConfigure(){
        LabelTip = UILabel()
        LabelTip.backgroundColor = UIColor(RGB: 0x333333, alpha: 0.6)
        LabelTip.textColor = UIColor.whiteColor()
        LabelTip.numberOfLines = 0
        LabelTip.textAlignment = .Center
        LabelTip.font = UIFont(name: NormalLanTingHeiFontName, size: 16)
        LabelTip.layer.cornerRadius = 6
        LabelTip.clipsToBounds = true
        self.view.addSubview(LabelTip)
        LabelTip.snp_makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalTo(self.view.frame.width - 50)
            make.bottom.equalTo(self.view).offset(-100)
            make.centerX.equalTo(self.view)
        }
        LabelTip.hidden = true
    }
    
    func ModeExit(){
        closeCodeScan()
        self.navigationItem.leftBarButtonItems = nil
        self.navigationItem.rightBarButtonItems = nil
        self.title = "入库&出库"
    }
    
    func ModePause(){
        self.navigationItem.leftBarButtonItems![1].title = "继续"
        self.navigationItem.leftBarButtonItems![1].action = #selector(self.ModeFire)
        self.codeScanViewController.session.stopRunning()
    }
    
    func ModeFire(){
        self.navigationItem.leftBarButtonItems![1].title = "暂停"
        self.navigationItem.leftBarButtonItems![1].action = #selector(self.ModePause)
        self.codeScanViewController.session.startRunning()
    }
    
    func ModeEnter(){
        items.removeAll(keepCapacity: false)
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "退出", style: .Plain, target: self, action: #selector(self.ModeExit)),UIBarButtonItem(title: "暂停", style: .Plain, target: self, action: #selector(self.ModePause))]
        openCodeScan()
    }
    
    
    func ModeFinishInBuy(){
        
    }
    
    func ModeFinishReturn(){
        
    }
    
    func ModeFinishOut(){
        
    }
    
    
    
    func ButtonConfigure(){
        ButtonBuy.layer.cornerRadius = 75
        ButtonBuy.layer.borderWidth = 4
        ButtonBuy.layer.borderColor = UIColor(RGB: 0x0076FF,alpha: 1).CGColor
        ButtonOut.layer.cornerRadius = 75
        ButtonOut.layer.borderWidth = 4
        ButtonOut.layer.borderColor = UIColor(RGB: 0x0076FF,alpha: 1).CGColor
        ButtonReturn.layer.cornerRadius = 75
        ButtonReturn.layer.borderWidth = 4
        ButtonReturn.layer.borderColor = UIColor(RGB: 0x0076FF,alpha: 1).CGColor
    }
    
    @IBAction func ButtonClicked(sender: UIButton) {
        self.model = sender.tag - 100
        ModeEnter()
    }
    

    
    func closeCodeScan(){
        codeScanViewController.stopRuning()
        UIView.animateWithDuration(0.5) {
            self.constraintHeight.constant = 0
            self.view.layoutIfNeeded()
            self.LabelTip.hidden = true

        }
    }
    
    func openCodeScan(){
        UIView.animateWithDuration(0.5) {
            self.constraintHeight.constant = self.view.frame.height
            self.view.layoutIfNeeded()
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            NSThread.sleepForTimeInterval(0.5)
            dispatch_async(dispatch_get_main_queue(), {
                self.codeScanViewController.setupAndFire()
                self.LabelTip.hidden = false
            })
        })
    }
    
    
    
    func codeScanViewControllerConfigure(){
        self.codeScanViewController = CodeScanViewController()
        self.codeScanViewController.resultAction = { code in
            self.resultAction(code)
        }
        codeContentView = UIView()
        codeContentView.backgroundColor = UIColor.blackColor()
        codeContentView.addSubview(codeScanViewController.view)
        codeContentView.addConstraints([NSLayoutConstraint(item: codeScanViewController.view, attribute: .Top, relatedBy: .Equal, toItem: codeContentView, attribute: .Top, multiplier: 1, constant: 0),NSLayoutConstraint(item: codeScanViewController.view, attribute: .Bottom, relatedBy: .Equal, toItem: codeContentView, attribute: .Bottom, multiplier: 1, constant: 0),NSLayoutConstraint(item: codeScanViewController.view, attribute: .Left, relatedBy: .Equal, toItem: codeContentView, attribute: .Left, multiplier: 1, constant: 0),NSLayoutConstraint(item: codeScanViewController.view, attribute: .Right, relatedBy: .Equal, toItem: codeContentView, attribute: .Right, multiplier: 1, constant: 0)
            ])
        self.view.addSubview(codeContentView)
        codeContentView.translatesAutoresizingMaskIntoConstraints = false
        codeContentView.frame = self.view.bounds
        self.addChildViewController(codeScanViewController)
        let constraintCenterX = NSLayoutConstraint(item: codeContentView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0)
        let constraintCenterY = NSLayoutConstraint(item: codeContentView, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1, constant: 0)
        let constraintWidth = NSLayoutConstraint(item: codeContentView, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 1, constant: 0)
        constraintHeight = NSLayoutConstraint(item: codeContentView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
        self.view.addConstraints([constraintCenterX,constraintCenterY,constraintHeight,constraintWidth])
        
    }

    
    func resultAction(code:String){
        AudioServicesPlaySystemSound(soundID)
        
        switch model {
        case 1:
            //买入模式
            resultActionInBuy(code)
        case 2:
            break
        case 3:
            break
        default:
            break
        }
    }
    
    
    func resultActionInBuy(code:String){
        if model == 1{
            self.tip = "识别码: " + code
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) { 
                NSThread.sleepForTimeInterval(0.8)
                dispatch_async(dispatch_get_main_queue(), { 
                    self.tip = "扫描物品识别码或者\"结束\""
                    self.ModeFire()
                })
            }
        }
    }
    


}
