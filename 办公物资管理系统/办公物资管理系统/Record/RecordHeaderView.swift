//
//  RecordHeaderView.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/3.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import SnapKit
class RecordHeaderView: UIView {
    let labelLeft = UILabel()
    let labelRight = UILabel()
    
    func setContent(left:String,right:String){
        labelLeft.text = left
        labelRight.text = right
    }
    
    init(){
        super.init(frame: CGRectZero)
        self.addSubview(labelLeft)
        self.addSubview(labelRight)
        labelLeft.snp_makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(10)
        }
        labelRight.snp_makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.right.equalTo(self).offset(-10)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
