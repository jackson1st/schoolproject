//
//  HomeCollectionViewCell.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/2.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import SDWebImage
class HomeCollectionViewCell: UICollectionViewCell,Reusable {
    
    var imgView = UIImageView()
    @IBOutlet weak var Button: UIButton!
    var content:(title:String,url:String)!{
        didSet{
            Button.setTitle(content.0, forState: .Normal)
            imgView.sd_setImageWithURL(NSURL(string: content.1)!) { (img, _, _, _) in
                self.Button.setImage(img, forState: .Normal)
                self.Button.verticalImgAndTitle(5)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        Button.setImage(imgView.image, forState: .Normal)
        self.backgroundColor = UIColor.whiteColor()
        let view = UIView()
        view.backgroundColor = UIColor.lightGrayColor()
        self.selectedBackgroundView = view
    }

}
