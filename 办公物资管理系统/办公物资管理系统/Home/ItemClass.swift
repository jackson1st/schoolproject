//
//  ItemClass.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/5.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import RealmSwift
class ItemClass: Object {
    dynamic var id = ""
    dynamic var name = ""
    dynamic var photoURL = ""
    convenience init(id:String,name:String,photoURL:String){
        self.init()
        self.id = id
        self.name = name
        self.photoURL = photoURL
    }
}

