//
//  HomeViewController.swift
//  办公管理系统
//
//  Created by jason on 16/5/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import YYModel
import SDWebImage
class HomeViewController: JSViewController,UISearchResultsUpdating{

    @IBOutlet weak var collectionView: UICollectionView!
    var searchController:UISearchController!
    var tableView:UITableView!
    var itemClasses = [ItemClass]()
    var itemListController = ItemListViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchControllerConfigure()
        collectionViewConfigure()
        testData()
    }
    
    func testData(){
        for _ in 0..<10{
            itemClasses.append(ItemClass(id: "123", name: "电脑", photoURL: "http://img11.360buyimg.com/n5/jfs/t2050/286/2344311381/99342/3d988cd2/56fe3d25Na0ebdff2.jpg"))
        }
    }
    
    func searchControllerConfigure(){
        tableView = UITableView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
        self.view.addSubview(tableView)
        tableView.snp_makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(self.view)
        }
        searchController = UISearchController(searchResultsController: nil)
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
    }
    
    func collectionViewConfigure(){
        self.view.bringSubviewToFront(collectionView)
        collectionView.backgroundColor = UIColor.BackgroundColor()
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSizeMake(ScreenWidth / 4, ScreenWidth / 4 + 10)
        collectionView.collectionViewLayout = layout
        collectionView.registerNib(HomeCollectionViewCell.nib, forCellWithReuseIdentifier: "cell")
    }
}

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemClasses.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! HomeCollectionViewCell
        cell.content = (itemClasses[indexPath.row].name,itemClasses[indexPath.row].photoURL)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        itemListController.title = itemClasses[indexPath.row].name
        self.navigationController?.pushViewController(itemListController, animated: true)
    }
    
}

extension HomeViewController:UISearchBarDelegate{
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        collectionView.hidden = true
//        self.presentViewController(searchController, animated: true, completion: nil)
        return true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        collectionView.hidden = false
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController){
        guard searchController.active else {return}
        
        
    }

}
