//
//  NetWorkManager.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/5.
//  Copyright © 2016年 jason. All rights reserved.
//

import Foundation
import Alamofire
var path = "http://192.168.31.3:8080/stumarket/"
public enum NetDestination:String{
    var url:String {
        get{
            return path + self.rawValue
        }
    }
    case GoodsInfoAndType = "ShopGoods/getGoodsInfoAndType.do" //获取店铺商品列表
    case AppGoodsByPId = "ShopGoods/getAppGoodsByPId.do" //获取商品大类信息
    case GoodsSpec = "ShopGoods/getGoodsSpec.do"//查询具体规格的商品
    case GoodOnShow = "ShopGoods/getSGoodsOnShowType.do"//请求展示类目
}


public class NetWorkManager {
    
    public class func getItemList(classID:String,block:(items:[Item])->Void){
        
        var items = [Item]()
        for _ in 0..<10{
            items.append(Item(dict:["name":"电脑","id":"1234","class_name":"电脑","unit_price":12.5,"stock_quantity":10,"stock_price":125,"supplier_name":"栗子科技","specification":"4G内存 1T硬盘","identifyCode":"19940603","max_number":10,"min_number":1,"remarks":"这是虚拟数据","unit":"台"]))
        }
        block(items: items)
        
    }
    
    public class func getIOList(filterContent: (one:Int,two:Int,dateStart:NSDate,dateEnd:NSDate),block:(AnyObject)->Void){
        //测试先不考虑
        if filterContent.0 == 0{
            var list = [ItemIOList]()
            for _ in 0..<10{
                list.append(ItemIOList.yy_modelWithDictionary(["name":"栗子科技MacBook Pro","id":"123","class_name":"电脑","specification":"512G固态硬盘，16G内存","supplier_name":"栗子科技","unit_price":12800,"stock_quantity":10,"stock_price":128000,"unit":"台","list":[["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":1,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],
                    ["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],
                    ["id":"1234","orderID":"1233241","inout_class":1,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"]]])!)
            }
            block(list)
        }else{
            var list = [PeopleIOList]()
            for _ in 0..<10{
                list.append(PeopleIOList.yy_modelWithDictionary(["name":"MacBook Pro","id":"123","class_name":"电脑","specification":"512G固态硬盘，16G内存","supplier_name":"栗子科技","unit_price":12800,"stock_quantity":10,"stock_price":128000,"unit":"台","list":[["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],["id":"1234","orderID":"1233241","inout_class":1,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],
                    ["id":"1234","orderID":"1233241","inout_class":0,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"],
                    ["id":"1234","orderID":"1233241","inout_class":1,"detail_class":0,"item_id":"123","item_name":"MacBook Pro","specifications":"4G内存 1T硬盘","model":"??","unit":"台","unit_price":12800,"quantity":1,"sum_price":12800,"date":1233123,"stock_quantity":1,"operator_staff":"Jason","object_name":"Jason","object_id":"434","object_class":"people"]]])!)
            }
            block(list)
        }
    }
}

func JSRequest(URLString: NetDestination, parameters: [String : AnyObject]?) -> Request{
    
    return Alamofire.request(.POST, URLString.url, parameters: parameters, encoding: .JSON, headers: nil)
}

extension Request{
    
    func JSResponse(Block:(JSON:[String:AnyObject])->Void) -> Self{
        return responseJSON(completionHandler: { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? [String:AnyObject]{
                    Block(JSON:json)
                }
            }else{
                Block(JSON:["message":"请求失败"])
            }
        })
    }
}