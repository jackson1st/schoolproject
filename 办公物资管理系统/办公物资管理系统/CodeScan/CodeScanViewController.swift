//
//  CodeViewController.swift
//  办公物资管理系统
//
//  Created by jason on 16/6/1.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
import AVFoundation

class CodeScanViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    var scanRectView: UIView!
    var device: AVCaptureDevice!
    var input: AVCaptureDeviceInput!
    var output: AVCaptureMetadataOutput!
    var session: AVCaptureSession!
    var preview: AVCaptureVideoPreviewLayer!
    var scanVisibleView:UIView!
    var resultAction:((String)->Void)!

    override func viewDidLoad() {
        super.viewDidLoad()
        scanViewConfigure()
    }
    
    func scanViewConfigure(){
        do{
            self.device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            self.input = try AVCaptureDeviceInput(device: device)
            self.output = AVCaptureMetadataOutput()
            output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            self.session = AVCaptureSession()
            if UIScreen.mainScreen().bounds.size.height < 500{
                self.session.sessionPreset = AVCaptureSessionPreset640x480
            }else{
                self.session.sessionPreset = AVCaptureSessionPresetHigh
            }
            self.session.addInput(self.input)
            self.session.addOutput(self.output)
            self.output.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code,
                                               AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code,
                                               AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeCode93Code]
            let windowSize: CGSize = UIScreen.mainScreen().bounds.size
            let scanSize: CGSize = CGSizeMake(windowSize.width*3/4, windowSize.width*3/4)
            var scanRect: CGRect = CGRectMake((windowSize.width-scanSize.width)/2,
                                              (windowSize.height-scanSize.height)/2, scanSize.width, scanSize.height)
            scanRect = CGRectMake(scanRect.origin.y/windowSize.height,
                                  scanRect.origin.x/windowSize.width,
                                  scanRect.size.height/windowSize.height,
                                  scanRect.size.width/windowSize.width)
            self.output.rectOfInterest = scanRect
            self.preview = AVCaptureVideoPreviewLayer(session: self.session)
            self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.preview.frame = UIScreen.mainScreen().bounds
            
            self.scanRectView = UIView()
            self.scanRectView.frame = CGRectMake(0, 0, scanSize.width, scanSize.height)
            self.scanRectView.center = CGPointMake(
                CGRectGetMidX(UIScreen.mainScreen().bounds),
                CGRectGetMidY(UIScreen.mainScreen().bounds));
            self.scanRectView.layer.borderColor = UIColor.greenColor().CGColor
            self.scanRectView.layer.borderWidth = 1;
        
            
            do {
                try self.device!.lockForConfiguration()
            } catch _ {
                NSLog("Error: lockForConfiguration.");
            }
            self.device!.videoZoomFactor = 1.5
            self.device!.unlockForConfiguration()
        }catch _ as NSError{
                //打印错误消息
                let errorAlert =  UIAlertController(title: "提醒", message: "请在iPhone的\"设置-隐私-相机\"选项中,允许本程序访问您的相机", preferredStyle: .Alert)
                errorAlert.addAction(UIAlertAction(title: "确定", style: .Cancel, handler: nil))
                self.presentViewController(errorAlert, animated: true, completion: nil)
        }
    }

    func captureOutput(captureOutput: AVCaptureOutput!,
                       didOutputMetadataObjects metadataObjects: [AnyObject]!,
                                                fromConnection connection: AVCaptureConnection!) {
        var stringValue:String?
        if metadataObjects.count > 0 {
            let metadataObject = metadataObjects[0]
                as! AVMetadataMachineReadableCodeObject
            stringValue = metadataObject.stringValue
            if stringValue != nil{
                self.session.stopRunning()
            }
        }
        if let value = stringValue{
            self.session.stopRunning()
            resultAction(value)
        }
    }
    
    func setupAndFire(){
        self.view.layer.insertSublayer(self.preview, atIndex: 0)
        self.view.addSubview(self.scanRectView)
        self.session.startRunning()
    }
    
    func startRunning(){
        self.session.startRunning()
    }
    
    func stopRuning(){
        self.preview.removeFromSuperlayer()
        scanRectView.removeFromSuperview()
        self.session.stopRunning()
    }
}
