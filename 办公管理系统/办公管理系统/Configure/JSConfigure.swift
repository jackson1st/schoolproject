//
//  JSConfigure.swift
//  办公管理系统
//
//  Created by jason on 16/5/25.
//  Copyright © 2016年 jason. All rights reserved.
//

import UIKit
extension UIColor{
    convenience init(R:CGFloat,G:CGFloat,B:CGFloat, alpha: CGFloat){
        self.init(red: R/255.0, green: G/255.0, blue: B/255.0, alpha: alpha)
    }
    convenience init(RGB:Int,alpha:CGFloat){
        let r = CGFloat((RGB & 0xFF0000)>>16)
        let g = CGFloat((RGB & 0xFF00)>>8)
        let b =  CGFloat((RGB & 0xFF))
        self.init(R:r,G:g,B:b,alpha: alpha)
    }
    
    class func BackgroundColor() -> UIColor{
        return UIColor(RGB: 0xf2f2f2, alpha: 1)
    }

}
